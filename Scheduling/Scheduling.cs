using System.Collections.Generic;
using Common;

namespace Scheduling
{
    public static class Scheduling
    {
        //static void Main(string[] args)
        //{
        //    SchedulingInfo test = new SchedulingInfo("1234D", 1, 1, 5, 6, 2018);
        //    FileIO.FileIO.WriteScheduling(test);
        //    SchedulingInfo test2 = new SchedulingInfo("1234D", 2, 1, 5, 6, 2018);
        //    FileIO.FileIO.WriteScheduling(test2);
        //    SchedulingInfo test3 = new SchedulingInfo("1234D", 3, 1, 5, 6, 2018);
        //    FileIO.FileIO.WriteScheduling(test3);
        //    SchedulingInfo test4 = new SchedulingInfo("1234D", 4, 1, 5, 6, 2018);
        //    FileIO.FileIO.WriteScheduling(test4);
        //    SchedulingInfo test5 = new SchedulingInfo("1234D", 5, 1, 5, 6, 2018);
        //    FileIO.FileIO.WriteScheduling(test5);
        //    SchedulingInfo test6 = new SchedulingInfo("1234D", 6, 1, 5, 6, 2018);
        //    FileIO.FileIO.WriteScheduling(test6);

        //    if (FullAppointmentShedulePerDay (5,6,2018))
        //    {
        //        Console.Write("Hello");
        //    }

        //    Console.ReadLine();
        //}

        /// <summary>
        /// This function checks the timeslot availability of a timeslot on a selected day.
        /// </summary>
        /// <param name="schedulingInfo">The scheduling information to check the availability for.</param>
        /// <returns>bool : true if appointment is availible, false otherwise</returns>
        public static bool AvailabilityOfAppointment(SchedulingInfo schedulingInfo)
        {
            SchedulingInfo ifThereIsATimeSlot = new SchedulingInfo();

            if (schedulingInfo.timeSlot > 0)
            {
                FileIO.FileIO.SearchSchedulingInfo(ref ifThereIsATimeSlot, schedulingInfo.timeSlot, schedulingInfo.month, schedulingInfo.day);

                if (ifThereIsATimeSlot.timeSlot != 0)
                {
                    return false;
                }
            }

            return true;
        }


        /// <summary>
        /// This function checks if a given day is fully booked.
        /// </summary>
        /// <param name="day">The day to check.</param>
        /// <param name="month">The month to check.</param>
        /// <returns>bool : true if day is fully booked, false otherwise</returns>
        public static bool CheckDayFullyBooked(int day, int month)
        {
            List<SchedulingInfo> CheckFullAppointments = new List<SchedulingInfo>();

            FileIO.FileIO.GetScheduleInfo(ref CheckFullAppointments, month, day);

            if (CheckFullAppointments.Count == 6)
            {
                return true;
            }

            return false;
        }


        /// <summary>
        /// This function finds all fully booked days and returns an array of all booked days.
        /// </summary>
        /// <param name="month">The month to check.</param>
        /// <param name="numDays">The number of days in the month.</param>
        /// <returns>int[] : All days that are fully booked.</returns>
        public static int[] GetFullDaysInMonth(int month, int numDays)
        {
            List<int> FullyBookedDays = new List<int>();

            for (int i = 1; i < numDays; i++)
            {
                if (CheckDayFullyBooked(i, month))
                {
                    FullyBookedDays.Add(i);
                }
            }

            return FullyBookedDays.ToArray();
        }


        /// <summary>
        /// This function removes an appointment for a client on a specific day.
        /// </summary>
        /// <param name="schedulingInfo"></param>
        /// <returns>bool : true if successful, false otherwise</returns>
        public static bool RemoveAppointment(string HCN, int month, int day)
        {
            bool ReturnStatus;

            ReturnStatus = FileIO.FileIO.RemoveSchedulingInfo(HCN, month, day);

            return ReturnStatus;
        }
    }
}
