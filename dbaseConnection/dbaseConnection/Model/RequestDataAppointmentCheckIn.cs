﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dbaseConnection.Model
{
    public class RequestDataAppointmentCheckIn
    {
        public string healthCardNumber;
        public string appointmentDate;
        public string appointmentSlot;
    }
}
