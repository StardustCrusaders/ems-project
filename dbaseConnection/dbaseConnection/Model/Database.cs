﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace dbaseConnection.Model
{
    public class Database
    {
        static string connectionString = "Server=tcp:stardustcrusaders.database.windows.net,1433;Initial Catalog = EMS; Persist Security Info=False;User ID = emsAdmin; Password=stardustCrusaders1337!;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout = 30;";


        /// <summary>
        /// Sends a request for inserting a new appointment request.
        /// </summary>
        /// <param name="appointmentDate">The date of the appointment.</param>
        /// <param name="appointmentSlot">The appointment slot number.</param>
        /// <param name="healthCardNumber">The healthcard number of the patient.</param>
        /// <returns>
        ///     int - 0 if an unkown error occurs
        ///          -3 if a database error occurs (i.e. exception is thrown).
        ///          -2 if the patient does not exist.
        ///          -1 if the appointment request already exists.
        ///           1 if the request was made.
        /// </returns>
        public static int InsertAppointmentRequest(string appointmentDate, string appointmentSlot, string healthCardNumber)
        {
            int requestResponse = 0;

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand($"RequestAppointment '{healthCardNumber}', '{appointmentDate}', {appointmentSlot}", connection);

                    connection.Open();
                    SqlDataReader dataReader = command.ExecuteReader();

                    // Get the first data row to grab the lamp build completion time and the first part count.
                    if (dataReader != null)
                    {
                        dataReader.Read();
                        requestResponse = (int)dataReader["Response"];
                    }

                    dataReader.Close();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                requestResponse = -3;
            }

            return requestResponse;
        }


        public static List<int> SelectAvailableAppointmentSlots(string appointmentDate)
        {
            List<int> takenAppointmentSlots = new List<int>();
            List<int> availableAppointmentSlots = Enumerable.Range(1, 6).ToList();

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand($"SelectTimeSlotForDate '{appointmentDate}'", connection);

                    connection.Open();
                    SqlDataReader dataReader = command.ExecuteReader();

                    // Get the first data row to grab the lamp build completion time and the first part count.
                    if (dataReader != null)
                    {
                        while (dataReader.Read())
                        {
                            takenAppointmentSlots.Add((int)dataReader.GetByte(0));
                        }
                    }

                    dataReader.Close();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                availableAppointmentSlots = null;
            }

            for (int i = 0; i < takenAppointmentSlots.Count; ++i)
            {
                if (availableAppointmentSlots.Contains(takenAppointmentSlots[i]))
                {
                    availableAppointmentSlots.Remove(takenAppointmentSlots[i]);
                }
            }

            return availableAppointmentSlots;
        }



        public static List<DateTime> SelectAllAppointmentsForMonthAndYear(string month, string year)
        {
            List<KeyValuePair<DateTime, int>> selectedAppointments = new List<KeyValuePair<DateTime, int>>();

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand($"SelectAllAppointmentsForMonthAndYear '{month}, {year}'", connection);

                    connection.Open();
                    SqlDataReader dataReader = command.ExecuteReader();

                    // Get the first data row to grab the lamp build completion time and the first part count.
                    if (dataReader != null)
                    {
                        while (dataReader.Read())
                        {
                            selectedAppointments.Add(new KeyValuePair<DateTime, int>(dataReader.GetDateTime(0), (int)dataReader["TimeSlot"]));
                        }
                    }

                    dataReader.Close();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                return null;
            }

            // Get a lookup table for each appointmetn date.
            var lookup = selectedAppointments.ToLookup(kvp => kvp.Key, kvp => kvp.Value);

            List<DateTime> fullAppointments = new List<DateTime>();

            // Check if any dates have been overbooked.
            foreach (var k in lookup)
            {
                // Check if the day is on the weekend.
                if (k.Key.DayOfWeek == DayOfWeek.Saturday || k.Key.DayOfWeek == DayOfWeek.Sunday)
                {
                    if (k.Count() >= 2)
                    {
                        fullAppointments.Add(k.Key);

                    }
                }
                else
                {
                    if (k.Count() >= 6)
                    {
                        fullAppointments.Add(k.Key);
                    }
                }
            }

            return fullAppointments;
        }


        public static int CheckInForAppointment(string healthCardNumber, string appointmentDate, string appointmentSlot)
        {
            int requestResponse = 0;

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand($"CheckInForAppointment '{healthCardNumber}', '{appointmentDate}', {appointmentSlot}", connection);

                    connection.Open();
                    SqlDataReader dataReader = command.ExecuteReader();

                    // Get the first data row to grab the lamp build completion time and the first part count.
                    if (dataReader != null)
                    {
                        dataReader.Read();
                        requestResponse = (int)dataReader["Response"];
                    }

                    dataReader.Close();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                requestResponse = -3;
            }

            return requestResponse;
        }


        public static List<KeyValuePair<DateTime, int>> RequestConfirmedAppointmentsForToday(string healthCardNumber)
        {
            List<KeyValuePair<DateTime, int>> confimredAppointments = new List<KeyValuePair<DateTime, int>>();

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand($"RequestConfirmedAppointments '{healthCardNumber}'", connection);

                    connection.Open();
                    SqlDataReader dataReader = command.ExecuteReader();

                    // Get the first data row to grab the lamp build completion time and the first part count.
                    if (dataReader != null)
                    {
                        while (dataReader.Read())
                        {
                            confimredAppointments.Add(new KeyValuePair<DateTime, int>(dataReader.GetDateTime(0), (int)dataReader.GetByte(1)));
                        }
                    }

                    dataReader.Close();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                return null;
            }

            foreach(var appointment in confimredAppointments)
            {
                if(appointment.Key.Date.Year != DateTime.Now.Year && appointment.Key.Date.Month != DateTime.Now.Year && appointment.Key.Date.Day != DateTime.Now.Day)
                {
                    confimredAppointments.Remove(appointment);
                }
            }

            return confimredAppointments;
        }


        public static bool VerifyMobileLogin(string healthCardNumber, string passwordHash)
        {
            if(passwordHash.Length > 128 || !Regex.Match(healthCardNumber, @"^[0-9]{10}[A-Za-z]{2}").Success)
            {
                return false;
            }

            int requestStatus = 0;

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand($"VerifyMobileLogin '{healthCardNumber}', '{passwordHash}'", connection);

                    connection.Open();
                    SqlDataReader dataReader = command.ExecuteReader();

                    // Get the first data row to grab the lamp build completion time and the first part count.
                    if (dataReader != null)
                    {
                        dataReader.Read();
                        requestStatus = (int)dataReader["verified"];
                    }

                    dataReader.Close();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            if(requestStatus == 0)
            {
                return false;
            }

            return true;
        }


        public static int AddMobileLogin(string healthCardNumber, string passwordHash)
        {
            if (passwordHash.Length > 128 || !Regex.Match(healthCardNumber, @"^[0-9]{10}[A-Za-z]{2}").Success)
            {
                return -3;
            }

            int requestStatus = 0;

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand($"AddMobileLogin '{healthCardNumber}', '{passwordHash}'", connection);

                    connection.Open();
                    SqlDataReader dataReader = command.ExecuteReader();

                    // Get the first data row to grab the lamp build completion time and the first part count.
                    if (dataReader != null)
                    {
                        dataReader.Read();
                        requestStatus = (int)dataReader["returnValue"];
                    }

                    dataReader.Close();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                return -4;
            }

            
            return requestStatus;
        }
    }
}
