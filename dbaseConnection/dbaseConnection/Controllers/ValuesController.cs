﻿using System.Collections.Generic;
using dbaseConnection.Model;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Web;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using System.Text;
using System;

namespace dbaseConnection.Controllers
{
    [Route("api/conn")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }


        [HttpPost]
        [Route("requestAppointment")]
        public JsonResult PostRequestAppointment([FromBody] RequestDataAppointmentRequest request)
        {
            int requestReturn = Database.InsertAppointmentRequest(request.Date, request.appointmentSlot, request.healthCardNumber);

            StringBuilder response = new StringBuilder("{ \"responseString\" : ");
            switch(requestReturn)
            {
                case 0: response.Append("\"unable to request appointment\"}");
                    break;
                case -1: response.Append("\"request already exists\"}");
                    break;
                case -2: response.Append("\"patient does not exist\"}");
                    break;
                case 1: response.Append("\"request added successfuly\"}");
                    break;
                default: response.Append("\"unkown error\"}");
                    break;
            }

            object json = JsonConvert.DeserializeObject(response.ToString());
            return new JsonResult(json);
        }


        [HttpPost]
        [Route("RequestAvailableAppointmentSlots")]
        public JsonResult PostRequestRequestAvailableSlots([FromBody] RequestDataAvailableSlots request)
        {
            List<int> availableSlots = Database.SelectAvailableAppointmentSlots(request.appointmentDate);

            // Build the JSON array containing all available time slots.
            StringBuilder response = new StringBuilder("{\"availableSlots\": [");
            foreach(var slot in availableSlots)
            {
                response.Append($" \"{slot}\",");
            }
            response.Remove(response.Length - 1, 1);
            response.Append(" ]}");

            object json = JsonConvert.DeserializeObject(response.ToString());
            return new JsonResult(json);
        }


        [HttpPost]
        [Route("RequestAvailableAppointmentDates")]
        public JsonResult PostRequestAvailableAppointmentDates([FromBody] RequestDataAvailableDates request)
        {
            List<DateTime> unavailabelAppointmentDate = Database.SelectAllAppointmentsForMonthAndYear(request.month, request.year);

            StringBuilder response = new StringBuilder("{\"unavailableDates\": [ ");

            if (unavailabelAppointmentDate != null)
            {
                foreach (var date in unavailabelAppointmentDate)
                {
                    response.Append($"\"{date.ToString("yyyy-MM-dd")}\", ");
                }
                response.Remove(response.Length - 1, 1);
            }
            response.Append("]}");

            object json = JsonConvert.DeserializeObject(response.ToString());

            return new JsonResult(json);
        }


        [HttpPost]
        [Route("RequestAppointmentCheckIn")]
        public JsonResult PostRequestAppointmentCheckIn([FromBody] RequestDataAppointmentCheckIn request)
        {
            int requestReturn = Database.CheckInForAppointment(request.healthCardNumber, request.appointmentDate, request.appointmentSlot);

            StringBuilder response = new StringBuilder("{ \"responseString\" : ");
            switch (requestReturn)
            {
                case 0:
                    response.Append("\"unable to check in for appointment\"}");
                    break;
                case -1:
                    response.Append("\"appointment does not exist\"}");
                    break;
                case 1:
                    response.Append("\"check in successful\"}");
                    break;
                default:
                    response.Append("\"unkown error\"}");
                    break;
            }

            object json = JsonConvert.DeserializeObject(response.ToString());
            return new JsonResult(json);
        }


        [HttpPost]
        [Route("RequestConfirmedAppointments")]
        public JsonResult PostRequestConfimredAppointmentsForToday([FromBody] RequestDataConfirmedAppointment request)
        {
            List<KeyValuePair<DateTime, int>> confirmedAppointmetns = Database.RequestConfirmedAppointmentsForToday(request.healthCardNumber);

            StringBuilder response = new StringBuilder("{ \"confirmedAppointments\" : [ ");
            foreach (var appointment in confirmedAppointmetns)
            {
                if (appointment.Key.Year == DateTime.Now.Year && appointment.Key.Month == DateTime.Now.Month && appointment.Key.Day == DateTime.Now.Day)
                {
                    response.Append(" { \"date\" : \"" + appointment.Key.ToString("yyyy-MM-dd") + "\", \"timeslot\" : \"" + appointment.Value + "\"},");
                }
            }
            response.Remove(response.Length - 1, 1);
            response.Append("]}");

            object json = JsonConvert.DeserializeObject(response.ToString());
            return new JsonResult(json);
        }


        [HttpPost]
        [Route("VerifyMobileLogin")]
        public JsonResult PostRequestVerifyPasswordHash([FromBody] RequestDataMobileLogin request)
        {

            StringBuilder response = new StringBuilder("{\"verificationResponse\" : ");

            if (Database.VerifyMobileLogin(request.healthCardNumber, request.passwordHash))
            {
                response.Append("\"good\"}");
            }
            else
            {
                response.Append("\"bad\"}");
            }

            object json = JsonConvert.DeserializeObject(response.ToString());
            return new JsonResult(json);
        }


        [HttpPost]
        [Route("addMobileLogin")]
        public JsonResult PostAddMobileLogin([FromBody] RequestDataMobileLogin request)
        {
            StringBuilder response = new StringBuilder("{\"verificationResponse\" : ");

            int requestStatus = Database.AddMobileLogin(request.healthCardNumber, request.passwordHash);
            
            if(requestStatus == 1)
            {
                response.Append("\"accepted\"}");
            }
            else if(requestStatus == -1)
            {
                response.Append("\"already exists\"}");
            }
            else if(requestStatus == -2)
            {
                response.Append("\"patient does not exist\"}");
            }
            else if(requestStatus == -3)
            {
                response.Append("\"invalid username or password\"");
            }
            else
            {
                response.Append("\"unkown error\"}");
            }

            object json = JsonConvert.DeserializeObject(response.ToString());
            return new JsonResult(json);
        }


        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }
    }
}
