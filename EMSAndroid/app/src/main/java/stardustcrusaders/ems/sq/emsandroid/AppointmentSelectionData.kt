package stardustcrusaders.ems.sq.emsandroid


import android.os.Parcel
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.lang.reflect.Constructor
import java.util.*



data class AppointmentSelectionData(var year: Int, var month: Int, var dayOfMonth: Int, var appointmentSlot: Int): Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(year)
        parcel.writeInt(month)
        parcel.writeInt(dayOfMonth)
        parcel.writeInt(appointmentSlot)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AppointmentSelectionData> {
        override fun createFromParcel(parcel: Parcel): AppointmentSelectionData {
            return AppointmentSelectionData(parcel)
        }

        override fun newArray(size: Int): Array<AppointmentSelectionData?> {
            return arrayOfNulls(size)
        }
    }

}