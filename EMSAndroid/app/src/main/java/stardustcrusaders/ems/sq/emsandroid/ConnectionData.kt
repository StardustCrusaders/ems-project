package stardustcrusaders.ems.sq.emsandroid

import com.google.gson.JsonObject
import okhttp3.*
import okhttp3.RequestBody
import okhttp3.OkHttpClient
import org.json.JSONObject


class DataRequest() {


    companion object {

        private val client by lazy { OkHttpClient() }

        fun RequestAppointment(appointmentDate: String, appointmentSlot: String, healthCardNumber: String): JSONObject? {

            var jsonResponse: JSONObject? = null

            val networkThread = Thread(Runnable {

                val mediaType = MediaType.parse("application/json")
                val body = RequestBody.create(
                    mediaType,
                    "{\n\t\"Date\" : \"$appointmentDate\",\n\t\"appointmentSlot\" : \"$appointmentSlot\",\n\t\"healthCardNumber\" : \"$healthCardNumber\"\n}"
                )
                val request = Request.Builder()
                    .url("https://dbaseconnection20190421094042.azurewebsites.net/api/conn/requestAppointment")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .build()

                val response = client.newCall(request).execute()

                val responseStr = response.body()?.string().toString()
                jsonResponse = JSONObject(responseStr)
            })

            networkThread.start()
            networkThread.join()

            return jsonResponse
        }


        fun RequestAvailableAppointmentSlots(appointmentDate: String): JSONObject? {

            var jsonResponse: JSONObject? = null

            val networkThread = Thread(Runnable {

                val mediaType = MediaType.parse("application/json")
                val body = RequestBody.create(mediaType, "{\n\t\"appointmentDate\" : \"$appointmentDate\"\n}")
                val request = Request.Builder()
                    .url("https://dbaseconnection20190421094042.azurewebsites.net/api/conn/RequestAvailableAppointmentSlots")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .build()

                val response = client.newCall(request).execute()

                val responseStr = response.body()?.string().toString()
                jsonResponse = JSONObject(responseStr)
            })

            networkThread.start()
            networkThread.join()

            return jsonResponse
        }

        fun RequestAvailableAppointmentDates(year: String, month: String): JSONObject? {

            var jsonResponse: JSONObject? = null

            val networkThread = Thread(Runnable {

                val mediaType = MediaType.parse("application/json")
                val body = RequestBody.create(mediaType, "{\n\t\"month\" : \"$month\",\n\t\"year\" : \"$year\"\n}")
                val request = Request.Builder()
                    .url("https://dbaseconnection20190421094042.azurewebsites.net/api/conn/RequestAvailableAppointmentDates")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .build()

                val response = client.newCall(request).execute()

                val responseStr = response.body()?.string().toString()
                jsonResponse = JSONObject(responseStr)
            })

            networkThread.start()
            networkThread.join()

            return jsonResponse
        }


        fun RequestAppointmentCheckIn(healthCardNumber: String, appointmentSlot: String, appointmentDate: String): JSONObject? {

            var jsonResponse: JSONObject? = null

            val networkThread = Thread(Runnable {

                val mediaType = MediaType.parse("application/json")
                val body = RequestBody.create(
                    mediaType,
                    "{\n\t\"healthCardNumber\" : \"$healthCardNumber\",\n\t\"appointmentDate\" : \"$appointmentDate\",\n\t\"appointmentSlot\" : \"$appointmentSlot\",\n}"
                )
                val request = Request.Builder()
                    .url("https://dbaseconnection20190421094042.azurewebsites.net/api/conn/RequestAppointmentCheckIn")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .build()

                val response = client.newCall(request).execute()

                val responseStr = response.body()?.string().toString()
                jsonResponse = JSONObject(responseStr)
            })

            networkThread.start()
            networkThread.join()

            return jsonResponse
        }


        fun RequestConfirmedAppointments(healthCardNumber: String): JSONObject? {

            var jsonResponse: JSONObject? = null

            val networkThread = Thread(Runnable {

                val mediaType = MediaType.parse("application/json")
                val body = RequestBody.create(mediaType, "{\n\t\"healthCardNumber\" : \"$healthCardNumber\"\n}")
                val request = Request.Builder()
                    .url("https://dbaseconnection20190421094042.azurewebsites.net/api/conn/RequestConfirmedAppointments")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .build()

                val response = client.newCall(request).execute()

                val responseStr = response.body()?.string().toString()
                jsonResponse = JSONObject(responseStr)
            })

            networkThread.start()
            networkThread.join()

            return jsonResponse
        }


        fun VerifyMobileLogin(healthCardNumber: String, passwordHash: String): JSONObject? {

            var jsonResponse: JSONObject? = null

            val networkThread = Thread(Runnable {

                val mediaType = MediaType.parse("application/json")
                val body = RequestBody.create(
                    mediaType,
                    "{\n\t\"healthCardNumber\" : \"$healthCardNumber\",\n\t\"passwordHash\" : \"$passwordHash\"\n}\n\n"
                )
                val request = Request.Builder()
                    .url("https://dbaseconnection20190421094042.azurewebsites.net/api/conn/VerifyMobileLogin")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .build()

                val response = client.newCall(request).execute()

                val responseStr = response.body()?.string().toString()
                jsonResponse = JSONObject(responseStr)
            })

            networkThread.start()
            networkThread.join()

            return jsonResponse
        }


        fun AddMobileLogin(healthCardNumber: String, passwordHash: String): JSONObject? {

            var jsonResponse: JSONObject? = null

            val networkThread = Thread(Runnable {

                val mediaType = MediaType.parse("application/json")
                val body = RequestBody.create(
                    mediaType,
                    "{\n\t\"healthCardNumber\" : \"$healthCardNumber\", \n\t\"passwordHash\" : \"$passwordHash\"\n}\n\n"
                )
                val request = Request.Builder()
                    .url("https://dbaseconnection20190421094042.azurewebsites.net/api/conn/addMobileLogin")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .build()

                val response = client.newCall(request).execute()

                val responseStr = response.body()?.string().toString()
                jsonResponse = JSONObject(responseStr)
            })

            networkThread.start()
            networkThread.join()

            return jsonResponse
        }
    }
}