package stardustcrusaders.ems.sq.emsandroid

import android.Manifest
import android.app.ActivityOptions
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.transition.Fade
import android.view.Window
import kotlinx.android.synthetic.main.activity_menu_options.*


class MenuOptionsActivity : AppCompatActivity() {

    companion object {

        const val BOOK_APPOINTMENT_REQUEST = 1
        const val CHECK_IN_REQUEST = 2

        // Permissions data.
        val ASK_PERMISSION_CODE: Int = 1
        val READ_REQUEST_CODE: Int = 42
        val PERMISSIONS = arrayOf(Manifest.permission.INTERNET)
    }

    var healthCardNumber: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {

        with(window) {
            requestFeature(Window.FEATURE_ACTIVITY_TRANSITIONS)

            val fade = Fade().excludeTarget(R.id.action_bar, true)
                .excludeTarget(android.R.id.navigationBarBackground, true)
                .excludeTarget(android.R.id.statusBarBackground, true)

            exitTransition = fade
            enterTransition = fade
        }


        healthCardNumber = this.intent?.getStringExtra("healthCardNumber")

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_options)

        book_appointment_button.setOnClickListener { _ ->

            val bookAppointmentIntent = Intent(this, RequestAppointmentWizardActivity::class.java)
            bookAppointmentIntent.putExtra("healthCardNumber", healthCardNumber)
            startActivity(bookAppointmentIntent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle())
        }

        check_in_button.setOnClickListener { _ ->

            val checkInIntent = Intent(this, CheckInActivity::class.java)
            checkInIntent.putExtra("healthCardNumber", healthCardNumber)
            startActivity(checkInIntent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle())
        }
    }


    override fun onBackPressed() {

        val checkInIntent = Intent(this, LoginActivity::class.java)
        checkInIntent.putExtra("healthCardNumber", CheckInActivity.healthCardNumber)
        startActivity(checkInIntent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle())
        finish()
    }
}