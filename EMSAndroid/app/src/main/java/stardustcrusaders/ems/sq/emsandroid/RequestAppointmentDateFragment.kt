package stardustcrusaders.ems.sq.emsandroid

import android.app.*
import android.os.Bundle
import java.util.*
import android.app.DatePickerDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Message
import android.content.Intent.getIntent




/**
 * Date picker dialog fragment.
 * @author Conor Macpherson
 * @version 1.0
 * @since 1.0
 */
class RequestAppointmentDateFragment: DialogFragment() {


    /**
     * <p>
     * Overrides the onCreateDialog event extended in the DialogFragment class.
     * </p>
     * <p>
     * Create a DatePicker dialog with a minimum date set as the current date.
     * </p>
     *
     * @param  savedInstanceState    A Bundle? received from any intents.
     *
     * @return Dialog   The date picker dialog
     */
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        // Unable to do without customer date picker dialog: Look at Material date picker
        //TODO: Add a query to disable all dates that do not have appointments available.

        val calendar = Calendar.getInstance()

        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        val listener = activity as DatePickerDialog.OnDateSetListener

        // Set the minimum date in the dialog to the current date.
        val datePickerDialog = DatePickerDialog(activity, listener, year, month, day)

        // Minus 100 makes the min date just a bit behind the current date.time
        // so we can select the current date, closing the date picker dialog in the process.
        datePickerDialog.datePicker.minDate = Date().time.minus(100)
        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "", Message())
        datePickerDialog.setCanceledOnTouchOutside(false)
        datePickerDialog.setCancelable(false)

        return datePickerDialog
    }

    override fun onCancel(dialog: DialogInterface?) {

        RequestAppointmentDateFragment().show(fragmentManager, "datePicker")
    }
}