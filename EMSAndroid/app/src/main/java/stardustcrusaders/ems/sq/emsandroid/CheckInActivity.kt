package stardustcrusaders.ems.sq.emsandroid

import android.annotation.SuppressLint
import android.app.ActionBar
import android.app.ActivityOptions
import android.content.Intent
import android.content.res.TypedArray
import android.graphics.Point
import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.CardView
import android.text.SpannableString
import android.text.style.StyleSpan
import android.text.style.UnderlineSpan
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import kotlinx.android.synthetic.main.notification_template_part_time.*


class CheckInActivity : AppCompatActivity() {

    companion object {

        var healthCardNumber: String? = ""
    }

    var slotViews: MutableList<View?> = mutableListOf()
    var timeSlotAppointmentDays: MutableMap<Int, String?> = mutableMapOf()


    @SuppressLint("Recycle")
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_appointment_check_in)

        healthCardNumber = this.intent?.getStringExtra("healthCardNumber")

        // Get the number of appointments
        val response = DataRequest.RequestConfirmedAppointments(healthCardNumber!!)
        val confirmedAppointments = response?.getJSONArray("confirmedAppointments")
        var numberOfAppointments = confirmedAppointments?.length()

        if(numberOfAppointments == null) {

            numberOfAppointments = 0
        }

        // Add the time slots and date strings to the map for use in the appointment confirmation clicks
        for(i in 0 until numberOfAppointments) {

            val appointmentSlot = confirmedAppointments?.getJSONObject(i)
            timeSlotAppointmentDays[Integer.parseInt(appointmentSlot?.get("timeslot").toString())] = appointmentSlot?.get("date").toString()
        }

        setupAppointmentConfirmation(numberOfAppointments, findViewById<LinearLayout?>(R.id.appointmentSlots))
    }



    override fun onBackPressed() {

        val checkInIntent = Intent(this, MenuOptionsActivity::class.java)
        checkInIntent.putExtra("healthCardNumber", healthCardNumber)
        startActivity(checkInIntent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle())
        finish()
    }


    /**
     * <p>
     * Set up each appointment confirmation selection cards
     * </p>
     *
     * @param  numberOfAppointments     The number of appointments
     * @param layout                    The layout to add the cards to.
     *
     * @return Unit
     */
    private fun setupAppointmentConfirmation(numberOfAppointments: Int, layout: LinearLayout?) {

        // Get a typed array of the slot layouts/
        val slotLayouts: TypedArray = resources.obtainTypedArray(R.array.slot_layouts)

        // If the number of appointmetns is greater than 0, set the appointment availability text to available.
        if(numberOfAppointments > 0) {

            findViewById<TextView>(R.id.appointmentAvailabilityText).text = getText(R.string.check_in_appointments_available)
        }

        // Iterate over each time slot and appointment
        for(timeslot in timeSlotAppointmentDays) {

            // Inflate and add an on click listener to the slot view.
            val appointment: View? = layoutInflater.inflate(slotLayouts.getResourceId(timeslot.key - 1, -1), null)
            appointment?.id = timeslot.key
            appointment?.tag = timeslot.key
            layout?.addView(appointment)
            slotViews.add(appointment)

            appointment?.setOnClickListener {

                // Remove all other slots
                for (slotView in slotViews) {

                    slotView?.isEnabled = false
                }

                // Get the containing the selected item and change the background color to indicate the selected slot.
                val cardView = appointment.findViewById<CardView>(R.id.card_view)
                cardView.setBackgroundColor(resources.getColor(R.color.slotSelectedColor))

                // Translate the slot to the right a large amount, then animate it off the screen to the left.
                appointment.animate().translationX(235.0f).withEndAction {

                    appointment.animate().translationX((-appointment.width).toFloat()).withEndAction {

                        layout?.removeView(appointment)
                    }

                    // Remove all other slots
                    for (slotView in slotViews) {

                        if (cardView != slotView) {

                            slotView?.animate()?.translationX(65.0f)?.withEndAction {

                                slotView.animate().translationX((-appointment.width).toFloat()).withEndAction {

                                    layout?.removeView(slotView)
                                }
                            }
                        }
                    }
                }

                setupConfirmation(layout, timeslot.key, timeslot.value.toString())
            }
        }
    }


    /**
     * <p>
     * Sets up the appointment and slot confirmation screen, checking in for an appointment if the patient chooses to.
     * </p>
     *
     * @param  layout                   The linear layout to setup the confirmation with.
     * @param  appointmentSlotID        The appointment slot to confirm.
     * @param  appointmentDate          The date of the appointment
     *
     * @return Unit
     */
    private fun setupConfirmation(layout: LinearLayout?, appointmentSlotID: Int, appointmentDate: String) {

        // Inflate the confirmation layout to add to the given linear layout.
        val confirmationView: View? = layoutInflater.inflate(resources.getLayout(R.layout.confirm_appointment_layout), null)
        layout?.addView(confirmationView)

        // Animate the layout add.
        val animation = AnimationUtils.loadAnimation(baseContext, R.anim.abc_grow_fade_in_from_bottom)
        animation.startOffset = 5
        confirmationView?.startAnimation(animation)

        // Set the appointment confirmation text.
        val confirmationText = confirmationView?.findViewById<TextView>(R.id.appointmentConfirmationText)
        val appointmentID = SpannableString("$appointmentSlotID")
        val appointmentDateSpannable = SpannableString(appointmentDate)

        // Bold and underline the important details.
        appointmentID.setSpan(StyleSpan(Typeface.BOLD), 0, appointmentID.length, 0)
        appointmentID.setSpan(UnderlineSpan(), 0, appointmentID.length, 0)
        appointmentDateSpannable.setSpan(StyleSpan(Typeface.BOLD), 0, appointmentDateSpannable.length, 0)
        appointmentDateSpannable.setSpan(UnderlineSpan(), 0, appointmentDateSpannable.length, 0)

        // Add the text while keeping the styles
        confirmationText?.text = getString(R.string.confirm_check_in) + " "
        confirmationText?.append(appointmentID)
        confirmationText?.append(" on ")
        confirmationText?.append(appointmentDateSpannable)
        confirmationText?.append("?")

        val confirm = confirmationView?.findViewById<Button>(R.id.confirmButton)
        val cancel = confirmationView?.findViewById<Button>(R.id.cancelButton)

        val confirmationMessage: TextView = TextView(this)
        confirmationMessage.layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )

        val params = LinearLayout.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT)
        params.weight = 1.0f
        params.leftMargin = 2
        params.rightMargin = 2
        confirmationMessage.layoutParams = params
        confirmationMessage.visibility = View.GONE
        confirmationMessage.gravity = Gravity.CENTER
        confirmationMessage.textSize = 20.0f
        layout?.addView(confirmationMessage)

        val display = windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        val height = size.y
        confirmationMessage.y = (height + 100).toFloat()

        // Animate all items off screen with confirm starting.
        confirm?.setOnClickListener {

            confirmationMessage.text = getString(R.string.check_in_confirm)

            confirm.isEnabled = false
            cancel?.isEnabled = false

            confirm.animate().translationX(-(confirm.x + confirm.width)).withEndAction {

                cancel?.animate()?.translationX(-(cancel.x + cancel.width))?.withEndAction {

                    confirmationText?.animate()?.translationX((-confirmationText.width).toFloat())?.withEndAction {

                        confirmationMessage.animate().translationY(confirmationMessage.y)
                        confirmationMessage.visibility = View.VISIBLE
                        confirmationMessage.animate().translationY(-(height / 4.0f)).withEndAction {

                            try {

                                val response = DataRequest.RequestAppointmentCheckIn(healthCardNumber.toString(), appointmentSlotID.toString(), appointmentDate)
                                val checkInResponse = response?.get("responseString").toString()

                                if(checkInResponse != "check in successful") {

                                    confirmationMessage.text = getString(R.string.check_in_failure)
                                }

                                Thread.sleep(2000)

                                val mainMenu = Intent(this, MenuOptionsActivity::class.java)
                                mainMenu.putExtra("healthCardNumber", healthCardNumber)
                                startActivity(mainMenu)
                                finish()
                            } catch (e: InterruptedException) {

                                println(e.message)
                            }
                        }
                    }
                }
            }
        }


        // Animate all items off screen with cancel starting.
        cancel?.findViewById<Button>(R.id.cancelButton)?.setOnClickListener {

            confirmationMessage.text = getString(R.string.check_in_cancel)

            cancel.isEnabled = false
            confirm?.isEnabled = false

            cancel.animate().translationX(-(cancel.x + cancel.width)).withEndAction {

                confirm?.animate()?.translationX(-(confirm.x + confirm.width))?.withEndAction {

                    confirmationText?.animate()?.translationX((-confirmationText.width).toFloat())?.withEndAction {

                        confirmationMessage.animate().translationY(confirmationMessage.y)
                        confirmationMessage.visibility = View.VISIBLE
                        confirmationMessage.animate().translationY(-(height / 4.0f)).withEndAction {

                            try {
                                Thread.sleep(2000)

                                val mainMenu = Intent(this, MenuOptionsActivity::class.java)
                                mainMenu.putExtra("healthCardNumber", healthCardNumber)
                                startActivity(mainMenu)
                                finish()
                            } catch (e: InterruptedException) {

                                println(e.message)
                            }
                        }
                    }
                }
            }
        }
    }

}