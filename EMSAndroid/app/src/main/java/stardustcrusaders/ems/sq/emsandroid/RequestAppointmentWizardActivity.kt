package stardustcrusaders.ems.sq.emsandroid

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.DatePicker
import java.util.*
import android.content.res.TypedArray
import android.graphics.Typeface
import android.support.v7.widget.CardView
import android.text.SpannableString
import android.text.style.StyleSpan
import android.text.style.UnderlineSpan
import android.transition.Fade
import android.view.ViewGroup.LayoutParams.*
import android.widget.LinearLayout
import android.widget.TextView
import android.view.animation.AnimationUtils
import android.widget.Button
import android.app.ActionBar
import android.app.ActivityOptions
import android.content.Intent
import android.graphics.Point
import android.view.*
import org.json.JSONArray


/**
* Date picker dialog fragment.
* @author Conor Macpherson
* @version 1.0
* @since 1.0
*/
class RequestAppointmentWizardActivity : AppCompatActivity(), DatePickerDialog.OnDateSetListener {

    companion object {

        var healthCardNumber: String? = ""
        var currentContent: CURRENT_CONTENT = CURRENT_CONTENT.CALENDAR
    }

    enum class CURRENT_CONTENT {

        CALENDAR,
        SLOT_SELECTION,
        CONFIRM_SELECTION
    }


    var date: String = ""
    var numberOfAppointmentSlots: Int = 6
    lateinit var slotLayouts: TypedArray
    var slotViews: MutableList<View?> = mutableListOf()
    lateinit var calendar: Calendar

    /**
     * <p>
     * Overrides the onCreate event extended in AppCompatActivity class.
     * </p>
     *
     * @param  savedInstanceState    A Bundle? received from any intents.
     *
     * @return Unit
     */
    override fun onCreate(savedInstanceState: Bundle?) {

        with(window) {
            requestFeature(Window.FEATURE_ACTIVITY_TRANSITIONS)

            val fade = Fade().excludeTarget(R.id.action_bar, true)
                .excludeTarget(android.R.id.navigationBarBackground, true)
                .excludeTarget(android.R.id.statusBarBackground, true)

            exitTransition = fade
            enterTransition = fade
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking_wizard)

        healthCardNumber = this.intent?.getStringExtra("healthCardNumber")

        showDatePickerDialog()
    }

    /**
     * <p>
     * Overrides the onBackPressed event to navigate to the previous appointment selection section.
     * </p>
     *
     * @return Unit
     */
    override fun onBackPressed() {

        if(currentContent == CURRENT_CONTENT.CALENDAR) {

            val checkInIntent = Intent(this, MenuOptionsActivity::class.java)
            checkInIntent.putExtra("healthCardNumber", CheckInActivity.healthCardNumber)
            startActivity(checkInIntent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle())
            finish()
        }
        else if(currentContent == CURRENT_CONTENT.SLOT_SELECTION) {

            showDatePickerDialog()
        }
        else if(currentContent == CURRENT_CONTENT.CONFIRM_SELECTION) {

            setupAppointmentSlots()
        }
    }


    /**
     * <p>
     * Shows the Date picker dialog fragment
     * </p>
     *
     * @return Unit
     */
    private fun showDatePickerDialog() {

        RequestAppointmentDateFragment().show(fragmentManager, "datePicker")
    }


    /**
     * <p>
     * Overrides the onDateSet listener event which implemented in the DatePickerDialog
     * </p>
     * <p>
     * This method will get the selected date the user selects in the dialog fragment.
     * </p>
     *
     * @param  view         The date picker created in the fragment dialog
     * @param  year         The year selected in the fragment dialog
     * @param  month        The month selected in the fragment dialog
     * @param  dayOfMonth   The day selected in the fragment dialog
     * @return Unit
     */
    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {

        calendar = Calendar.getInstance()
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, month)
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

        numberOfAppointmentSlots = when (calendar.get(Calendar.DAY_OF_WEEK)) {
            Calendar.SATURDAY -> 2
            Calendar.SUNDAY -> 2
            else -> 6
        }

        date = "%d-%02d-%02d".format(year, month + 1, dayOfMonth)

        currentContent = CURRENT_CONTENT.SLOT_SELECTION
        setupAppointmentSlots()
    }


    /**
     * <p>
     * Adds each available appointment slot to the slot linear layout.
     * </p>
     *
     * @return Unit
     */
    @SuppressLint("Recycle")
    private fun setupAppointmentSlots() {

        currentContent = CURRENT_CONTENT.SLOT_SELECTION

        // Get the layout which will contain the slots.
        val layout: LinearLayout? = this.findViewById(R.id.slot_selection)

        val dateTextView = TextView(this)
        val appointmentDate = SpannableString(getDateString())

        // Bold and underline the important details.
        appointmentDate.setSpan(StyleSpan(Typeface.BOLD), 0, appointmentDate.length, 0)
        appointmentDate.setSpan(UnderlineSpan(), 0, appointmentDate.length, 0)

        // Add the text while keeping the styles
        dateTextView.text = getString(R.string.appointment_slot_date) + " "
        dateTextView.append(appointmentDate)
        dateTextView.append(".")

        val params = LinearLayout.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT)
        params.weight = 1.0f
        params.leftMargin = 2
        params.rightMargin = 2
        dateTextView.layoutParams = params
        dateTextView.visibility = View.GONE
        dateTextView.gravity = Gravity.CENTER
        dateTextView.textSize = 20.0f
        layout?.addView(dateTextView)


        // Get an array of the slot slotLayouts.
        slotLayouts = resources.obtainTypedArray(R.array.slot_layouts)

        val response = DataRequest.RequestAvailableAppointmentSlots(date)
        val availableSlots = response?.get("availableSlots") as JSONArray

        // Get an array list of the available slots for use of the contains method
        val availableSlotsArray = ArrayList<Int>()
        if (availableSlots != null) {
            val len = availableSlots.length()
            for (i in 0 until len) {
                availableSlotsArray.add(Integer.parseInt(availableSlots.get(i).toString()))
            }
        }

        // Remove all the views in the case we are moving backwards from the slot confirmation.
        layout?.removeAllViews()

        // Add the slot slotLayouts for each available appointment slot.
        for (i in 1..numberOfAppointmentSlots) {

            // Inflate and add an on click listener to the slot view.
            val appointmentSlot: View? = layoutInflater.inflate(slotLayouts.getResourceId(i - 1, -1), null)
            slotViews.add(appointmentSlot)

            var isAvailable = false

            if (availableSlotsArray.contains(i)) {

                isAvailable = true
                // Add a click listener to animate all slots off the screen and display the confirmation.
                appointmentSlot?.setOnClickListener {

                    // Remove all other slots
                    for (slotView in slotViews) {

                        slotView?.isEnabled = false
                    }

                    // Get the containing the selected item and change the background color to indicate the selected slot.
                    val cardView = appointmentSlot.findViewById<CardView>(R.id.card_view)
                    cardView.setBackgroundColor(resources.getColor(R.color.slotSelectedColor))

                    // Translate the slot to the right a large amount, then animate it off the screen to the left.
                    appointmentSlot.animate().translationX(235.0f).withEndAction {

                        appointmentSlot.animate().translationX((-appointmentSlot.width).toFloat()).withEndAction {

                            layout?.removeView(appointmentSlot)
                        }

                        // Remove all other slots
                        for (slotView in slotViews) {

                            if (cardView != slotView) {
                                slotView?.animate()?.translationX(65.0f)?.withEndAction {

                                    slotView.animate().translationX((-appointmentSlot.width).toFloat()).withEndAction {

                                        layout?.removeView(slotView)
                                    }
                                }
                            }
                        }

                        setupConfirmation(layout, appointmentSlot.id)
                    }
                }
            }

            // Add the appointment slot with an id and tag.
            appointmentSlot?.id = i
            appointmentSlot?.tag = i
            layout?.addView(appointmentSlot)

            // If the slot is not available, set the disabled color.
            if(!isAvailable) {

                val cardView = appointmentSlot?.findViewById<CardView>(R.id.card_view)
                cardView?.setBackgroundColor(resources.getColor(R.color.disabledColor))
            }
        }
    }


    /**
     * <p>
     * Sets up the appointment and slot confirmation screen.
     * </p>
     *
     * @param  layout                   The linear layout to setup the confirmation with.
     * @param  appointmentSlotID        The appointment slot to confirm.
     *
     * @return Unit
     */
     fun setupConfirmation(layout: LinearLayout?, appointmentSlotID: Int) {

        currentContent = CURRENT_CONTENT.CONFIRM_SELECTION

        // Inflate the confirmation layout to add to the given linear layout.
        val confirmationView: View? = layoutInflater.inflate(resources.getLayout(R.layout.confirm_appointment_layout), null)
        layout?.addView(confirmationView)

        // Animate the layout add.
        val animation = AnimationUtils.loadAnimation(baseContext, R.anim.abc_grow_fade_in_from_bottom)
        animation.startOffset = 5
        confirmationView?.startAnimation(animation)


        // Set the appointment confirmation text.
        val confirmationText = confirmationView?.findViewById<TextView>(R.id.appointmentConfirmationText)
        val appointmentID = SpannableString("$appointmentSlotID")
        val appointmentDate = SpannableString(getDateString())

        // Bold and underline the important details.
        appointmentID.setSpan( StyleSpan(Typeface.BOLD), 0, appointmentID.length, 0)
        appointmentID.setSpan(UnderlineSpan(), 0, appointmentID.length, 0)
        appointmentDate.setSpan(StyleSpan(Typeface.BOLD), 0, appointmentDate.length, 0)
        appointmentDate.setSpan(UnderlineSpan(), 0, appointmentDate.length, 0)

        // Add the text while keeping the styles
        confirmationText?.text = getString(R.string.confirm_appointment_slot_prefix) + " "
        confirmationText?.append(appointmentID)
        confirmationText?.append(" on ")
        confirmationText?.append(appointmentDate)
        confirmationText?.append(".")

        val confirm = confirmationView?.findViewById<Button>(R.id.confirmButton)
        val cancel = confirmationView?.findViewById<Button>(R.id.cancelButton)

        val confirmationMessage: TextView = TextView(this)
        confirmationMessage.layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT)

        val params = LinearLayout.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT)
        params.weight = 1.0f
        params.leftMargin = 2
        params.rightMargin = 2
        confirmationMessage.layoutParams = params
        confirmationMessage.visibility = View.GONE
        confirmationMessage.gravity = Gravity.CENTER
        confirmationMessage.textSize = 20.0f
        layout?.addView(confirmationMessage)

        val display = windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        val height = size.y
        confirmationMessage.y = (height + 100).toFloat()

        // Animate all items off screen with confirm starting.
        confirm?.setOnClickListener {

            confirmationMessage.text = getString(R.string.thank_you_message)

            confirm.isEnabled = false
            cancel?.isEnabled = false

            confirm.animate().translationX(-(confirm.x + confirm.width)).withEndAction {

                cancel?.animate()?.translationX(-(cancel.x + cancel.width))?.withEndAction {

                    confirmationText?.animate()?.translationX((-confirmationText.width).toFloat())?.withEndAction {

                        confirmationMessage.animate().translationY(confirmationMessage.y)
                        confirmationMessage.visibility = View.VISIBLE
                        confirmationMessage.animate().translationY(-(height / 4.0f)).withEndAction {

                            try {

                                DataRequest.RequestAppointment(date, appointmentSlotID.toString(), healthCardNumber.toString())

                                Thread.sleep(2000)

                                val mainMenu = Intent(this, MenuOptionsActivity::class.java)
                                mainMenu.putExtra("healthCardNumber", healthCardNumber)
                                startActivity(mainMenu)
                                finish()
                            } catch (e: InterruptedException) {

                                println(e.message)
                            }
                        }
                    }
                }
            }
        }

        // Animate all items off screen with cancel starting.
        cancel?.findViewById<Button>(R.id.cancelButton)?.setOnClickListener {

            confirmationMessage.text = getString(R.string.goodbye_message)

            cancel.isEnabled = false
            confirm?.isEnabled = false

            cancel.animate().translationX(-(cancel.x + cancel.width)).withEndAction {

                confirm?.animate()?.translationX(-(confirm.x + confirm.width))?.withEndAction {

                    confirmationText?.animate()?.translationX((-confirmationText.width).toFloat())?.withEndAction {

                        confirmationMessage.animate().translationY(confirmationMessage.y)
                        confirmationMessage.visibility = View.VISIBLE
                        confirmationMessage.animate().translationY(-(height / 4.0f)).withEndAction {

                            try {
                                Thread.sleep(1700)

                                val mainMenu = Intent(this, MenuOptionsActivity::class.java)
                                mainMenu.putExtra("healthCardNumber", healthCardNumber)
                                startActivity(mainMenu)
                                finish()
                            } catch (e: InterruptedException) {

                                println(e.message)
                            }
                        }
                    }
                }
            }
        }
    }


    /**
     * <p>
     * Creates a string containing the date in a written format. e.g. Monday April 22, 2019
     * </p>
     *
     * @return String   The date in a written string format.
     */
    private fun getDateString(): String {

        val date: String = when (calendar.get(Calendar.DAY_OF_WEEK)) {
            Calendar.SUNDAY -> "Sunday"
            Calendar.MONDAY -> "Monday"
            Calendar.TUESDAY -> "Tuesday"
            Calendar.WEDNESDAY -> "Wednesday"
            Calendar.THURSDAY -> "Thursday"
            Calendar.FRIDAY -> "Friday"
            Calendar.SATURDAY -> "Saturday"
            else -> "Undefined"
        }

        val month: String = when (calendar.get(Calendar.MONTH)) {
            Calendar.JANUARY -> "January"
            Calendar.FEBRUARY -> "February"
            Calendar.MARCH -> "March"
            Calendar.APRIL -> "April"
            Calendar.MAY -> "May"
            Calendar.JUNE -> "JUNE"
            Calendar.JULY -> "JULY"
            Calendar.AUGUST -> "August"
            Calendar.SEPTEMBER -> "September"
            Calendar.OCTOBER -> "October"
            Calendar.NOVEMBER -> "November"
            Calendar.DECEMBER -> "December"

            else -> "Undefined"
        }

        return "$date $month ${calendar.get(Calendar.DATE)}, ${calendar.get(Calendar.YEAR)}"
    }
}