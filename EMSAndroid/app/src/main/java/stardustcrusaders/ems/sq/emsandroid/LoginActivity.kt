package stardustcrusaders.ems.sq.emsandroid

import android.Manifest
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.TargetApi
import android.content.Intent
import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast

import kotlinx.android.synthetic.main.activity_login.*
import java.util.*

/**
 * A login screen that offers login via healthCardNumber/password.
 */
class LoginActivity : AppCompatActivity() {

    companion object {

        // Permissions data.
        const val ASK_PERMISSION_CODE: Int = 1
        const val READ_REQUEST_CODE: Int = 42
        val PERMISSIONS = arrayOf(Manifest.permission.INTERNET)

        /**
         * A dummy authentication store containing known user names and passwords.
         * TODO: remove after connecting to a real authentication system.
         */
        private val DUMMY_CREDENTIALS = arrayOf("foo@example.com:hello", "bar@example.com:world")
    }

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private var mAuthTask: UserLoginTask? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        askForPermissions(PERMISSIONS)

        password.setOnEditorActionListener(TextView.OnEditorActionListener { _, id, _ ->
            if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                attemptLogin()
                return@OnEditorActionListener true
            }
            false
        })

        health_card_number_sign_in_button.setOnClickListener { attemptLogin() }
        create_account_button.setOnClickListener { attempAddMobileLogin() }
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid healthCardNumber, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private fun attemptLogin() {

        if (mAuthTask != null) {
            return
        }

        // Reset errors.
        healthCardNumber.error = null
        password.error = null

        // Store values at the time of the login attempt.
        val healthCardNumberStr = healthCardNumber.text.toString()
        val passwordStr = password.text.toString()

        var cancel = false
        var focusView: View? = null

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(passwordStr) && !isPasswordValid(passwordStr)) {
            password.error = getString(R.string.error_invalid_password)
            focusView = password
            cancel = true
        }

        // Check for a valid health card number.
        if (TextUtils.isEmpty(healthCardNumberStr)) {
            healthCardNumber.error = getString(R.string.error_field_required)
            focusView = healthCardNumber
            cancel = true
        } else if (!isHealthCardNumberValid(healthCardNumberStr)) {
            healthCardNumber.error = getString(R.string.error_invalid_email)
            focusView = healthCardNumber
            cancel = true
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView?.requestFocus()
        } else {

            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.

            showProgress(true)
            mAuthTask = UserLoginTask(healthCardNumberStr, passwordStr)
            mAuthTask!!.execute(null as Void?)
        }
    }


    private fun isHealthCardNumberValid(healthCardNumber: String): Boolean {

        return healthCardNumber.matches(Regex("^[0-9]{10}[A-Za-z]{2}"))
    }

    private fun isPasswordValid(password: String): Boolean {

        return password.length >= 12
    }


    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private fun showProgress(show: Boolean) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            val shortAnimTime = resources.getInteger(android.R.integer.config_shortAnimTime).toLong()

            login_form.visibility = if (show) View.GONE else View.VISIBLE
            login_form.animate()
                    .setDuration(shortAnimTime)
                    .alpha((if (show) 0 else 1).toFloat())
                    .setListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationEnd(animation: Animator) {
                            login_form.visibility = if (show) View.GONE else View.VISIBLE
                        }
                    })

            login_progress.visibility = if (show) View.VISIBLE else View.GONE
            login_progress.animate()
                    .setDuration(shortAnimTime)
                    .alpha((if (show) 1 else 0).toFloat())
                    .setListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationEnd(animation: Animator) {
                            login_progress.visibility = if (show) View.VISIBLE else View.GONE
                        }
                    })
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            login_progress.visibility = if (show) View.VISIBLE else View.GONE
            login_form.visibility = if (show) View.GONE else View.VISIBLE
        }
    }


    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    inner class UserLoginTask internal constructor(private val mHealthCardNumber: String, private val mPassword: String) : AsyncTask<Void, Void, Boolean>() {

        override fun doInBackground(vararg params: Void): Boolean? {

            val response = DataRequest.VerifyMobileLogin(mHealthCardNumber, mPassword)
            val loginResponse = response?.get("verificationResponse").toString()

            return when(loginResponse) {
                "good" -> true
                "bad" ->false
                else -> false
            }
        }

        override fun onPostExecute(success: Boolean?) {

            mAuthTask = null
            showProgress(false)

            if (success!!) {

                val menuIntent = Intent(baseContext, MenuOptionsActivity::class.java)

                val healthCardNumberView = findViewById<EditText>(R.id.healthCardNumber)
                menuIntent.putExtra("healthCardNumber", healthCardNumberView.text.toString())
                startActivity(menuIntent)
            }
            else {

                password.error = getString(R.string.error_incorrect_password)
                password.requestFocus()
            }
        }

        override fun onCancelled() {

            mAuthTask = null
            showProgress(false)
        }
    }

    private fun attempAddMobileLogin() {

        // Store values at the time of the login attempt.
        val healthCardNumberStr = healthCardNumber.text.toString()
        val passwordStr = password.text.toString()

        val response = DataRequest.AddMobileLogin(healthCardNumberStr, passwordStr)
        val responseResult = response?.get("verificationResponse").toString()

        when(responseResult) {
            "accepted" -> Snackbar.make(findViewById<LinearLayout>(R.id.login_layout), "Your account has been created.", Snackbar.LENGTH_LONG).show()
            "already exists" -> Snackbar.make(findViewById<LinearLayout>(R.id.login_layout), "An account with this health card number already exists.", Snackbar.LENGTH_LONG).show()
            "patient does not exist" -> Snackbar.make(findViewById<LinearLayout>(R.id.login_layout), "You must be a patient to create a login. Please speak to a receptionist for more details", Snackbar.LENGTH_LONG).show()
            else -> Snackbar.make(findViewById<LinearLayout>(R.id.login_layout), "An unexpected error has occurred. Please try again later.", Snackbar.LENGTH_LONG).show()
        }
    }


    /**
     *
     * <p>
     *   Checks if permissions are granted, calling the permissions result method if any are not.
     * </p>
     *
     * @param permissionsArray The permissions to check.
     */
    fun askForPermissions(permissionsArray: Array<String>) {

        val missingPermissions = mutableListOf<String>()

        // Iterate over each permission and check if it is granted.
        // If a permission is not granted, add it to the list of permissions to ask for.
        for (permission in permissionsArray) {

            val result = ContextCompat.checkSelfPermission(this, permission)

            if (result != PackageManager.PERMISSION_GRANTED) {

                missingPermissions.add(permission)
            }
        }

        // Request the permissions the the permissions array is not empty.
        // Otherwise, indicate that permissions are granted.
        if (!missingPermissions.isEmpty()) {

            val permissions = missingPermissions.toTypedArray()
            ActivityCompat.requestPermissions(this, permissions, ASK_PERMISSION_CODE)
        }
        else {

            val grantResults = IntArray(permissionsArray.size)
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED)
            onRequestPermissionsResult(ASK_PERMISSION_CODE, permissionsArray, grantResults)
        }
    }


    /**
     * <p>
     *  Asks for permissions.
     * </p>
     * @param requestCode - The permission request.
     * @param permissions - A String array of the permissions to ask for.
     * @param grantResults - Indicates each permission is tobe asked for.
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {

        when (requestCode) {

            ASK_PERMISSION_CODE -> for (index in permissions.indices.reversed()) {
                if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {

                    Snackbar.make(findViewById(R.id.health_card_number_login_form), "" + permissions[index] + " is requred to use this application.", Snackbar.LENGTH_INDEFINITE)

                    Toast.makeText(
                        this, "Required permission '" + permissions[index]
                                + "' not granted, exiting", Toast.LENGTH_LONG
                    ).show()
                    finish()
                    return
                }
            }
        }
    }
}
