/*
 * FILE          : Billing.cs
 * PROJECT       : EMS II
 * PROGRAMMER    : Firas Areibi
 * DESCRIPTION   :
 *      This file contains the billing class.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;

namespace ems
{
    /// <summary>
    /// This struct is used to store billing information.
    /// </summary>
    public struct BillingInfo
    {
        public BillingInfo(string HCN, char sex, int day, int month, int year, List<string> billingCodes)
        {
            this.HCN = HCN;
            this.sex = sex;
            this.day = day;
            this.month = month;
            this.year = year;
            this.billingCodes = billingCodes;
        }

        public string HCN;
        public char sex;
        public int day;
        public int month;
        public int year;
        public List<string> billingCodes;
    }

    
    /// <summary>
    /// This class contains functions to handle billing operations
    /// </summary>
    public static class Billing
    {
        private static List<string> tempList = new List<string>();
        private static int globalwrite = 0;


        /// <summary>
        /// Creates Directories for Billing
        /// </summary>
        public static void CreateBillingDir()
        {
            try
            {
                Directory.CreateDirectory(@"./Billing");
                Directory.CreateDirectory(@"./Billing/MasterFile");
                Directory.CreateDirectory(@"./Billing/BillingFile");
                Directory.CreateDirectory(@"./Billing/ResponseFile");
                Directory.CreateDirectory(@"./Billing/BillingSummary");
            }
            catch (DirectoryNotFoundException)
            {
                Logging.MethodCalled("Billing - Creating Directories", "Failed");
            }
        }


        /// <summary>
        /// This function adds billing codes to a patient 
        /// </summary>
        /// <param name="hcn">hcn for patient codes to attach to</param>
        /// <param name="dateofservice">date of service of appointment</param>
        /// <param name="billingCodes">codes to add to data base</param>
        /// <param name="bInfo">struct of billing in a list</param>
        public static void ConcatCodes(string hcn, string dateofservice, string billingCodes, List<BillingInfo> bInfo)
        {
            if (dateofservice.Length == 8)
            {
                dateofservice = dateofservice.Insert(4, "/");
                dateofservice = dateofservice.Insert(7, "/");
            }

            string[] years = dateofservice.Split('/');

            string[] billingcodes = billingCodes.Split(' ');

            Int32.TryParse(years[0], out int year);
            Int32.TryParse(years[1], out int month);
            Int32.TryParse(years[2], out int day);

            foreach (BillingInfo billingCode in bInfo)
            {
                if (billingCode.day == day && billingCode.month == month && billingCode.year == year && billingCode.HCN == hcn)
                {
                    for (int i = 0; i < billingcodes.Length; i++)
                        billingCode.billingCodes.Add(billingcodes[i]);
                }
            }
        }


        /// <summary>
        /// cycles through the list of billing struct and spits out billing file
        /// </summary>
        /// <param name="bInfo">list of structs containing billing</param>
        /// <param name="filePath">The path for the billing file</param>
        /// <returns>true if successful, false otherwise</returns>
        public static bool EnterInfo(List<BillingInfo> bInfo, string filePath)
        {
            string code = string.Empty;
            string date = string.Empty;
            int count = 0;

            foreach(BillingInfo billingInfo in bInfo)
            {
                if (billingInfo.billingCodes.Count == 0)
                {
                    MessageBox.Show("Some appointments are missing billing codes!", "Billing Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return false;
                }
            }

            foreach (BillingInfo billingCode in bInfo)
            {
                code = string.Empty;
                foreach (var line in billingCode.billingCodes)
                {
                    code += line + " ";
                }

                count = code.Length - 1;

                code = code.Remove(count);

                date = $"{billingCode.year}{billingCode.month}{billingCode.day}";

                if (!MatchCodes(date, billingCode.HCN, billingCode.sex.ToString(), code, filePath))
                {
                    MessageBox.Show("Could not parse Master File!", "Billing Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    File.Delete(filePath);
                    return false;
                }
            }

            globalwrite++;

            return true;
        }


        /// <summary>
        /// Matches biling codes with master file
        /// </summary>
        /// <param name="dateOfService">date of service for patient</param>
        /// <param name="HCN">hcn for patient</param>
        /// <param name="Gender">gender for person</param>
        /// <param name="billingCode">billing codes to match</param>
        /// <param name="filePath">The path for the billing file</param>
        /// <returns>True if successful, false otherwise</returns>
        private static bool MatchCodes(string dateOfService, string HCN, string Gender, string billingCode, string filePath)
        {
            try
            {
                string value = "";
                //string billingCode = "";
                string[] codes = File.ReadAllLines(@"./Billing/MasterFile/MasterFile.txt");
                string[] billingCodes = billingCode.Split(' ');
                string[] tempHold = new string[codes.Length];
                string[] holdingZeros = new string[tempHold.Length];
                string[] billingFee = new string[codes.Length];
                string contents = string.Empty;
                tempList.Clear();

                for (int i = 0; i < billingCodes.Length; i++)
                {
                    for (int j = 0; j < codes.Length; j++)
                    {
                        string line = codes[j].Trim();

                        //see if billing code matches fee
                        if (line.Contains(billingCodes[i]))
                        {
                            //if length is less then these numbers fee =0
                            if (line.Length == 12 || line.Length == 20)
                            {
                                value = "00000000000";
                                tempList.Add(value);
                                
                                return false;
                            }
                            else
                            {
                                Regex first12CharPatt = new Regex(@"[A-Z]{1}[0-9]{11}");
                                tempHold = first12CharPatt.Split(line);
                                tempList.Add(tempHold[1]);
                                break;
                            }
                        }
                    }
                }
                tempHold = tempList.ToArray();
                tempList.Clear();
                foreach (var line in tempHold)
                {
                    if (line.Length == 11)
                    {
                        //Regex zeroParse = new Regex(@"[0]{4}");
                        //holdingZeros = zeroParse.Split(line, 2);
                        //string tempstr = holdingZeros[1];
                        //tempstr = tempstr.Insert(5, ".");
                        //holdingZeros = tempstr.Split('.');
                        //tempList.Add(holdingZeros[0]);
                        tempList.Add(line);
                    }
                    else if (line.Length == 19)
                    {
                        Regex zeroParse = new Regex(@"[0-9]{8}");
                        holdingZeros = zeroParse.Split(line, 2);
                        tempList.Add(holdingZeros[1]);
                    }
                    else if (line.Length == 22)
                    {
                        MatchCollection a = Regex.Matches(line, @"[0-9]{11}");
                        foreach (Match lines in a)
                        {

                            holdingZeros[1] = lines.Value;
                            break;

                        }
                        tempList.Add(holdingZeros[1]);
                        //tempList.Add(line);
                    }
                    else if (line.Length == 30)
                    {
                        Regex zeroParse = new Regex(@"[0-9]{8}");
                        holdingZeros = zeroParse.Split(line, 2);
                        MatchCollection a = Regex.Matches(holdingZeros[1], @"[0-9]{11}");
                        foreach (Match lines in a)
                        {

                            holdingZeros[1] = lines.Value;
                            break;

                        }

                        tempList.Add(holdingZeros[1]);
                    }
                    else if (line.Length == 33)
                    {
                        MatchCollection a = Regex.Matches(line, @"[0-9]{11}");
                        foreach (Match lines in a)
                        {

                            holdingZeros[1] = lines.Value;
                            break;

                        }
                        tempList.Add(holdingZeros[1]);
                        //tempList.Add(line);
                    }
                    else if (line.Length == 41)
                    {
                        Regex zeroParse = new Regex(@"[0-9]{8}");
                        holdingZeros = zeroParse.Split(line, 2);
                        MatchCollection a = Regex.Matches(holdingZeros[1], @"[0-9]{11}");
                        foreach (Match lines in a)
                        {

                            holdingZeros[1] = lines.Value;
                            break;

                        }
                        tempList.Add(holdingZeros[1]);
                    }
                    else if (line.Length == 44)
                    {
                        //Regex zeroParse = new Regex(@"[0]{4}");
                        //holdingZeros = zeroParse.Split(line, 2);
                        //holdingZeros[0] = holdingZeros[1].Insert(5, ".");
                        //holdingZeros = holdingZeros[0].Split('.');
                        //tempList.Add(holdingZeros[0]);
                        MatchCollection a = Regex.Matches(line, @"[0-9]{11}");
                        foreach (Match lines in a)
                        {
                            holdingZeros[1] = lines.Value;
                            break;
                        }
                        tempList.Add(holdingZeros[1]);
                        //tempList.Add(line);

                    }
                    else if (line.Length == 52)
                    {
                        Regex zeroParse = new Regex(@"[0-9]{8}");
                        holdingZeros = zeroParse.Split(line, 2);
                        MatchCollection a = Regex.Matches(holdingZeros[1], @"[0-9]{11}");
                        foreach (Match lines in a)
                        {
                            holdingZeros[1] = lines.Value;
                            break;
                        }

                        tempList.Add(holdingZeros[1]);
                    }
                }
                //if want user doesnt want all codes parse here 
                billingFee = tempList.ToArray();
                tempList.Clear();
                for (int i = 0; i < billingCodes.Length; i++)
                {
                    //once 
                    contents = $"{dateOfService}{HCN}{Gender}{billingCodes[i]}{billingFee[i]}";
                    CreateBillingFile(contents, filePath);
                }
            }
            catch (IOException e)
            {
                Logging.MethodCalled(e.Message, "Failed");
            }
            catch (Exception e)
            {
                Logging.MethodCalled(e.Message, "Failed");
            }

            return true;
        }


        /// <summary>
        /// Creates billing file 
        /// </summary>
        /// <param name="contents">contents for the file</param>
        /// <param name="filePath">The path for the billing file</param>
        private static void CreateBillingFile(string contents, string filePath)
        {
            try
            {
                string tempcont = contents;
                tempcont=tempcont.Insert(6,".");
                tempcont = tempcont.Insert(4,".");
                string[] date = tempcont.Split('.');
                if (!File.Exists(filePath))
                {
                    FileStream billingFile = File.Create(filePath);
                    billingFile.Close();
                    //logging.MethodCalled("Creating billing file", "Succesful");
                }

                InsertToBillingFile(filePath, contents);

            }
            catch (Exception)
            {
                Logging.MethodCalled("Creating billing file", "Failed");

            }
        }


        /// <summary>
        /// insert contents into billing file
        /// </summary>
        /// <param name="filePath">file path for inserting</param>
        /// <param name="contents">contents of the billing file</param>
        private static void InsertToBillingFile(string filePath, string contents)
        {
            try
            {
                if (globalwrite == 0)
                {
                    File.AppendAllText(filePath, contents + Environment.NewLine);
                }
                else
                {
                    File.WriteAllText(filePath, contents + Environment.NewLine);
                    globalwrite = 0;

                }

                Logging.MethodCalled("Appending to billing file", "Successful");
            }
            catch (IOException)
            {
                Logging.MethodCalled("Appending to billing file failed", "Failed");
            }
        }


        /// <summary>
        /// Reading from the response file
        /// </summary>
        /// <param name="path">path for the response</param>
        /// <returns>True if successful, false otherwise</returns>
        public static bool ReadResponseFile(string path, out string outputPath)
        {
            double encounters = 0;
            double encounterFhcvCmoh = 0;
            string[] parsingValues = new string[] { }; ;
            string[] parsingValuesOutOfStates = new string[] { };
            string[] insertingDecimalInString = new string[] { };
            string[] insertDecimalInString = new string[] { };
            int[] values = new int[] { };
            int[] allValues = new int[] { };
            string dateofService = string.Empty;
            double requestedSum = 0.0;
            double paidSum = 0.0;
            double averageBilling = 0.0;
            double receivedPercent = 0.0;
            double totalBilledProcedure = 0.0;
            outputPath = "";

            try
            {
                using (StreamReader r = new StreamReader(path))
                {
                    //how many encounters are in file
                    while (r.ReadLine() != null)
                    {

                        encounters++;
                    }
                    r.Close();
                }

                string[] invoiced = File.ReadAllLines(path);
                for (int i = 0; i < invoiced.Length; i++)
                {
                    string line = invoiced[i].Trim();

                    if (line != "")
                    {
                        //counts states
                        if (line.Contains("CMOH") || line.Contains("FHCV"))
                        {
                            encounterFhcvCmoh++;
                        }

                        //this will parse date, hcn,and the billing code out of the line
                        //201711201234567890 KVFA 6650000
                        Regex patt = new Regex(@"[0-9]{18}[A-Z]{4}[0-9]{7}");
                        parsingValues = patt.Split(line);
                        string tempstring = parsingValues[1];
                        tempList.Add(tempstring);
                        if (dateofService == string.Empty)
                        {
                            dateofService = Regex.Match(line, "[0-9]{6}").Value;
                        }
                    }
                }

                Array.Resize(ref parsingValues, tempList.Count + 1);

                parsingValues = tempList.ToArray();
                tempList.Clear();

                //parse out paid values from response
                foreach (var line in parsingValues)
                {
                    string[] tempStringHold = new string[parsingValues.Length];
                    if (line.Length == 0)
                    {

                    }
                    else
                    {
                        if (line.Contains("PAID"))
                        {
                            Regex states = new Regex(@"[PAID]{4}");
                            parsingValuesOutOfStates = states.Split(line, 2);
                            string tempString = parsingValuesOutOfStates[0];

                            if (tempString.Length == 7)
                            {
                                tempList.Add(tempString);
                            }
                            else if (tempString.Length == 18)
                            {
                                tempString = tempString.Insert(7, "-");
                                tempStringHold = tempString.Split(new string[] { "-0000" }, StringSplitOptions.RemoveEmptyEntries);

                                for (int i = 0; i < tempStringHold.Length; i++)
                                {
                                    tempList.Add(tempStringHold[i]);
                                }
                            }
                            else if (tempString.Length == 29)
                            {
                                tempString = tempString.Insert(7, "-");
                                tempString = tempString.Insert(19, "-");
                                tempStringHold = tempString.Split(new string[] { "-0000" }, StringSplitOptions.RemoveEmptyEntries);

                                for (int i = 0; i < tempStringHold.Length; i++)
                                {
                                    tempList.Add(tempStringHold[i]);
                                }
                            }
                            else if (tempString.Length == 40)
                            {
                                tempString = tempString.Insert(7, "-");
                                tempString = tempString.Insert(19, "-");
                                tempString = tempString.Insert(31, "-");
                                tempStringHold = tempString.Split(new string[] { "-0000" }, StringSplitOptions.RemoveEmptyEntries);

                                for (int i = 0; i < tempStringHold.Length; i++)
                                {
                                    tempList.Add(tempStringHold[i]);
                                }

                            }


                            //tempList.Add();
                        }
                    }
                }
                Array.Resize(ref insertingDecimalInString, tempList.Count + 1);

                insertingDecimalInString = tempList.ToArray();
                tempList.Clear();

                //parses out other state codes attached to response file
                foreach (var line in parsingValues)
                {
                    if (!line.Contains("PAID"))
                    {


                        if (line.Contains("DECL"))
                        {
                            Regex states = new Regex(@"[DECL]{4}");
                            insertDecimalInString = states.Split(line, 2);

                            string tempStrings = insertDecimalInString[0];
                            string[] tempStringHold = new string[parsingValues.Length];
                            if (tempStrings.Length == 7)
                            {
                                tempList.Add(tempStrings);
                            }
                            else if (tempStrings.Length == 18)
                            {
                                tempStrings = tempStrings.Insert(7, "-");
                                tempStringHold = tempStrings.Split(new string[] { "-0000" }, StringSplitOptions.RemoveEmptyEntries);

                                for (int i = 0; i < tempStringHold.Length; i++)
                                {
                                    tempList.Add(tempStringHold[i]);
                                }
                            }
                            else if (tempStrings.Length == 29)
                            {
                                tempStrings = tempStrings.Insert(7, "-");
                                tempStrings = tempStrings.Insert(19, "-");
                                tempStringHold = tempStrings.Split(new string[] { "-0000" }, StringSplitOptions.RemoveEmptyEntries);

                                for (int i = 0; i < tempStringHold.Length; i++)
                                {
                                    tempList.Add(tempStringHold[i]);
                                }
                            }
                            else if (tempStrings.Length == 40)
                            {
                                tempStrings = tempStrings.Insert(7, "-");
                                tempStrings = tempStrings.Insert(19, "-");
                                tempStrings = tempStrings.Insert(31, "-");
                                tempStringHold = tempStrings.Split(new string[] { "-0000" }, StringSplitOptions.RemoveEmptyEntries);

                                for (int i = 0; i < tempStringHold.Length; i++)
                                {
                                    tempList.Add(tempStringHold[i]);
                                }

                            }


                        }
                        else if (line.Contains("FHCV"))
                        {
                            Regex states = new Regex(@"[FHCV]{4}");
                            insertDecimalInString = states.Split(line, 2);

                            string tempStrings = insertDecimalInString[0];
                            string[] tempStringHold = new string[parsingValues.Length];
                            if (tempStrings.Length == 7)
                            {
                                tempList.Add(tempStrings);
                            }
                            else if (tempStrings.Length == 18)
                            {
                                tempStrings = tempStrings.Insert(7, "-");
                                tempStringHold = tempStrings.Split(new string[] { "-0000" }, StringSplitOptions.RemoveEmptyEntries);

                                for (int i = 0; i < tempStringHold.Length; i++)
                                {
                                    tempList.Add(tempStringHold[i]);
                                }
                            }
                            else if (tempStrings.Length == 29)
                            {
                                tempStrings = tempStrings.Insert(7, "-");
                                tempStrings = tempStrings.Insert(19, "-");
                                tempStringHold = tempStrings.Split(new string[] { "-0000" }, StringSplitOptions.RemoveEmptyEntries);

                                for (int i = 0; i < tempStringHold.Length; i++)
                                {
                                    tempList.Add(tempStringHold[i]);
                                }
                            }
                            else if (tempStrings.Length == 40)
                            {
                                tempStrings = tempStrings.Insert(7, "-");
                                tempStrings = tempStrings.Insert(19, "-");
                                tempStrings = tempStrings.Insert(31, "-");
                                tempStringHold = tempStrings.Split(new string[] { "-0000" }, StringSplitOptions.RemoveEmptyEntries);

                                for (int i = 0; i < tempStringHold.Length; i++)
                                {
                                    tempList.Add(tempStringHold[i]);
                                }

                            }
                        }
                        else if (line.Contains("CMOH"))
                        {
                            Regex states = new Regex(@"[CMOH]{4}");
                            insertDecimalInString = states.Split(line, 2);

                            string tempStrings = insertDecimalInString[0];
                            string[] tempStringHold = new string[parsingValues.Length];
                            if (tempStrings.Length == 7)
                            {
                                tempList.Add(tempStrings);
                            }
                            else if (tempStrings.Length == 18)
                            {
                                tempStrings = tempStrings.Insert(7, "-");
                                tempStringHold = tempStrings.Split(new string[] { "-0000" }, StringSplitOptions.RemoveEmptyEntries);

                                for (int i = 0; i < tempStringHold.Length; i++)
                                {
                                    tempList.Add(tempStringHold[i]);
                                }
                            }
                            else if (tempStrings.Length == 29)
                            {
                                tempStrings = tempStrings.Insert(7, "-");
                                tempStrings = tempStrings.Insert(19, "-");
                                tempStringHold = tempStrings.Split(new string[] { "-0000" }, StringSplitOptions.RemoveEmptyEntries);

                                for (int i = 0; i < tempStringHold.Length; i++)
                                {
                                    tempList.Add(tempStringHold[i]);
                                }
                            }
                            else if (tempStrings.Length == 40)
                            {
                                tempStrings = tempStrings.Insert(7, "-");
                                tempStrings = tempStrings.Insert(19, "-");
                                tempStrings = tempStrings.Insert(31, "-");
                                tempStringHold = tempStrings.Split(new string[] { "-0000" }, StringSplitOptions.RemoveEmptyEntries);

                                for (int i = 0; i < tempStringHold.Length; i++)
                                {
                                    tempList.Add(tempStringHold[i]);
                                }

                            }
                        }
                        //string tempString = insertDecimalInString[0];
                        //tempList.Add(tempString);
                    }
                }

                Array.Resize(ref insertDecimalInString, tempList.Count + 1);

                insertDecimalInString = tempList.ToArray();
                tempList.Clear();

                for (int i = 0; i < insertDecimalInString.Length; i++)
                {
                    double value = 0.0;
                    string temp = insertDecimalInString[i].Insert(3, ".");
                    string[] cleanUpNumbers = temp.Split(new string[] { "0000" }, StringSplitOptions.RemoveEmptyEntries);
                    temp = cleanUpNumbers[0];
                    if (temp.StartsWith("0"))
                    {
                        cleanUpNumbers[0] = temp.TrimStart('0');
                        //cleanUpNumbers = temp.Split(new string[] { "0" }, StringSplitOptions.RemoveEmptyEntries);
                    }
                    temp = cleanUpNumbers[0];
                    if (double.TryParse(temp, out value))
                    {
                        requestedSum += value;
                    }

                    // requestedSum += value;


                    //allValues[i] = Convert.ToInt32(insertDecimalInString[i].Insert(3, "."));
                    //requestedSum += allValues[i];
                }


                //RECEIVED TOTALS
                for (int i = 0; i < insertingDecimalInString.Length; i++)
                {
                    double value = 0.0;
                    string temp = insertingDecimalInString[i].Insert(3, ".");
                    string[] cleanUpNumbers = temp.Split(new string[] { "0000" }, StringSplitOptions.RemoveEmptyEntries);
                    temp = cleanUpNumbers[0];
                    if (temp.StartsWith("0"))
                    {
                        cleanUpNumbers[0] = temp.TrimStart('0');//Split(new string[] { "0" }, StringSplitOptions.RemoveEmptyEntries);
                    }
                    temp = cleanUpNumbers[0];

                    if (double.TryParse(temp, out value))
                    {
                        paidSum += value;
                    }

                }

                //insert decimal 3 spaces after
                /*foreach (var line in parsingValuesOutOfStates)
                {

                    for (int i = 0; line != null; i++)
                    {
                        insertingDecimalInString[i] = line.Insert(3, ".");
                        values[i] = Convert.ToInt32(insertingDecimalInString[i]);
                        //gather recieved total
                        paidSum += values[i];
                    }
                }*/

                //total encounters
                //encounters

                //totalbilledprocedure
                totalBilledProcedure = paidSum + requestedSum;

                //received total
                //Paidsum

                //RECEIVED % 
                receivedPercent = (paidSum / (paidSum + requestedSum) * 100);

                //averagebilling rt/teb
                averageBilling = (paidSum / encounters);

                //# of encounters for states(cmoh, fhcv)
                //encounterFhcvCmoh

                //Monthly billing summary method below
                outputPath = CreateMonthlyBillingSummary(dateofService, encounters, totalBilledProcedure, paidSum, receivedPercent, averageBilling, encounterFhcvCmoh);
            }
            catch (IOException)
            {
                //log error
                Logging.MethodCalled("Reading/Loading response file", "Failed");
                return false;
            }
            catch (Exception e)
            {
                Logging.MethodCalled(e.Message, "Failed");
                return false;
            }

            return true;
        }


        /// <summary>
        /// Creates monthly billing summary
        /// </summary>
        /// <param name="dateOfService">date of service for summary</param>
        /// <param name="encounters">encounters for billing summary</param>
        /// <param name="totalBilled">total billed for billing summary</param>
        /// <param name="receivedTotal">recieved total for billing summary</param>
        /// <param name="receivedPercentage">recieved percentage for billing summary</param>
        /// <param name="averageBilling">average billing for summary</param>
        /// <param name="numberOfStateEncounters">state encounters of billing</param>
        /// <returns>The output summary file</returns>
        private static string CreateMonthlyBillingSummary(string dateOfService, double encounters, double totalBilled, double receivedTotal, double receivedPercentage, double averageBilling, double numberOfStateEncounters)
        {

            dateOfService = dateOfService.Insert(4, "-");
            receivedPercentage = Math.Round(receivedPercentage, 2);
            averageBilling = Math.Round(averageBilling, 2);


            string newPath = "./Billing/BillingSummary";
            //int index = path.LastIndexOf('/');
            string content = $"Total Encounters Billed: {encounters}{Environment.NewLine}" +
                             $"Total Billed Procedures: {totalBilled}{Environment.NewLine}" +
                             $"Received Total: {receivedTotal}{Environment.NewLine}" +
                             $"Received Precentage: {receivedPercentage}{Environment.NewLine}" +
                             $"Average Billing: {averageBilling}{Environment.NewLine}" +
                             $"Encounters to Follow-up: {numberOfStateEncounters}";
            /*if (index!=-1)
            {
                newPath = path.Substring(0, index);
            }*/
            newPath = newPath + @"/BillingSummary-" + dateOfService + ".txt";

            if (!File.Exists(newPath))
            {
                FileStream billingSummary = File.Create(newPath);
                billingSummary.Close();
            }

            File.WriteAllText(newPath, content);

            return newPath;
        }
    }
}