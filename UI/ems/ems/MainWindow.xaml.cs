﻿/*
 * FILE             : MainWindow.xaml.cs
 * FIRST VERSION    : March 13, 2019
 * PROGRAMMERS      : William Bicknell, Jacob Funes
 * DESCRIPTION      :
 *      The UI for the EMS II application.
 */

using System;
using System.Windows;
using System.Windows.Controls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.IO;
using System.Media;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Input;
using System.Threading;
using Microsoft.Win32;

namespace ems
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const int ADD_PATIENT_TAB_INDEX = 0;
        const int SEARCH_PATIENT_TAB_INDEX = 1;
        const int BOOK_APT_TAB_INDEX = 0;
        const int SEARCH_APT_TAB_INDEX = 1;
        const int BILLING_SUMMARY_TAB_INDEX = 0;
        const int BILLING_FILE_TAB_INDEX = 1;
        const int APP_MANAGEMENT_TAB_INDEX = 0;
        const int USER_MANAGEMENT_TAB_INDEX = 1;
        const char ADMIN_TYPE = 'A';
        const int APT_REQUEST_SLEEP = 5000;
        const int APT_CHECKIN_SLEEP = 10000;
        int numAptRequests;
        volatile bool exitThread;

        ThreadStart aptRequestThreadRef;
        Thread aptRequestThread;

        ThreadStart aptCheckinThreadRef;
        Thread aptCheckinThread;

        List<KeyValuePair<DateTime, List<int>>> takenDates;
        PatientInfo selectedPatient;

        public MainWindow()
        {
            InitializeComponent();

            Closing += OnWindowClosing;

            selectedPatient = new PatientInfo();
            takenDates = new List<KeyValuePair<DateTime, List<int>>>();
            numAptRequests = 0;
            exitThread = false;

            calAptSchedule.BlackoutDates.AddDatesInPast();

            UpdateFilledTimeslots();
            Logging.CreateDirectory();
            Billing.CreateBillingDir();

            aptRequestThreadRef = new ThreadStart(AptRequestUpdate);
            aptRequestThread = new Thread(aptRequestThreadRef);
            aptRequestThread.Start();

            aptCheckinThreadRef = new ThreadStart(AptCheckinUpdate);
            aptCheckinThread = new Thread(aptCheckinThreadRef);
            aptCheckinThread.Start();

            // Play intro music
            //using (FileStream stream = File.Open(@"./amiga.wav", FileMode.Open))
            //{
            //    SoundPlayer introMusic = new SoundPlayer(stream);
            //    introMusic.Load();
            //    introMusic.Play();
            //}
        }

        #region Appointment Checkins

        /// <summary>
        /// Thread function to update appointment checkins
        /// </summary>
        private void AptCheckinUpdate()
        {
            while (!exitThread)
            {
                Dispatcher.BeginInvoke(new Action(delegate ()
                {
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
                    {
                        SqlCommand cmd = new SqlCommand("GetCurrentCheckins", conn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        conn.Open();

                        DataTable table = new DataTable();
                        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                        adapter.Fill(table);

                        dtaAptCheckins.ItemsSource = table.DefaultView;
                    }
                }));

                Thread.Sleep(APT_CHECKIN_SLEEP);
            }
        }

        /// <summary>
        /// Enable checkin button
        /// </summary>
        /// <param name="sender">The object that raised the event</param>
        /// <param name="e">Arguments for the event</param>
        private void dtaAptCheckins_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnCheckin.IsEnabled = dtaAptCheckins.SelectedItem != null;
        }


        /// <summary>
        /// Check in an appointment
        /// </summary>
        /// <param name="sender">The object that raised the event</param>
        /// <param name="e">Arguments for the event</param>
        private void btnCheckin_Click(object sender, RoutedEventArgs e)
        {
            DataRowView row = dtaAptCheckins.SelectedItems[0] as DataRowView;

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("CheckinApt", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("AptID", Int32.Parse(row["ID"].ToString()));

                conn.Open();
                cmd.ExecuteNonQuery();
            }

            row["Checked In"] = true;
        }

        #endregion  

        #region Appointment Requests

        /// <summary>
        /// Thread function to check how many appointment requests are pending
        /// </summary>
        private void AptRequestUpdate()
        {
            while (!exitThread)
            {
                Dispatcher.BeginInvoke(new Action(delegate ()
                {

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
                    {
                        SqlCommand cmd = new SqlCommand("GetAppReq", conn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        conn.Open();

                        DataTable table = new DataTable();
                        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                        adapter.Fill(table);

                        if (table.Rows.Count != numAptRequests)
                        {
                            dtaAptRequests.ItemsSource = table.DefaultView;
                            numAptRequests = table.Rows.Count;
                        }
                    }

                    lblPendingApt.Content = "Pending Appointment Requests: " + numAptRequests;
                    tabAptRequests.Header = "Appointment Requests (" + numAptRequests + ")";
                }));

                Thread.Sleep(APT_REQUEST_SLEEP);
            }
        }


        /// <summary>
        /// Enable Accept Appointment button when row selected
        /// </summary>
        /// <param name="sender">The object that raised the event</param>
        /// <param name="e">Arguments for the event</param>
        private void dtaAptRequests_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnAcceptApt.IsEnabled = dtaAptRequests.SelectedItem != null;
        }


        /// <summary>
        /// Accept all current appointment requests
        /// </summary>
        /// <param name="sender">The object that raised the event</param>
        /// <param name="e">Arguments for the event</param>
        private void btnAcceptAllApt_Click(object sender, RoutedEventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            {
                conn.Open();

                foreach (DataRowView row in dtaAptRequests.Items)
                {
                    DateTime date = DateTime.ParseExact(row["Date"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture);

                    SqlCommand cmd = new SqlCommand("InsertAppointment", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@HCN", row["HCN"].ToString());
                    cmd.Parameters.AddWithValue("@AppointmentDate", date.ToShortDateString());
                    cmd.Parameters.AddWithValue("@TimeSlot", Int32.Parse(row["Time Slot"].ToString()));

                    SqlCommand cmdDel = new SqlCommand("DeleteAppointmentRequest", conn);
                    cmdDel.CommandType = CommandType.StoredProcedure;
                    cmdDel.Parameters.AddWithValue("RequestID", Int32.Parse(row["ID"].ToString()));

                    cmd.ExecuteNonQuery();
                    cmdDel.ExecuteNonQuery();

                    dtaAptRequests.Items.Remove(row);
                }

                Logging.MethodCalled("Booked an appointment from mobile appointment request", "SUCCESSFUL");
            }

            UpdateFilledTimeslots();
        }


        /// <summary>
        /// Accept currently selected appointment request
        /// </summary>
        /// <param name="sender">The object that raised the event</param>
        /// <param name="e">Arguments for the event</param>
        private void btnAcceptApt_Click(object sender, RoutedEventArgs e)
        {
            DataRowView row = dtaAptRequests.SelectedItems[0] as DataRowView;
            DateTime date = DateTime.ParseExact(row["Date"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture);

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("InsertAppointment", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@HCN", row["HCN"].ToString());
                cmd.Parameters.AddWithValue("@AppointmentDate", date.ToShortDateString());
                cmd.Parameters.AddWithValue("@TimeSlot", Int32.Parse(row["Time Slot"].ToString()));

                SqlCommand cmdDel = new SqlCommand("DeleteAppointmentRequest", conn);
                cmdDel.CommandType = CommandType.StoredProcedure;
                cmdDel.Parameters.AddWithValue("RequestID", Int32.Parse(row["ID"].ToString()));

                conn.Open();
                cmd.ExecuteNonQuery();
                cmdDel.ExecuteNonQuery();
            }

            dtaAptRequests.SelectedItem = null;
            dtaAptRequests.Items.Remove(row);
            UpdateFilledTimeslots();

            Logging.MethodCalled("Booked an appointment from mobile appointment request", "SUCCESSFUL");
        }

        #endregion

        #region Patient Management Events

        /// <summary>
        /// Clear all fields in the patient search window.
        /// </summary>
        /// <param name="sender">The object that raised the event</param>
        /// <param name="e">Arguments for the event</param>
        private void btnClearAddPatient_Click(object sender, RoutedEventArgs e)
        {
            ClearPatientInfoFields();
            ClearPatientInfoErrors();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender">The object that raised the event</param>
        /// <param name="e">Arguments for the event</param>
        private void txtHOH_TextChanged(object sender, TextChangedEventArgs e)
        {
            bool useHOH = txtHOH.Text.Length > 0;

            txtAddress1.IsEnabled = !useHOH;
            txtAddress2.IsEnabled = !useHOH;
            txtPostal.IsEnabled = !useHOH;
            txtCity.IsEnabled = !useHOH;
            cmbProvince.IsEnabled = !useHOH;
            txtPhone.IsEnabled = !useHOH;

            if (txtHOH.Text.Length == txtHOH.MaxLength)
            {
                lblHOHError.Content = "";

                if (!Demographics.HCNValidator(txtHOH.Text))
                {
                    lblHOHError.Content = "Expected HOH in form 0123456789AA";
                }
                else if (!Demographics.CheckHCNExists(txtHOH.Text))
                {
                    lblHOHError.Content = "No patient exists with this HCN!";
                }
                else
                {
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
                    {
                        SqlCommand cmd = new SqlCommand("GetHOHInfo", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("HOH", txtHOH.Text);

                        conn.Open();

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                txtAddress1.Text = reader["AddressLine1"].ToString();
                                txtAddress2.Text = reader["AddressLine2"].ToString();
                                txtCity.Text = reader["City"].ToString();
                                txtPostal.Text = reader["PostalCode"].ToString();
                                txtPhone.Text = reader["PhoneNumber"].ToString();

                                string province = reader["Province"].ToString();
                                foreach (ComboBoxItem item in cmbProvince.Items)
                                {
                                    if (item.Content.ToString() == province)
                                    {
                                        cmbProvince.SelectedIndex = cmbProvince.Items.IndexOf(item);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        /// <summary>
        /// Add patient to database if all fields are valid.
        /// </summary>
        /// <param name="sender">The object that raised the event</param>
        /// <param name="e">Arguments for the event</param>
        private void btnSubmitAddPatient_Click(object sender, RoutedEventArgs e)
        {
            if (ValidatePatientInfo())
            {
                char gender = cmbGender.Text[0];

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("InsertPatient", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@HealthCardNumber", txtHCN.Text);
                    cmd.Parameters.AddWithValue("@HeadOfHouse", txtHOH.Text);
                    cmd.Parameters.AddWithValue("@FirstName", txtFirstName.Text);
                    cmd.Parameters.AddWithValue("@LastName", txtLastName.Text);
                    cmd.Parameters.AddWithValue("@MiddleInitial", txtMiddleInitial.Text);
                    cmd.Parameters.AddWithValue("@DateOfBirth", txtDOB.Text);
                    cmd.Parameters.AddWithValue("@Gender", gender);
                    cmd.Parameters.AddWithValue("@AddressLine1", txtAddress1.Text);
                    cmd.Parameters.AddWithValue("@AddressLine2", txtAddress2.Text);
                    cmd.Parameters.AddWithValue("@City", txtCity.Text);
                    cmd.Parameters.AddWithValue("@Province", cmbProvince.Text);
                    cmd.Parameters.AddWithValue("@PostalCode", txtPostal.Text);
                    cmd.Parameters.AddWithValue("@PhoneNumber", txtPhone.Text);

                    conn.Open();
                    cmd.ExecuteNonQuery();
                }

                selectedPatient = new PatientInfo(txtHCN.Text, txtFirstName.Text, txtLastName.Text, txtAddress1.Text, txtPhone.Text, txtHOH.Text);
                lblSelectedPatient.Content = "Selected Patient: " + txtLastName.Text + ", " + txtFirstName.Text + " (" + txtHCN.Text + ")";

                ClearPatientInfoFields();
                
                Logging.MethodCalled("Patient added to database", "SUCCESSFUL");
            }
        }


        /// <summary>
        /// Performs the patient search and displays the results.
        /// </summary>
        /// <param name="sender">The object that raised the event</param>
        /// <param name="e">Arguments for the event</param>
        private void btnSearchPatient_Click(object sender, RoutedEventArgs e)
        {
            bool valid = true;
            string HCN = "";
            string lastName = "";

            lblSearchPatientError.Content = "";

            if (txtPatientSearch.Text != "")
            {
                if (radPatientSearchHCN.IsChecked.Value)
                {
                    if (!Demographics.HCNValidator(txtPatientSearch.Text))
                    {
                        valid = false;
                        lblSearchPatientError.Content = "Expected HCN in form 0123456789AA";
                    }
                    else
                    {
                        HCN = txtPatientSearch.Text;
                    }
                }
                else if (radPatientSearchName.IsChecked.Value)
                {
                    if (!Demographics.OnlyLettersValidator(txtPatientSearch.Text))
                    {
                        valid = false;
                        lblSearchPatientError.Content = "Last name must only contain letters!";
                    }
                    else
                    {
                        lastName = txtPatientSearch.Text;
                    }
                }
            }

            if (valid)
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("SearchPatient", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@HCN", HCN);
                    cmd.Parameters.AddWithValue("@LastName", lastName);

                    conn.Open();

                    DataTable table = new DataTable();
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(table);

                    dtaPatientSearch.ItemsSource = table.DefaultView;
                }
            }
        }


        /// <summary>
        /// Reset all patient info fields
        /// </summary>
        private void ClearPatientInfoFields()
        {
            txtHCN.Text = "";
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtMiddleInitial.Text = "";
            txtDOB.Text = "";
            cmbGender.SelectedIndex = 0;
            txtHOH.Text = "";
            txtAddress1.Text = "";
            txtAddress2.Text = "";
            txtCity.Text = "";
            cmbProvince.SelectedIndex = 0;
            txtPostal.Text = "";
            txtPhone.Text = "";
        }


        /// <summary>
        /// Reset all patient info error messages
        /// </summary>
        private void ClearPatientInfoErrors()
        {
            lblHCNError.Content = "";
            lblFNameError.Content = "";
            lblLNameError.Content = "";
            lblMInitialError.Content = "";
            lblDOBError.Content = "";
            lblHOHError.Content = "";
            lblAddress1Error.Content = "";
            lblAddress2Error.Content = "";
            lblCityError.Content = "";
            lblPostalError.Content = "";
            lblPhoneError.Content = "";
        }


        /// <summary>
        /// Validate all patient information fields
        /// </summary>
        /// <returns>True if all patient info is valid, false otherwise</returns>
        private bool ValidatePatientInfo()
        {
            bool valid = true;

            ClearPatientInfoErrors();

            // HCN
            if (txtHCN.Text == "")
            {
                lblHCNError.Content = "HCN is required!";
                valid = false;
            }
            else if (!Demographics.HCNValidator(txtHCN.Text))
            {
                lblHCNError.Content = "Expected HCN in form 0123456789AA";
                valid = false;
            }
            else if (Demographics.CheckHCNExists(txtHCN.Text))
            {
                lblHCNError.Content = "Duplicate HCN detected!";
                valid = false;
            }

            // First name
            if (txtFirstName.Text == "")
            {
                lblFNameError.Content = "First name is required!";
                valid = false;
            }
            else if (!Demographics.NameValidator(txtFirstName.Text))
            {
                lblFNameError.Content = "First name may only contain letters!";
                valid = false;
            }

            // Last name
            if (txtLastName.Text == "")
            {
                lblLNameError.Content = "Last name is required!";
                valid = false;
            }
            else if (!Demographics.NameValidator(txtLastName.Text))
            {
                lblLNameError.Content = "Last name may only contain letters!";
                valid = false;
            }

            // Middle initial
            if (!Demographics.OnlyLettersValidator(txtMiddleInitial.Text))
            {
                lblMInitialError.Content = "Middle initial must be a letter!";
                valid = false;
            }

            // DOB
            if (txtDOB.Text == "")
            {
                lblDOBError.Content = "DOB is required!";
                valid = false;
            }
            else if (!Demographics.DoBValidator(txtDOB.Text))
            {
                lblDOBError.Content = "Expected DOB in form mm/dd/yyyy";
                valid = false;
            }

            // Phone number
            if (txtPhone.Text == "")
            {
                lblPhoneError.Content = "Phone number is required!";
                valid = false;
            }
            else if (!Demographics.PhoneValidator(txtPhone.Text))
            {
                lblPhoneError.Content = "Expected phone number in form\n111-111-1111";
                valid = false;
            }

            // Address line 1
            if (txtAddress1.Text == "")
            {
                lblAddress1Error.Content = "Address line 1 is required!";
                valid = false;
            }
            else if (!Demographics.AddressValidator(txtAddress1.Text))
            {
                lblAddress1Error.Content = "Address line 1 is invalid!";
                valid = false;
            }

            // Address line 2
            if (txtAddress2.Text != "" && !Demographics.AddressLine2Validator(txtAddress2.Text))
            {
                lblAddress2Error.Content = "Address line 2 is in invalid!";
                valid = false;
            }

            // City
            if (txtCity.Text == "")
            {
                lblCityError.Content = "City is required!";
                valid = false;
            }
            else if (!Demographics.OnlyLettersValidator(txtCity.Text))
            {
                lblCityError.Content = "City may only contain letters!";
                valid = false;
            }

            // Postal code
            if (txtPostal.Text == "")
            {
                lblPostalError.Content = "Postal code is required!";
                valid = false;
            }
            else if (!Demographics.PostalValidator(txtPostal.Text))
            {
                lblPostalError.Content = "Expected postal code format is A1A 1A1";
                valid = false;
            }

            return valid;
        }


        /// <summary>
        /// Change the selected patient
        /// </summary>
        /// <param name="sender">The object that raised the event</param>
        /// <param name="e">Arguments for the event</param>
        private void dtaPatientSearch_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dtaPatientSearch.SelectedItems.Count <= 0)
            {
                return;
            }

            DataRowView row = dtaPatientSearch.SelectedItems[0] as DataRowView;

            string HCN = row["HCN"].ToString();
            string firstName = row["First Name"].ToString();
            string lastName = row["Last Name"].ToString();
            string address = row["Address"].ToString();
            string phoneNumber = row["Phone Number"].ToString();
            string HOH = row["Head of House"].ToString();

            selectedPatient = new PatientInfo(HCN, firstName, lastName, address, phoneNumber, HOH);

            lblSelectedPatient.Content = "Selected Patient: " + lastName + ", " + firstName +  " (" + HCN + ")";
        }

        #endregion

        #region Appointment Management Events

        /// <summary>
        /// Performs the patient search and displays the results.
        /// </summary>
        /// <param name="sender">The object that raised the event</param>
        /// <param name="e">Arguments for the event</param>
        private void btnSearchApt_Click(object sender, RoutedEventArgs e)
        {
            bool valid = true;
            string HCN = "";
            string lastName = "";

            lblSearchAptError.Content = "";

            if (txtAptSearch.Text != "")
            {
                if (radAptSearchHCN.IsChecked.Value)
                {
                    if (!Demographics.HCNValidator(txtAptSearch.Text))
                    {
                        valid = false;
                        lblSearchAptError.Content = "Expected HCN in form 0123456789AA";
                    }
                    else
                    {
                        HCN = txtAptSearch.Text;
                    }
                }
                else if (radAptSearchName.IsChecked.Value)
                {
                    if (!Demographics.OnlyLettersValidator(txtAptSearch.Text))
                    {
                        valid = false;
                        lblSearchAptError.Content = "Last name must only contain letters!";
                    }
                    else
                    {
                        lastName = txtAptSearch.Text;
                    }
                }
            }
            else if (radAptSearchPatient.IsChecked.Value)
            {
                if (selectedPatient.HCN == null)
                {
                    valid = false;
                    lblSearchAptError.Content = "No patient selected!";
                }
                else
                {
                    HCN = selectedPatient.HCN;
                }
            }

            if (valid)
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("SearchAppointment", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@HCN", HCN);
                    cmd.Parameters.AddWithValue("@LastName", lastName);

                    conn.Open();

                    DataTable table = new DataTable();
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(table);

                    table.Columns.Add("Billing Codes");

                    foreach (DataRow row in table.Rows)
                    {
                        string billingCodes = "";

                        foreach (string billingCode in GetBillingCodes(Int32.Parse(row["ID"].ToString())))
                        {
                            billingCodes += billingCode + " ";
                        }

                        row["Billing Codes"] = billingCodes;
                    }

                    dtaAptSearch.ItemsSource = table.DefaultView;
                }
            }
        }


        /// <summary>
        /// Display the timeslots for the new selected date
        /// </summary>
        /// <param name="sender">The object that raised the event</param>
        /// <param name="e">Arguments for the event</param>
        private void calAptSchedule_SelectionChanged(object sender, RoutedEventArgs e)
        {
            List<Button> btns = new List<Button> { btnApt1, btnApt2, btnApt3, btnApt4, btnApt5, btnApt6 };
            DateTime selectedDate;
            bool dayFound = false;

            if (calAptSchedule.SelectedDate != null)
            {
                selectedDate = calAptSchedule.SelectedDate.Value;
            }
            else
            {
                return;
            }

            foreach (KeyValuePair<DateTime, List<int>> daySlots in takenDates)
            {
                if (daySlots.Key == selectedDate)
                {
                    dayFound = true;

                    for (int i = 0; i < 6; i++)
                    {
                        btns[i].IsEnabled = !daySlots.Value.Contains(i + 1);
                    }
                }
            }

            if (!dayFound)
                {
                foreach (Button btn in btns)
                {
                    btn.IsEnabled = true;
                }
            }

            Mouse.Capture(null);
        }


        /// <summary>
        /// Book an appointment
        /// </summary>
        /// <param name="sender">The object that raised the event</param>
        /// <param name="e">Arguments for the event</param>
        private void btnAppt_Click(object sender, RoutedEventArgs e)
        {
            List<Button> btns = new List<Button> { btnApt1, btnApt2, btnApt3, btnApt4, btnApt5, btnApt6 };
            DateTime selectedDate;
            int timeSlot = btns.IndexOf((Button)sender) + 1;

            if (calAptSchedule.SelectedDate == null) {
                lblBookAptError.Content = "Please select a date!";
                return;
            }
            else
            {
                selectedDate = calAptSchedule.SelectedDate.Value;
            }

            lblBookAptError.Content = "";

            if (selectedPatient.HCN == null)
            {
                lblBookAptError.Content = "No patient selected!";
            }
            else
            {
                btns[timeSlot - 1].IsEnabled = false;

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("InsertAppointment", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@HCN", selectedPatient.HCN);
                    cmd.Parameters.AddWithValue("@AppointmentDate", selectedDate.ToShortDateString());
                    cmd.Parameters.AddWithValue("@TimeSlot", timeSlot);

                    conn.Open();
                    cmd.ExecuteNonQuery();
                }

                UpdateFilledTimeslots();
            }

            Logging.MethodCalled("Booked an appointment", "SUCCESSFUL");
        }


        /// <summary>
        /// Updates the calendar to show all taken timeslots/filled days
        /// </summary>
        private void UpdateFilledTimeslots()
        {
            const int MAX_NUM_TIMESLOTS = 6;

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("GetTakenDates", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                conn.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    DateTime date = new DateTime();
                    DateTime currentDate = new DateTime();
                    List<int> timeSlots = new List<int>();
                    bool firstDate = true;

                    takenDates.Clear();

                    while (reader.Read())
                    {
                        date = DateTime.ParseExact(reader["Date"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture);

                        if (firstDate)
                        {
                            currentDate = date;
                            firstDate = false;
                        }
                        
                        if (date != currentDate)
                        {
                            takenDates.Add(new KeyValuePair<DateTime, List<int>>(currentDate, timeSlots));
                            timeSlots = new List<int>();
                            currentDate = date;
                        }

                        timeSlots.Add(Int32.Parse(reader["TimeSlot"].ToString()));
                    }

                    takenDates.Add(new KeyValuePair<DateTime, List<int>>(currentDate, timeSlots));
                }
            }

            foreach (KeyValuePair<DateTime, List<int>> dateSlots in takenDates)
            {
                if (dateSlots.Value.Count >= MAX_NUM_TIMESLOTS)
                {
                    if (!calAptSchedule.BlackoutDates.Contains(dateSlots.Key))
                    {
                        calAptSchedule.SelectedDate = null;
                        calAptSchedule.BlackoutDates.Add(new CalendarDateRange(dateSlots.Key));
                    }
                }
            }
        }


        /// <summary>
        /// Change whether the AddBillingCodes button can be clicked
        /// </summary>
        /// <param name="sender">The object that raised the event</param>
        /// <param name="e">Arguments for the event</param>
        private void dtaAptSearch_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            bool aptSelected = dtaAptSearch.SelectedItem != null;

            btnAddBillingCode.IsEnabled = aptSelected;
            txtBillingCode.IsEnabled = aptSelected;

            btnRecall.IsEnabled = aptSelected;
            cmbRecallWeeks.IsEnabled = aptSelected;
        }


        /// <summary>
        /// Add billing codes to an appointment
        /// </summary>
        /// <param name="sender">The object that raised the event</param>
        /// <param name="e">Arguments for the event</param>
        private void btnAddBillingCode_Click(object sender, RoutedEventArgs e)
        {
            DataRowView row = dtaAptSearch.SelectedItems[0] as DataRowView;
            string billingCode = txtBillingCode.Text;
            int aptID = Int32.Parse(row["ID"].ToString());
            DateTime date = DateTime.ParseExact(row["Date"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture);

            if (billingCode == "")
            {
                lblBillingCodeError.Content = "Please enter a billing code!";
                return;
            }
            else if (!Demographics.BillingCodeValidator(billingCode))
            {
                lblBillingCodeError.Content = "Expected billing code in form A111";
                return;
            }

            row["Billing Codes"] += " " + billingCode;

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("AddBillingCode", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@AptID", aptID);
                cmd.Parameters.AddWithValue("@BillingCode", billingCode);
                cmd.Parameters.AddWithValue("@HCN", row["HCN"].ToString());
                cmd.Parameters.AddWithValue("@Month", date.Month);
                cmd.Parameters.AddWithValue("@Year", date.Year);

                conn.Open();
                cmd.ExecuteNonQuery();
            }

            txtBillingCode.Text = "";

            Logging.MethodCalled("Added billing code to encounter", "SUCCESSFUL");
        }


        /// <summary>
        /// Book a recall appointment for the patient
        /// </summary>
        /// <param name="sender">The object that raised the event</param>
        /// <param name="e">Arguments for the event</param>
        private void btnRecall_Click(object sender, RoutedEventArgs e)
        {
            const int NUM_TIMESLOTS = 6;
            DataRowView row = dtaAptSearch.SelectedItems[0] as DataRowView;
            DateTime date = DateTime.ParseExact(row["Date"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture);
            int numWeeks = cmbRecallWeeks.SelectedIndex;
            List<int> takenTimeSlots = new List<int>();
            int timeSlot = -1;
            bool timeSlotFound = false;

            if (numWeeks != 0)
            {
                date = date.AddDays(numWeeks * 7);

                while (!timeSlotFound)
                {
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
                    {
                        SqlCommand cmd = new SqlCommand("SelectTimeSlotForDate", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@AppointmentDate", date.ToShortDateString());

                        conn.Open();

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                takenTimeSlots.Add(Int32.Parse(reader["TimeSlot"].ToString()));
                            }
                        }
                    }

                    for (int i = 0; i < NUM_TIMESLOTS; i++)
                    {
                        if (!takenTimeSlots.Contains(i))
                        {
                            timeSlot = i;
                        }
                    }

                    if (timeSlot == -1)
                    {
                        date.AddDays(1);
                    }
                    else
                    {
                        timeSlotFound = true;
                    }
                }

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("InsertAppointment", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@HCN", row["HCN"].ToString());
                    cmd.Parameters.AddWithValue("@AppointmentDate", date.ToShortDateString());
                    cmd.Parameters.AddWithValue("@TimeSlot", timeSlot);

                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }

            Logging.MethodCalled("Booked a recall appointment", "SUCCESSFUL");
        }

        #endregion

        #region Billing Events

        /// <summary>
        /// Gets all billing info for a specific year and month.
        /// </summary>
        /// <param name="month">The month to get billing info from</param>
        /// <param name="year">The year to get billing info from</param>
        /// <returns></returns>
        private List<BillingInfo> GetBillingInfo(int month, int year)
        {
            List<BillingInfo> bInfo = new List<BillingInfo>();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("GetBillingInfo", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("Year", year);
                cmd.Parameters.AddWithValue("Month", month);

                conn.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    DateTime date = new DateTime();
                    List<string> billingCodes = new List<string>();

                    while (reader.Read())
                    {
                        date = DateTime.ParseExact(reader["Date"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture);
                        billingCodes = GetBillingCodes(Int32.Parse(reader["AppointmentID"].ToString()));

                        bInfo.Add(new BillingInfo(reader["HCN"].ToString(), reader["Gender"].ToString()[0], date.Day, date.Month, date.Year, billingCodes));
                    }
                }
            }

            return bInfo;
        }


        /// <summary>
        /// Gets the billing codes for a specific appointment
        /// </summary>
        /// <param name="appointmentID">The appointment to get billing codes for</param>
        /// <returns>The billing codes</returns>
        private List<string> GetBillingCodes(int appointmentID)
        {
            List<string> billingCodes = new List<string>();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("GetBillingCodes", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("AppointmentID", appointmentID);

                conn.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        billingCodes.Add(reader["BillingCode"].ToString().Trim());
                    }
                }
            }

            return billingCodes;
        }


        /// <summary>
        /// Allows the user to select an MOH file to parse
        /// </summary>
        /// <param name="sender">The object that raised the event</param>
        /// <param name="e">Arguments for the event</param>
        private void btnPickFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            ofd.InitialDirectory = Directory.GetCurrentDirectory() + "\\Billing\\ResponseFile\\";

            if (ofd.ShowDialog() == true)
            {
                txtMOHFilePath.Text = ofd.FileName;
            }
        }


        /// <summary>
        /// Validates the selected MOH file and then generates a monthly summary
        /// </summary>
        /// <param name="sender">The object that raised the event</param>
        /// <param name="e">Arguments for the event</param>
        private void btnGenerateSummary_Click(object sender, RoutedEventArgs e)
        {
            lblMOHFileNameError.Content = "";

            if (txtMOHFilePath.Text == "")
            {
                lblMOHFileNameError.Content = "Please select an MOH file!";
            }
            else if (!File.Exists(txtMOHFilePath.Text))
            {
                lblMOHFileNameError.Content = "The selected file does not exist!";
            }
            else
            {
                string outputFilePath;

                if (!Billing.ReadResponseFile(txtMOHFilePath.Text, out outputFilePath))
                {
                    lblMOHFileNameError.Content = "The selected file could not be read!";
                }
                else
                {
                    txtSummaryOutput.Text = File.ReadAllText(outputFilePath);
                }
            }

            Logging.MethodCalled("Generated billing summary", "SUCCESSFUL");
        }


        /// <summary>
        /// Sets the billing date to the current month and year
        /// </summary>
        /// <param name="sender">The object that raised the event</param>
        /// <param name="e">Arguments for the event</param>
        private void btnUseCurrentDate_Click(object sender, RoutedEventArgs e)
        {
            cmbBillMonth.SelectedIndex = DateTime.Today.Month - 1;
            txtBillYear.Text = DateTime.Today.Year.ToString();
        }


        /// <summary>
        /// Generate the billing file if year/month selection is valid.
        /// </summary>
        /// <param name="sender">The object that raised the event</param>
        /// <param name="e">Arguments for the event</param>
        private void btnGenerateBillingFile_Click(object sender, RoutedEventArgs e)
        {
            lblBillYearError.Content = "";

            if (txtBillYear.Text == "")
            {
                lblBillYearError.Content = "Please enter a year!";
            }
            else if (!Demographics.OnlyNumbersValidator(txtBillYear.Text))
            {
                lblBillYearError.Content = "Year must only contain numbers!";
            }
            else if (Int32.Parse(txtBillYear.Text) > DateTime.Today.Year)
            {
                lblBillYearError.Content = "Year cannot be greater than current year!";
            }
            else
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.InitialDirectory = Directory.GetCurrentDirectory() + "\\Billing\\BillingFile\\";
                sfd.FileName = "ems." + (cmbBillMonth.SelectedIndex + 1) + "-" +  Int32.Parse(txtBillYear.Text);
                sfd.Filter = "Text File (*.txt)|*.txt| All Files (*.*)|*.*";
                sfd.DefaultExt = "txt";

                if (sfd.ShowDialog() == true)
                {
                    Billing.EnterInfo(GetBillingInfo(cmbBillMonth.SelectedIndex + 1, Int32.Parse(txtBillYear.Text)), sfd.FileName);
                }
            }

            Logging.MethodCalled("Generated billing file", "SUCCESSFUL");
        }

        #endregion

        #region Login Events

        /// <summary>
        /// Login the user if their information is valid.
        /// </summary>
        /// <param name="sender">The object that raised the event</param>
        /// <param name="e">Arguments for the event</param>
        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            if (!ValidateLogin())
            {
                return;
            }

            char employeeType;
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("GetEmployeeType", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Username", txtUsername.Text);
                cmd.Parameters.AddWithValue("@Password", txtPassword.Password);

                SqlParameter ret = new SqlParameter("@retValue", SqlDbType.NChar);
                ret.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(ret);

                conn.Open();
                cmd.ExecuteNonQuery();

                employeeType = cmd.Parameters["@retValue"].Value.ToString()[0];
            }

            if (employeeType == ADMIN_TYPE)
            {
                tcMain.SelectedIndex = tcMain.Items.Count - 1;
            }
            else
            {
                tabScheduling.Visibility = Visibility.Visible;
                tabPatients.Visibility = Visibility.Visible;
                tabAptRequests.Visibility = Visibility.Visible;
                tabBilling.Visibility = Visibility.Visible;
                tabAptCheckins.Visibility = Visibility.Visible;
                sbInfoBar.Visibility = Visibility.Visible;
                tcMain.SelectedIndex = 0;
            }
        }


        /// <summary>
        /// Validates the login information entered by the user
        /// </summary>
        /// <returns>true if valid, false otherwise</returns>
        private bool ValidateLogin()
        {
            string username = txtUsername.Text;
            string password = txtPassword.Password;
            bool retCode = true;

            lblUsernameError.Content = "";
            lblPasswordError.Content = "";

            if (username == "")
            {
                lblUsernameError.Content = "Please enter a username!";
                retCode = false;
            }
            else if (!Demographics.UsernameValidator(username))
            {
                lblUsernameError.Content = "Username is invalid!";
                retCode = false;
            }

            if (password == "")
            {
                lblPasswordError.Content = "Please enter a password!";
                retCode = false;
            }
            else if (!Demographics.PasswordValidator(password))
            {
                lblPasswordError.Content = "Password is invalid!";
                retCode = false;
            }

            if (retCode)
            {
                bool correctLogin = false;
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("CheckLogin", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Username", username);
                    cmd.Parameters.AddWithValue("@Password", password);

                    SqlParameter ret = new SqlParameter("@retValue", SqlDbType.Bit);
                    ret.Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add(ret);

                    conn.Open();
                    cmd.ExecuteNonQuery();

                    correctLogin = (bool)cmd.Parameters["@retValue"].Value;
                }

                if (!correctLogin)
                {
                    lblUsernameError.Content = "Incorrect login details!";
                    retCode = false;
                }
            }

            return retCode;
        }

        #endregion

        #region Window Navigation Events

        /// <summary>
        /// Exit the requested appointment thread
        /// </summary>
        /// <param name="sender">The object that raised the event</param>
        /// <param name="e">Arguments for the event</param>
        private void OnWindowClosing(object sender, EventArgs e)
        {
            exitThread = true;
            aptRequestThread.Abort();
            aptCheckinThread.Abort();
        }

        /// <summary>
        /// Switch to the add a patient window.
        /// </summary>
        /// <param name="sender">The object that raised the event</param>
        /// <param name="e">Arguments for the event</param>
        private void btnPatientAdd_Click(object sender, RoutedEventArgs e)
        {
            tbcPatient.SelectedIndex = ADD_PATIENT_TAB_INDEX;
            btnPatientSearch.IsEnabled = true;
            btnPatientAdd.IsEnabled = false;
        }


        /// <summary>
        /// Switch to the patient search window.
        /// </summary>
        /// <param name="sender">The object that raised the event</param>
        /// <param name="e">Arguments for the event</param>
        private void btnPatientSearch_Click(object sender, RoutedEventArgs e)
        {
            tbcPatient.SelectedIndex = SEARCH_PATIENT_TAB_INDEX;
            btnPatientSearch.IsEnabled = false;
            btnPatientAdd.IsEnabled = true;
        }


        /// <summary>
        /// Switch to the book appointment window.
        /// </summary>
        /// <param name="sender">The object that raised the event</param>
        /// <param name="e">Arguments for the event</param>
        private void btnBookApts_Click(object sender, RoutedEventArgs e)
        {
            tbcAppointments.SelectedIndex = BOOK_APT_TAB_INDEX;
            btnSearchApts.IsEnabled = true;
            btnBookApts.IsEnabled = false;
        }


        /// <summary>
        /// Switch to the search appointments window.
        /// </summary>
        /// <param name="sender">The object that raised the event</param>
        /// <param name="e">Arguments for the event</param>
        private void btnSearchApts_Click(object sender, RoutedEventArgs e)
        {
            tbcAppointments.SelectedIndex = SEARCH_APT_TAB_INDEX;
            btnSearchApts.IsEnabled = false;
            btnBookApts.IsEnabled = true;
        }


        /// <summary>
        /// Switch to the billing summary window.
        /// </summary>
        /// <param name="sender">The object that raised the event</param>
        /// <param name="e">Arguments for the event</param>
        private void btnSummary_Click(object sender, RoutedEventArgs e)
        {
            tbcBilling.SelectedIndex = BILLING_SUMMARY_TAB_INDEX;
            btnSummary.IsEnabled = false;
            btnFile.IsEnabled = true;
        }


        /// <summary>
        /// Switch to the billing file window.
        /// </summary>
        /// <param name="sender">The object that raised the event</param>
        /// <param name="e">Arguments for the event</param>
        private void btnFile_Click(object sender, RoutedEventArgs e)
        {
            tbcBilling.SelectedIndex = BILLING_FILE_TAB_INDEX;
            btnSummary.IsEnabled = true;
            btnFile.IsEnabled = false;
        }

        #endregion

    }
}
