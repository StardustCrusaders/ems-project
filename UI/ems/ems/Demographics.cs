/*
 * FILE          : Demographics.cs
 * PROJECT       : EMS II
 * PROGRAMMER    : Jacob Funes
 * DESCRIPTION   :
 *      This file contains the Demographics class
 */

using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace ems
{
    /// <summary>
    /// Used to store patient information
    /// </summary>
    struct PatientInfo
    {
        public readonly string HCN;
        public readonly string FirstName;
        public readonly string LastName;
        public readonly string Address;
        public readonly string PhoneNumber;
        public readonly string HOH;

        public PatientInfo(string HCN, string FirstName, string LastName, string Address, string PhoneNumber, string HOH)
        {
            this.HCN = HCN;
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.Address = Address;
            this.PhoneNumber = PhoneNumber;
            this.HOH = HOH;
        }
    };


    /// <summary>
    /// This class contains validators for all demographic input types
    /// </summary>
    public static class Demographics
    {
        /// <summary>
        /// Validate the user input, checking if only numbers exist within the string.
        /// If there is a letter, then the input is invalid (ie, the user input a DOB such as "21NO199E")
        /// If there is no letter, the number is valid. 
        /// </summary>
        /// <param name="text"></param>
        /// <returns>True if valid, false otherwise</returns>
        public static bool OnlyNumbersValidator(string text)
        {
            return Int32.TryParse(text, out int temp);
        }


        /// <summary>
        /// Validate the user input from the text boxes that do not need to contain a number. 
        /// If the input contains a number, return false.
        /// If the input contains no number, return true.
        /// </summary>
        /// <param name="text"> Text that the user input to the box. </param>
        /// <returns>Returns true or false depending on if the input is proper.</returns>
        public static bool OnlyLettersValidator(string text)
        {
            string pattern = @"^[a-zA-Z]*$";
            Regex letterRegex = new Regex(pattern);

            return letterRegex.IsMatch(text);
        }


        /// <summary>
        /// Validates a name.
        /// </summary>
        /// <param name="text">The text to validate</param>
        /// <returns>Returns true or false depending on if the input is proper.</returns>
        public static bool NameValidator(string text)
        {
            string pattern = @"^[a-zA-Z'`-]*$";
            Regex numberRegex = new Regex(pattern);

            return numberRegex.IsMatch(text);
        }


        /// <summary>
        /// Validate the user input from the HCN text box.
        /// If the HCN is not in the proper format (10 Numbers followed by 2 Letterss), return false.
        /// If the format is valid, return true.
        /// </summary>
        /// <param name="text"> Text that the user input to the box. </param>
        /// <returns> Returns true or false depending on if the input is proper. </returns>
        public static bool HCNValidator(string text)
        {
            string pattern = @"^[0-9]{10}[A-Za-z]{2}$";
            Regex HCNRegex = new Regex(pattern);

            return HCNRegex.IsMatch(text);
        }


        /// <summary>
        /// Enforce a minimum of 1 number, followed by any numbers of letters without numbers.
        /// If the user inputs an address without a number, return false.
        /// If the user inputs an address with a number, return true.
        /// </summary>
        /// <param name="text"> Text that the user input to the box. </param>
        /// <returns> Returns true or false depending on if the input is proper. </returns>
        public static bool AddressValidator(string text)
        {
            string pattern = @"^\d+\s[\sa-zA-z.]*$";
            Regex AddressRegex = new Regex(pattern);

            return AddressRegex.IsMatch(text);
        }


        /// <summary>
        /// Enforce DoB to be in DD/MM/YYYY format, user cannot input days past 31, or months past 12.
        /// If the user inputs a day/month out of range (13/30/2001) or (12/32/2000), return true.
        /// If the DoB is a valid day/month, return true.
        /// </summary>
        /// <param name="text"> Text to be tested against regex </param>
        /// <returns> Returns true or false depending on if the input is proper. </returns>  
        public static bool DoBValidator(string text)
        {
            string pattern = @"^([0]{0,1}[1-9]|1[0-2])\/([0]{0,1}[1-9]|[1-2][0-9]|3[01])\/(19|20)\d{2}$";
            Regex dateValidator = new Regex(pattern);

            return dateValidator.IsMatch(text);
        }


        /// <summary>
        /// Phone Validator, enforcing format 000-000-0000.
        /// If the phone number isn't in the above format, return false.
        /// If the phone number is in proper format, return true.
        /// </summary>
        /// <param name="text"> Text to be tested against regex </param>
        /// <returns> Returns true or false depending on if the input is proper. </returns>
        public static bool PhoneValidator(string text)
        {
            string pattern = @"^[0-9]{3}-[0-9]{3}-[0-9]{4}$";
            Regex phoneRegex = new Regex(pattern);

            return phoneRegex.IsMatch(text);
        }


        /// <summary>
        /// Validates a billing code, expects a letter followed by three numbers.
        /// </summary>
        /// <param name="text">The billing code to validate</param>
        /// <returns>Returns true or false depending on if the input is proper.</returns>
        public static bool BillingCodeValidator(string text)
        {
            string pattern = @"^[A-Z]{1}[0-9]{3}$";
            Regex codeRegex = new Regex(pattern);

            return codeRegex.IsMatch(text);
        }


        /// <summary>
        /// Address validator, allowing user to input Unit/Apt followed by 1 or 2 characters
        /// If the user inputs anything other than Unit/Apt followed by a number, return false.
        /// If the user inputs Unit followed by a number or Apt followed by a number, return true.
        /// </summary>
        /// <param name="text">Text to be tested against regex</param>
        /// <returns>Returns true or false depending on if the input is proper.</returns>
        public static bool AddressLine2Validator(string text)
        {
            string pattern = @"^([u|U]nit|[a|A]pt|[b|B]ldg){1} [0-9]{1,2}$";
            Regex addressRegex = new Regex(pattern);

            return addressRegex.IsMatch(text);
        }


        /// <summary>
        /// Postal code validator, expects either A1A1A1 or A1A 1A1 forms.
        /// Allows lowercase and uppercase
        /// </summary>
        /// <param name="text">Text to be tested against regex</param>
        /// <returns>True if valid, false otherwise</returns>
        public static bool PostalValidator(string text)
        {
            string pattern = @"^[A-Z][0-9][A-Z]\s[0-9][A-Z][0-9]$";
            Regex postalRegex = new Regex(pattern);

            return postalRegex.IsMatch(text);
        }


        /// <summary>
        /// Validates a username expects 3-8 letters followed by 4 numbers
        /// </summary>
        /// <param name="text">Text to be tested against regex</param>
        /// <returns>True if valid, false otherwise</returns>
        public static bool UsernameValidator(string text)
        {
            string pattern = @"^[A-Za-z]{3,10}[1-9]{0,4}$";
            Regex usernameRegex = new Regex(pattern);

            return usernameRegex.IsMatch(text);
        }


        /// <summary>
        /// Validates a password expects any length with only letters, numbers, and/or some special characters
        /// </summary>
        /// <param name="text">Text to be tested against regex</param>
        /// <returns>True if valid, false otherwise</returns>
        public static bool PasswordValidator(string text)
        {
            string pattern = @"^[A-Za-z0-9!$_?-]+$";
            Regex passwordRegex = new Regex(pattern);

            return passwordRegex.IsMatch(text);
        }


        /// <summary>
        /// Check if a HCN already exists in the database
        /// </summary>
        /// <param name="HCN">The HCN to look for</param>
        /// <returns>true if the HCN exists, false otherwise</returns>
        public static bool CheckHCNExists(string HCN)
        {
            if (HCN != "")
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("DoesHCNExist", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@HealthCardNumber", HCN);
                    var ret = cmd.Parameters.Add("@Found", SqlDbType.Bit);
                    ret.Direction = ParameterDirection.Output;

                    conn.Open();
                    cmd.ExecuteNonQuery();

                    return (bool)ret.Value;
                }
            }

            return false;
        }
    }
}