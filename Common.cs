﻿using System.Collections.Generic;

namespace Common
{
    public delegate void OnClick();
    public delegate bool Validator(string text);

    /// <summary>
    /// This struct holds an (x, y) coordinate for the console.
    /// </summary>
    public struct Coordinates
    {
        public Coordinates(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public int x;
        public int y;
    }


    /// <summary>
    /// This struct is used to store client demographic information.
    /// </summary>
    public struct DemographicInfo
    {
        public DemographicInfo(string HCN, string firstName, string lastName, string middleInitial, string dateOfBirth, string sex, string addressLine1,
                          string addressLine2, string city, string province, string phoneNum, string headOfHouse = "")
        {
            this.HCN = HCN;
            this.lastName = lastName;
            this.firstName = firstName;
            mInitial = middleInitial;
            this.dateOfBirth = dateOfBirth;
            this.sex = sex;
            this.addressLine1 = addressLine1;
            this.addressLine2 = addressLine2;
            this.city = city;
            this.province = province;
            this.phoneNum = phoneNum;
            this.headOfHouse = headOfHouse;
        }

        public DemographicInfo(string HCN, string firstName, string lastName, string middleInitial, string dateOfBirth, string sex, string headOfHouse)
        {
            this.HCN = HCN;
            this.lastName = lastName;
            this.firstName = firstName;
            mInitial = middleInitial;
            this.dateOfBirth = dateOfBirth;
            this.sex = sex;
            addressLine1 = "";
            addressLine2 = "";
            city = "";
            province = "";
            phoneNum = "";
            this.headOfHouse = headOfHouse;
        }

        public string lastName;
        public string firstName;
        public string mInitial;
        public string HCN;
        public string dateOfBirth;
        public string sex;
        public string headOfHouse;
        public string addressLine1;
        public string addressLine2;
        public string city;
        public string province;
        public string phoneNum;
    }


    /// <summary>
    /// This struct is used to store billing information.
    /// </summary>
    public struct BillingInfo
    {
        public BillingInfo(string HCN, char sex, int day, int month, int year, List<string> billingCodes)
        {
            this.HCN = HCN;
            this.sex = sex;
            this.day = day;
            this.month = month;
            this.year = year;
            this.billingCodes = billingCodes;
        }

        public string HCN;
        public char sex;
        public int day;
        public int month;
        public int year;
        public List<string> billingCodes;
    }


    /// <summary>
    /// This struct is used to store scheduling information.
    /// </summary>
    public struct SchedulingInfo
    {
        public SchedulingInfo(string HCN, int timeSlot, int numPatients, int day, int month, int year)
        {
            this.HCN = HCN;
            this.numPatients = numPatients;
            this.day = day;
            this.month = month;
            this.year = year;
            this.timeSlot = timeSlot;
        }

        public string HCN;
        public int numPatients;
        public int day;
        public int month;
        public int year;
        public int timeSlot;
    }

    static class Common
    {
        public static UI.BaseMenu currentMenu { get; set; }
        public static Stack<UI.BaseMenu> lastMenu = new Stack<UI.BaseMenu>();
    }
}
