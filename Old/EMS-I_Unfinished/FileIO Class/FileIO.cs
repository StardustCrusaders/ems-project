/*
 *  FILE            : FileIO.cs
 *  PROGRAMMER      : William Bicknell
 *  FIRST VERSION   : November 3, 2018
 *  DESCRIPTION     :
 *      This file contains basic fileIO functions as well as functions that read and write to databases.
 */

using Common;
using System;
using System.Collections.Generic;
using System.IO;

//TODO: Update function for all
namespace FileIO
{
    /// <summary>
    /// This class contains functions that allow for reading and writing to/from the Demographics, 
    /// Scheduling, and Billing databases.<br/>
    ///
    /// When a fault or exception occurs, it is logged and the function where it occured returns a failure flag.
    /// To avoid/catch faults and exceptions, incoming data to be stored in the databases is checked before it is written.
    /// 
    /// Programmers:    William Bicknell, Jacob Funes, Firas Areibi, Moises Diaz<br/>
    /// Date:           2018-11-13
    /// </summary>
    public static class FileIO
    {
        private const string DEMOGRAPHICS_FILE = "./DBase/demographics/demo.db";
        private const string DEMOGRAPHICS_DIR = "./DBase/demographics/";
        private const string SCHEDULING_DIR = "./DBase/scheduling/";
        private const string BILLING_DIR = "./DBase/billing/";
        private const char DELIMITER = '|';
        private const char BILLCODE_DELIM = '~';


        /// <summary>
        /// The pieces of data contained in a client demographic database entry.
        /// </summary>
        private enum DInfoTypes
        {
            HCN,
            firstName,
            initial,
            lastName,
            dateOfBirth,
            sex,
            addressLine1,
            addressLine2,
            city,
            province,
            phoneNum,
            headOfHouse
        };


        /// <summary>
        /// The pieces of data contained in a billing database entry.
        /// </summary>
        private enum BInfoTypes
        {
            HCN,
            sex,
            day,
            month,
            year,
            billingCodes
        };


        /// <summary>
        /// The pieces of data contained in a scheduling database entry.
        /// </summary>
        private enum SInfoTypes
        {
            timeSlot,
            HCN,
            numPatients,
            day,
            month,
            year
        };


        /// <summary>
        /// This dictionary contains the months of the year paired in numerical and alphabetical forms.
        /// </summary>
        private static readonly Dictionary<int, string> months = new Dictionary<int, string>()
        {
            { 1, "January" },
            { 2, "February" },
            { 3, "March" },
            { 4, "April" },
            { 5, "May" },
            { 6, "June" },
            { 7, "July" },
            { 8, "August" },
            { 9, "September" },
            { 10, "October" },
            { 11, "November" },
            { 12, "December" }
        };


        //static void Main(string[] args)
        //{
        //    List<string> codes = new List<string>(){ "A123", "B234", "C435" };
        //    BillingInfo[] bInfos = new BillingInfo[]
        //    {
        //        new BillingInfo("1234D", 'm', 5, 6, 2018, codes), // success
        //        new BillingInfo("2234D", 'f', 5, 6, 2018, codes), // success
        //        new BillingInfo("3234D", 'm', 5, 6, 2018, codes), // success
        //        new BillingInfo("4234D", 'm', 5, 6, 2018, codes)  // success
        //    };
        //    WriteBilling(bInfos);

        //    SchedulingInfo[] sInfos = new SchedulingInfo[]
        //    {
        //        new SchedulingInfo("1234D", 1, 1, 5, 6, 2018), // success
        //        new SchedulingInfo("2234D", 2, 2, 5, 6, 2018), // success
        //        new SchedulingInfo("3234D", 3, 3, 5, 6, 2018), // failure
        //        new SchedulingInfo("4234D", 2, 1, 5, 6, 2018)  // failure
        //    };
        //    WriteScheduling(sInfos);

        //    DemographicInfo[] cInfos = new DemographicInfo[]
        //    {
        //        new DemographicInfo("1234D", "dave", "jim", ' ', "01/02/88", "m", "123 st", "", "woody", "ON", "123-1234"), // success
        //        new DemographicInfo("2234D", "dave", "dave", 'd', "01/02/98", "m", "1234D"), // success - filled in from first entry
        //        new DemographicInfo("3234D", "jon", "jonsworth", 'h', "03/04/95", "f", ""),  // failure
        //    };
        //    WriteDemographic(cInfos);

        //    //BillingInfo b = new BillingInfo();
        //    //SearchBillingInfo(ref b, "1234D", 6, 2018);
        //    //Console.WriteLine(b.HCN + " - " + b.day + " - " + b.month + " - " + b.year + " - " + b.billingCodes);

        //    //DemographicInfo d = new DemographicInfo();
        //    //SearchDemographic(ref d, "1234D");
        //    //Console.WriteLine(d.HCN + " - " + d.firstName + " - " + d.lastName + " - " + d.phoneNum + " - " + d.province + " - " + d.sex + " - " + d.city + " - " + d.addressLine1 + " - " + d.addressLine2 + " - " + d.dateOfBirth);
        //    //Console.ReadLine();
        //}


        /// <summary>
        /// This function creates all required directories for the database.
        /// </summary>
        public static void InitDatabase()
        {
            Directory.CreateDirectory(DEMOGRAPHICS_DIR);
            Directory.CreateDirectory(SCHEDULING_DIR);
            Directory.CreateDirectory(BILLING_DIR);
        }


        /// <summary>
        /// Writes a line of text to a file.
        /// </summary>
        /// <param name="path">The path of the file that will be written to.</param>
        /// <param name="line">The text that will be written to the file.</param>
        private static void WriteLine(string path, string line)
        {
            if (!Directory.Exists(path))
            {
                InitDatabase();
            }

            using (StreamWriter file = new StreamWriter(path, true))
            {
                file.WriteLine(line);
            }
        }


        /// <summary>
        /// Writes lines of text to a file.
        /// </summary>
        /// <param name="path">The path of the file that will be written to.</param>
        /// <param name="lines">The lines of text that will be written to the file.</param>
        private static void WriteLines(string path, string[] lines)
        {
            if (!Directory.Exists(path))
            {
                InitDatabase();
            }

            using (StreamWriter file = new StreamWriter(path, true))
            {
                foreach (string line in lines)
                {
                    file.WriteLine(line);
                }
            }
        }


        /// <summary>
        /// This function writes a line of demographic information to the database.
        /// </summary>
        /// <param name="cInfo">Client information that will be added to the database.</param>
        /// <param name="log">Should logging be done.</param>
        /// <returns>bool : true if successful, false otherwise.</returns>
        public static bool WriteDemographic(DemographicInfo cInfo, bool log = true)
        {
            if (cInfo.headOfHouse == "" && (cInfo.addressLine1 == "" || cInfo.city == "" || cInfo.province == "" || cInfo.phoneNum == ""))
            {
                if (log)
                {
                    Logging.Logging.MethodCalled("Invalid client info found.", "FAILURE");
                }
                
                return false;
            }
            // Fills in headOfHouse info
            else if (cInfo.headOfHouse != "" && (cInfo.addressLine1 == "" || cInfo.addressLine2 == "" || cInfo.city == "" || cInfo.province == "" || cInfo.phoneNum == ""))
            {
                if (File.Exists(DEMOGRAPHICS_FILE))
                {
                    using (StreamReader file = new StreamReader(DEMOGRAPHICS_FILE))
                    {
                        string line = "";

                        while (!file.EndOfStream)
                        {
                            line = file.ReadLine();
                            if (line.Split(DELIMITER)[(int)DInfoTypes.HCN] == cInfo.headOfHouse)
                            {
                                ExtractHeadOfHouseInfo(line, ref cInfo);
                                break;
                            }
                        }
                    }
                }
            }

            string writeLine = string.Format("{1}{0}{2}{0}{3}{0}{4}{0}{5}{0}{6}{0}{7}{0}{8}{0}{9}{0}{10}{0}{11}{0}{12}", DELIMITER, cInfo.HCN, 
                cInfo.firstName, cInfo.mInitial, cInfo.lastName, cInfo.dateOfBirth, cInfo.sex, cInfo.addressLine1, 
                cInfo.addressLine2, cInfo.city, cInfo.province, cInfo.phoneNum, cInfo.headOfHouse);
            WriteLine(DEMOGRAPHICS_FILE, writeLine);

            if (log)
            {
                Logging.Logging.MethodCalled("Wrote 1 line to Demographics DB", "SUCCESS");
            }

            return true;
        }


        /// <summary>
        /// This function writes multiple lines of demographic information to the database.
        /// </summary>
        /// <param name="clientInformation">Multiple client's information that will be added to the database.</param>
        /// <returns>bool : true if successful, false otherwise.</returns>
        public static bool WriteDemographic(DemographicInfo[] clientInformation)
        {
            int numValid = 0;
            int numInvalid = 0;
            string result = "SUCCESS";

            foreach (DemographicInfo cInfo in clientInformation)
            {
                if (!WriteDemographic(cInfo, false))
                {
                    numInvalid++;
                }
                else
                {
                    numValid++;
                }
            }

            if (numValid == 0)
            {
                result = "FAILURE";
            }

            Logging.Logging.MethodCalled("Wrote " + numValid + " VALID record(s) to Demographics DB. " + numInvalid + " INVALID record(s)." , result);

            return (numValid > 0);
        }


        /// <summary>
        /// This function writes a line of billing information to the database.
        /// </summary>
        /// <param name="bInfo">The billing information that will be added to the database.</param>
        /// <param name="log">Should logging be done.</param>
        /// <returns>bool : true if successful, false otherwise.</returns>
        public static bool WriteBilling(BillingInfo bInfo, bool log = true)
        {
            string path = BILLING_DIR + "Billing-" + months[bInfo.month] + "-" + bInfo.year + ".db";

            string billingCodes = "";
            for (int i = 0; i < bInfo.billingCodes.Count; i++)
            {
                billingCodes += bInfo.billingCodes[i];

                if (i < bInfo.billingCodes.Count - 1)
                {
                    billingCodes += BILLCODE_DELIM;
                }
            }

            string line = string.Format("{1}{0}{2}{0}{3}{0}{4}{0}{5}{0}{6}", DELIMITER, bInfo.HCN, bInfo.sex, 
                bInfo.day, bInfo.month, bInfo.year, billingCodes);
            WriteLine(path, line);
            
            if (log)
            {
                Logging.Logging.MethodCalled("Wrote 1 line to Billing DB", "SUCCESS");
            }

            return true;
        }


        /// <summary>
        /// This function writes multiple lines of billing information to the database.
        /// </summary>
        /// <param name="billingInformation">Multiple pieces of billing information that will be added to the database.</param>
        /// <returns>bool : true if successful, false otherwise.</returns>
        public static bool WriteBilling(BillingInfo[] billingInformation)
        {
            int numValid = 0;
            int numInvalid = 0;
            string result = "SUCCESS";

            foreach (BillingInfo bInfo in billingInformation)
            {
                if (!WriteBilling(bInfo, false))
                {
                    numInvalid++;
                }
                else
                {
                    numValid++;
                }
            }

            if (numValid == 0)
            {
                result = "FAILURE";
            }

            Logging.Logging.MethodCalled("Wrote " + numValid + " VALID record(s) to Billing DB. " + numInvalid + " INVALID record(s).", result);

            return (numValid > 0);
        }


        /// <summary>
        /// This function writes a line of scheduling information to the database.
        /// </summary>
        /// <param name="sInfo">The scheduling information that will be added to the database.</param>
        /// <param name="log">Should logging be done.</param>
        /// <returns>bool : true if successful, false otherwise.</returns>
        public static bool WriteScheduling(SchedulingInfo sInfo, bool log = true)
        {
            Directory.CreateDirectory(SCHEDULING_DIR + months[sInfo.month]);
            string path = SCHEDULING_DIR + months[sInfo.month] + "/Scheduling-" + months[sInfo.month] + "-" + sInfo.day + ".db";

            SchedulingInfo temp = new SchedulingInfo();
            SearchSchedulingInfo(ref temp, sInfo.timeSlot, sInfo.month, sInfo.day);
            if (temp.timeSlot == sInfo.timeSlot)
            {
                if (log)
                {
                    Logging.Logging.MethodCalled("Invalid scheduling information found, duplicate time slot.", "FAILURE");
                }

                return false;
            }

            if (sInfo.numPatients > 2)
            {
                if (log)
                {
                    Logging.Logging.MethodCalled("Invalid scheduling information found, too many patients.", "FAILURE");
                }

                return false;
            }

            string line = string.Format("{1}{0}{2}{0}{3}{0}{4}{0}{5}{0}{6}", DELIMITER, sInfo.timeSlot, sInfo.HCN, 
                sInfo.numPatients, sInfo.day, sInfo.month, sInfo.year);
            WriteLine(path, line);

            if (log)
            {
                Logging.Logging.MethodCalled("Wrote 1 line to Scheduling DB", "SUCCESS");
            }

            return true;
        }


        /// <summary>
        /// This function writes multiple lines of scheduling information to the database.
        /// </summary>
        /// <param name="schedulingInformation">Multiple pieces of scheduling information that will be added to the database.</param>
        /// <returns>bool : true if successful, false otherwise.</returns>
        public static bool WriteScheduling(SchedulingInfo[] schedulingInformation)
        {
            int numValid = 0;
            int numInvalid = 0;
            string result = "SUCCESS";

            foreach (SchedulingInfo sInfo in schedulingInformation)
            {
                if (!WriteScheduling(sInfo, false))
                {
                    numInvalid++;
                }
                else
                {
                    numValid++;
                }
            }

            if (numValid == 0)
            {
                result = "FAILURE";
            }

            Logging.Logging.MethodCalled("Wrote " + numValid + " VALID record(s) to Scheduling DB. " + numInvalid + " INVALID record(s).", result);

            return (numValid > 0);
        }


        /// <summary>
        /// This function extracts the demographic information from a database entry.
        /// </summary>
        /// <param name="line">A line of demographic DB information.</param>
        /// <returns>ClientInfo : The extracted information.</returns>
        private static DemographicInfo ExtractDemographicInfo(string line)
        {
            DemographicInfo cInfo = new DemographicInfo();

            string[] information = line.Split(DELIMITER);
            cInfo.HCN = information[(int)DInfoTypes.HCN];
            cInfo.firstName = information[(int)DInfoTypes.firstName];
            cInfo.mInitial = information[(int)DInfoTypes.initial];
            cInfo.lastName = information[(int)DInfoTypes.lastName];
            cInfo.dateOfBirth = information[(int)DInfoTypes.dateOfBirth];
            cInfo.sex = information[(int)DInfoTypes.sex];
            cInfo.addressLine1 = information[(int)DInfoTypes.addressLine1];
            cInfo.addressLine2 = information[(int)DInfoTypes.addressLine2];
            cInfo.city = information[(int)DInfoTypes.city];
            cInfo.province = information[(int)DInfoTypes.province];
            cInfo.phoneNum = information[(int)DInfoTypes.phoneNum];
            cInfo.headOfHouse = information[(int)DInfoTypes.headOfHouse];

            return cInfo;
        }


        /// <summary>
        /// This function extracts the required information to fill in an entry that only has a HoH.
        /// </summary>
        /// <param name="line">A line of demographic DB information.</param>
        /// <param name="cInfo">Where the information will be stored.</param>
        private static void ExtractHeadOfHouseInfo(string line, ref DemographicInfo cInfo)
        {
            string[] information = line.Split(DELIMITER);
            cInfo.addressLine1 = information[(int)DInfoTypes.addressLine1];
            cInfo.addressLine2 = information[(int)DInfoTypes.addressLine2];
            cInfo.city = information[(int)DInfoTypes.city];
            cInfo.province = information[(int)DInfoTypes.province];
            cInfo.phoneNum = information[(int)DInfoTypes.phoneNum];
        }


        /// <summary>
        /// This function extracts the billing information from a database entry.
        /// </summary>
        /// <param name="line">A line of billing DB information.</param>
        /// <returns>BillingInfo : The extracted information.</returns>
        private static BillingInfo ExtractBillingInfo(string line)
        {
            BillingInfo bInfo = new BillingInfo();

            string[] information = line.Split(DELIMITER);
            bInfo.HCN = information[(int)BInfoTypes.HCN];
            bInfo.sex = information[(int)BInfoTypes.sex][0];
            bInfo.day = Int32.Parse(information[(int)BInfoTypes.day]);
            bInfo.month = Int32.Parse(information[(int)BInfoTypes.month]);
            bInfo.year = Int32.Parse(information[(int)BInfoTypes.year]);

            foreach (string code in information[(int)BInfoTypes.billingCodes].Split(BILLCODE_DELIM))
            {
                bInfo.billingCodes.Add(line);
            }

            return bInfo;
        }


        /// <summary>
        /// This function extracts the scheduling information from a database entry.
        /// </summary>
        /// <param name="line">string line : A line of scheduling DB information.</param>
        /// <returns>SchedulingInfo : The extracted information.</returns>
        private static SchedulingInfo ExtractSchedulingInfo(string line)
        {
            SchedulingInfo sInfo = new SchedulingInfo();

            string[] information = line.Split(DELIMITER);
            sInfo.timeSlot = Int32.Parse(information[(int)SInfoTypes.timeSlot]);
            sInfo.HCN = information[(int)SInfoTypes.HCN];
            sInfo.numPatients = Int32.Parse(information[(int)SInfoTypes.numPatients]);
            sInfo.day = Int32.Parse(information[(int)SInfoTypes.day]);
            sInfo.month = Int32.Parse(information[(int)SInfoTypes.month]);
            sInfo.year = Int32.Parse(information[(int)SInfoTypes.year]);

            return sInfo;
        }


        /// <summary>
        /// This function searches for a client in the database and retrieves the information.
        /// </summary>
        /// <param name="cInfo">Where the client info will be stored if it is found.</param>
        /// <param name="HCN">A client health card number to search for.</param>
        /// <param name="firstName">A client first name to search for.</param>
        /// <param name="lastName">A client last name to search for.</param>
        /// <param name="searchHOH">If true HCN will be compared with HOH</param>
        /// <returns>bool : True if successful, false otherwise.</returns>
        public static bool SearchDemographic(ref List<DemographicInfo> cInfo, string HCN="", string firstName="", string lastName="", bool searchHOH=false)
        {
            if (HCN == "" && (firstName == "" || lastName == ""))
            {
                Logging.Logging.MethodCalled("No search parameters were provided.", "FAILURE");
                return false;
            }

            int numRead = 0;

            if (File.Exists(DEMOGRAPHICS_FILE))
            {
                using (StreamReader file = new StreamReader(DEMOGRAPHICS_FILE))
                {
                    string line = "";

                    while (!file.EndOfStream)
                    {
                        line = file.ReadLine();
                        numRead++;

                        if (HCN != "")
                        {
                            if (!searchHOH)
                            {
                                if (line.Split(DELIMITER)[(int)DInfoTypes.HCN] == HCN)
                                {
                                    cInfo.Add(ExtractDemographicInfo(line));
                                }
                            }
                            else
                            {
                                if (line.Split(DELIMITER)[(int)DInfoTypes.headOfHouse] == HCN)
                                {
                                    cInfo.Add(ExtractDemographicInfo(line));
                                }
                            }
                        }
                        else
                        {
                            string[] info = line.Split(DELIMITER);

                            if (info[(int)DInfoTypes.firstName] == firstName &&
                                info[(int)DInfoTypes.lastName] == lastName)
                            {
                                cInfo.Add(ExtractDemographicInfo(line));
                            }
                        }
                    }
                }
            }
			
			if (cInfo.Count == 0)
			{
				Logging.Logging.MethodCalled("Read " + numRead + " VALID record(s) in Demographics DB. No matches found.", "FAILURE");
				return false;
			}

            Logging.Logging.MethodCalled("Read " + numRead + " VALID record(s) in Demographics DB. Found " + cInfo.Count + " matches", "SUCCESS");
            return true;
        }


        /// <summary>
        /// This function gets the scheduling information for a month.
        /// </summary>
        /// <param name="sInfo">Where the scheduling information will be stored.</param>
        /// <param name="month">The month to get the information from.</param>
        /// <param name="year">The year to get the information from.</param>
        /// <returns>bool : True if successful, false otherwise.</returns>
        public static bool GetScheduleInfo(ref List<SchedulingInfo> sInfo, int month, int day)
        {
            string path = SCHEDULING_DIR + months[month] + "/Scheduling-" + months[month] + "-" + day + ".db";
            int numRead = 0;

            if (File.Exists(path))
            {
                using (StreamReader file = new StreamReader(path))
                {
                    string line = "";

                    while (!file.EndOfStream)
                    {
                        line = file.ReadLine();
                        numRead++;

                        sInfo.Add(ExtractSchedulingInfo(line));
                    }
                }
            }
            else {
                Logging.Logging.MethodCalled("Failed to open Scheduling DB (does not exist).", "FAILURE");
                return false;
            }

            Logging.Logging.MethodCalled("Read " + numRead + " VALID record(s) in Scheduling DB.", "SUCCESS");
            return true;
        }


        /// <summary>
        /// This function gets the billing information for a month.
        /// </summary>
        /// <param name="bInfo">Where the billing information will be stored.</param>
        /// <param name="month">The month to get the information from.</param>
        /// <param name="year">The year to get the information from.</param>
        /// <returns>bool : True if successful, false otherwise.</returns>
        public static bool GetBillingInfo(ref List<BillingInfo> bInfo, int month, int year)
        {
            string path = BILLING_DIR + "Billing-" + months[month] + "-" + year + ".db";
            int numRead = 0;

            if (File.Exists(path))
            {
                using (StreamReader file = new StreamReader(path))
                {
                    string line = "";

                    while (!file.EndOfStream)
                    {
                        line = file.ReadLine();
                        numRead++;

                        bInfo.Add(ExtractBillingInfo(line));
                    }
                }
            }
            else
            {
                Logging.Logging.MethodCalled("Failed to open Billing DB (does not exist).", "FAILURE");
                return false;
            }

            Logging.Logging.MethodCalled("Read " + numRead + " VALID record(s) in Billing DB.", "SUCCESS");
            return true;
        }


        /// <summary>
        /// This function searches for and retrieves a specific piece of billing information.
        /// </summary>
        /// <param name="bInfo">Where the billing information will be stored.</param>
        /// <param name="HCN">The client health card number to search for.</param>
        /// <param name="month">The month to get the information from.</param>
        /// <param name="year">The year to get the information from.</param>
        /// <returns>bool : True if successful, false otherwise.</returns>
        public static bool SearchBillingInfo(ref BillingInfo bInfo, string HCN, int month, int year)
        {
            string path = BILLING_DIR + "Billing-" + months[month] + "-" + year + ".db";
            int numRead = 0;

            if (File.Exists(path))
            {
                using (StreamReader file = new StreamReader(path))
                {
                    string line = "";

                    while (!file.EndOfStream)
                    {
                        line = file.ReadLine();
                        numRead++;

                        if (line.Split(DELIMITER)[(int)BInfoTypes.HCN] == HCN)
                        {
                            bInfo = ExtractBillingInfo(line);
                            break;
                        }
                    }
                }
            }

            Logging.Logging.MethodCalled("Read " + numRead + " VALID record(s) in Billing DB.", "SUCCESS");
            return true;
        }


        /// <summary>
        /// This function searches for and retrieves a specific piece of scheduling information.
        /// </summary>
        /// <param name="sInfo">Where the scheduling information will be stored.</param>
        /// <param name="timeSlot">The time slot to search for.</param>
        /// <param name="month">The month to get information from.</param>
        /// <param name="day">The day to get information from.</param>
        /// <returns>bool : True if successful, false otherwise.</returns>
        public static bool SearchSchedulingInfo(ref SchedulingInfo sInfo, int timeSlot, int month, int day)
        {
            string path = SCHEDULING_DIR + months[month] + "/Scheduling-" + months[month] + "-" + day + ".db";
            int numRead = 0;

            if (File.Exists(path))
            {
                using (StreamReader file = new StreamReader(path))
                {
                    string line = "";

                    while (!file.EndOfStream)
                    {
                        line = file.ReadLine();
                        numRead++;

                        if (Int32.Parse(line.Split(DELIMITER)[(int)SInfoTypes.timeSlot]) == timeSlot)
                        {
                            sInfo = ExtractSchedulingInfo(line);
                            break;
                        }
                    }
                }
            }

            Logging.Logging.MethodCalled("Read " + numRead + " VALID record(s) in Scheduling DB.", "SUCCESS");
            return true;
        }

		
        /// <summary>
        /// This function updates an entry in the billing database.
        /// </summary>
        /// <param name="bInfoNew">The updated information.</param>
        /// <param name="searchHCN">The HCN to search for to update.</param>
        /// <param name="month">The month of the entry being updated.</param>
        /// <param name="year">The year of the entry being updated.</param>
        /// <returns>bool : True if successful, false otherwise.</returns>
		public static bool UpdateBillingInfo(BillingInfo bInfoNew)
		{
			string path = BILLING_DIR + "Billing-" + months[bInfoNew.month] + "-" + bInfoNew.year + ".db";
			int numRead = 0;
			int numWritten = 0;

            string billingCodes = "";
            for (int i = 0; i < bInfoNew.billingCodes.Count; i++)
            {
                billingCodes += bInfoNew.billingCodes[i];

                if (i < bInfoNew.billingCodes.Count - 1)
                {
                    billingCodes += BILLCODE_DELIM;
                }
            }

            string newLine = string.Format("{1}{0}{2}{0}{3}{0}{4}{0}{5}{0}{6}", DELIMITER, bInfoNew.HCN, bInfoNew.sex, bInfoNew.day, 
                bInfoNew.month, bInfoNew.year, billingCodes);
			
			List<string> dbLines = new List<string>();
			bool lineFound = false;
			
			if (File.Exists(path))
			{
				using (StreamReader file = new StreamReader(path))
				{
					string line = "";
					
					while (!file.EndOfStream)
					{
						line = file.ReadLine();
						numRead++;
						
						if (!lineFound && bInfoNew.HCN == line.Split(DELIMITER)[(int)BInfoTypes.HCN])
						{
							dbLines.Add(newLine);
							lineFound = true;
							continue;
						}
						
						dbLines.Add(line);
					}
				}
				
				File.WriteAllText(path, string.Empty);

                WriteLines(path, dbLines.ToArray());
                numWritten = dbLines.Count;
			}

            Logging.Logging.MethodCalled("Read " + numRead + " VALID record(s) in Scheduling DB. Wrote " + numWritten + " record(s) in Scheduling DB.", "SUCCESS");

            return true;
		}


        /// <summary>
        /// This function removes a line from the Scheduling DB.
        /// </summary>
        /// <param name="HCN">The HCN of the line to remove.</param>
        /// <param name="month">The month of the entry.</param>
        /// <param name="day">The day of the entry.</param>
        /// <returns>bool : True if successful, false otherwise.</returns>
        public static bool RemoveSchedulingInfo(string HCN, int month, int day)
        {
            string path = SCHEDULING_DIR + months[month] + "/Scheduling-" + months[month] + "-" + day + ".db";
            int numRead = 0;
            int numWritten = 0;

            if (File.Exists(path))
            {
                List<string> lines = new List<string>();

                using (StreamReader file = new StreamReader(path))
                {
                    string line = "";

                    while (!file.EndOfStream)
                    {
                        line = file.ReadLine();
                        numRead++;

                        if (line.Split(DELIMITER)[(int)SInfoTypes.HCN] != HCN)
                        {
                            lines.Add(line);
                        }
                    }
                }

                WriteLines(path, lines.ToArray());
            }

            Logging.Logging.MethodCalled("Read " + numRead + " VALID record(s) in Scheduling DB. Wrote " + numWritten + " record(s) in Scheduling DB.", "SUCCESS");
            return true;
        }
    }
}
