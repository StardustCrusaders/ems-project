﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
/*
 * Description: This class so far only has parsing response file
 *              and billing summary. 
 * Programmer:  Firas Areibi
 * 
 *  Warning:    This code is very ugly to look at :'(
 */
 
namespace Billing
{
    class Response
    {
        List<string> tempList = new List<string>();
        
        public void MatchCodes(string dateOfService,string HCN,string Gender, string billingCode)
        {
            string value = "";
            //string billingCode = "";
            string[] codes = File.ReadAllLines(@"C:\Users\Firas\Desktop\Software Quality l\EMS Project\Billing UI\UpdatedMasterFIle.txt");
            string[] billingCodes = billingCode.Split(' ');
            string[] tempHold = new string[codes.Length];
            string[] holdingZeros = new string[tempHold.Length];
            string[] billingFee = new string[codes.Length];
            string contents = string.Empty;
            tempList.Clear();

            for (int i = 0; i < billingCodes.Length; i++)
            {
                foreach (var line in codes)
                {
                    //see if billing code matches fee
                    if (line.Contains(billingCodes[i]))
                    {
                        //if length is less then these numbers fee =0
                        if (line.Length == 12 || line.Length == 20)
                        {
                            value = "00000000000";
                            tempList.Add(value);
                        }
                        else
                        {
                            Regex first12CharPatt = new Regex(@"[A-Z]{1}[0-9]{11}");
                            tempHold = first12CharPatt.Split(line);
                            tempList.Add(tempHold[1]);
                            break;
                        }
                    }
                }
            }
            tempHold = tempList.ToArray();
            tempList.Clear();
            foreach (var line in tempHold)
            {
                if (line.Length == 11)
                {
                    //Regex zeroParse = new Regex(@"[0]{4}");
                    //holdingZeros = zeroParse.Split(line, 2);
                    //string tempstr = holdingZeros[1];
                    //tempstr = tempstr.Insert(5, ".");
                    //holdingZeros = tempstr.Split('.');
                    //tempList.Add(holdingZeros[0]);
                    tempList.Add(line);
                }
                else if (line.Length == 19)
                {
                    Regex zeroParse = new Regex(@"[0-9]{8}");
                    holdingZeros = zeroParse.Split(line, 2);
                    tempList.Add(holdingZeros[1]);
                }
                else if (line.Length == 22)
                {
                    tempList.Add(line);
                }
                else if (line.Length == 30)
                {
                    Regex zeroParse = new Regex(@"[0-9]{8}");
                    holdingZeros = zeroParse.Split(line, 2);
                    tempList.Add(holdingZeros[1]);
                }
                else if (line.Length == 33)
                {
                    tempList.Add(line);
                }
                else if (line.Length == 41)
                {
                    Regex zeroParse = new Regex(@"[0-9]{8}");
                    holdingZeros = zeroParse.Split(line, 2);
                    tempList.Add(holdingZeros[1]);
                }
                else if (line.Length == 44)
                {
                    //Regex zeroParse = new Regex(@"[0]{4}");
                    //holdingZeros = zeroParse.Split(line, 2);
                    //holdingZeros[0] = holdingZeros[1].Insert(5, ".");
                    //holdingZeros = holdingZeros[0].Split('.');
                    //tempList.Add(holdingZeros[0]);
                    tempList.Add(line);

                }
                else if (line.Length == 52)
                {
                    Regex zeroParse = new Regex(@"[0-9]{8}");
                    holdingZeros = zeroParse.Split(line, 2);
                    tempList.Add(holdingZeros[1]);
                }
            }
                billingFee= tempList.ToArray();
                tempList.Clear();
                for (int i =0; i<billingCodes.Length; i++)
                { 
                //once 
                    contents = $"{dateOfService}{HCN}{Gender}{billingCodes[i]}{billingFee[i]}";
                    CreateBillingFile(contents);
                }

        }

        public void CreateBillingFile(string contents)
        {
            try
            {
                string newFile = @"./BillingFile"+ DateTime.Now.ToString("yyyy-MM") + ".txt";
                if (!File.Exists(newFile))
                {
                    FileStream billingFile= File.Create(newFile);
                    billingFile.Close();
                }

                InsertToBillingFile(newFile, contents);
                    
            }
            catch(Exception e)
            {
                //log
                
            }

        }

        public void InsertToBillingFile(string filePath, string contents)
        {
            File.AppendAllText(filePath, contents + Environment.NewLine);
        }


         public void ReadResponseFile(string path)
        {
            double encounters = 0;
            double encounterFhcvCmoh = 0;
            string[] parsingValues = new string[] { }; ;
            string[] parsingValuesOutOfStates = new string[] { };
            string[] insertingDecimalInString = new string[] { };
            string[] insertDecimalInString = new string[] { };
            int[] values= new int[] { };
            int[] allValues= new int[] { };
            string dateofService = string.Empty;
            double requestedSum = 0.0;
            double paidSum = 0.0;
            double averageBilling = 0.0;
            double receivedPercent = 0.0;
            double totalBilledProcedure = 0.0;
            try
            {


                using (StreamReader r = new StreamReader(path))
                {
                    //how many encounters are in file
                    while (r.ReadLine() != null)
                    {

                        encounters++;
                    }
                    r.Close();
                }

                string[] invoiced = File.ReadAllLines(path);
                foreach (var line in invoiced)
                {
                    //counts states
                    if (line.Contains("CMOH") || line.Contains("FHCV"))
                    {
                        encounterFhcvCmoh++;
                    }

                    //this will parse date, hcn,and the billing code out of the line
                    //201711201234567890 KVFA 6650000
                    Regex patt = new Regex(@"[0-9]{18}[A-Z]{4}[0-9]{7}");
                    parsingValues=patt.Split(line);
                    string tempstring= parsingValues[1];
                    tempList.Add(tempstring);
                    if (dateofService == string.Empty)
                    {
                        dateofService = Regex.Match(line, "[0-9]{6}").Value;
                    }

                }
               
                Array.Resize(ref parsingValues, tempList.Count+1);
                
                parsingValues = tempList.ToArray();
                tempList.Clear();

                //parse out paid values from response
                foreach (var line in parsingValues)
                {
                    string[] tempStringHold = new string[parsingValues.Length];
                    if (line.Length==0)
                    {

                    }
                    else
                    {
                        if (line.Contains("PAID"))
                        {
                            Regex states = new Regex(@"[PAID]{4}");
                            parsingValuesOutOfStates = states.Split(line, 2);
                            string tempString = parsingValuesOutOfStates[0];

                            if(tempString.Length==7)
                            {
                                tempList.Add(tempString);
                            }
                            else if (tempString.Length == 18)
                            {
                                tempString = tempString.Insert(7, "-");
                                tempStringHold = tempString.Split(new string[] { "-0000" }, StringSplitOptions.RemoveEmptyEntries);

                                for (int i = 0; i < tempStringHold.Length; i++)
                                {
                                        tempList.Add(tempStringHold[i]);
                                }
                            }
                            else if (tempString.Length == 29)
                            {
                                tempString = tempString.Insert(7, "-");
                                tempString = tempString.Insert(19, "-");
                                tempStringHold = tempString.Split(new string[] { "-0000" }, StringSplitOptions.RemoveEmptyEntries);

                                for (int i = 0; i < tempStringHold.Length; i++)
                                {
                                        tempList.Add(tempStringHold[i]);   
                                }
                            }
                            else if (tempString.Length == 40)
                            {
                                tempString = tempString.Insert(7, "-");
                                tempString = tempString.Insert(19, "-");
                                tempString = tempString.Insert(31, "-");
                                tempStringHold = tempString.Split(new string[] { "-0000" }, StringSplitOptions.RemoveEmptyEntries);

                                for (int i = 0; i < tempStringHold.Length; i++)
                                {
                                        tempList.Add(tempStringHold[i]);
                                }

                            }

                            
                            //tempList.Add();
                        }
                    }
                }
                Array.Resize(ref insertingDecimalInString, tempList.Count+1);

                insertingDecimalInString = tempList.ToArray();
                tempList.Clear();

                //parses out other state codes attached to response file
                foreach (var line in parsingValues)
                {
                    if (!line.Contains("PAID"))
                    {


                        if (line.Contains("DECL"))
                        {
                            Regex states = new Regex(@"[DECL]{4}");
                            insertDecimalInString = states.Split(line, 2);

                            string tempStrings = insertDecimalInString[0];
                            string[] tempStringHold = new string[parsingValues.Length];
                            if (tempStrings.Length == 7)
                            {
                                tempList.Add(tempStrings);
                            }
                            else if (tempStrings.Length == 18)
                            {
                                tempStrings = tempStrings.Insert(7, "-");
                                tempStringHold = tempStrings.Split(new string[] { "-0000" }, StringSplitOptions.RemoveEmptyEntries);

                                for (int i = 0; i < tempStringHold.Length; i++)
                                {
                                    tempList.Add(tempStringHold[i]);
                                }
                            }
                            else if (tempStrings.Length == 29)
                            {
                                tempStrings = tempStrings.Insert(7, "-");
                                tempStrings = tempStrings.Insert(19, "-");
                                tempStringHold = tempStrings.Split(new string[] { "-0000" }, StringSplitOptions.RemoveEmptyEntries);

                                for (int i = 0; i < tempStringHold.Length; i++)
                                {
                                    tempList.Add(tempStringHold[i]);
                                }
                            }
                            else if (tempStrings.Length == 40)
                            {
                                tempStrings = tempStrings.Insert(7, "-");
                                tempStrings = tempStrings.Insert(19, "-");
                                tempStrings = tempStrings.Insert(31, "-");
                                tempStringHold = tempStrings.Split(new string[] { "-0000" }, StringSplitOptions.RemoveEmptyEntries);

                                for (int i = 0; i < tempStringHold.Length; i++)
                                {
                                    tempList.Add(tempStringHold[i]);
                                }

                            }


                        }
                        else if (line.Contains("FHCV"))
                        {
                            Regex states = new Regex(@"[FHCV]{4}");
                            insertDecimalInString = states.Split(line, 2);

                            string tempStrings = insertDecimalInString[0];
                            string[] tempStringHold = new string[parsingValues.Length];
                            if (tempStrings.Length == 7)
                            {
                                tempList.Add(tempStrings);
                            }
                            else if (tempStrings.Length == 18)
                            {
                                tempStrings = tempStrings.Insert(7, "-");
                                tempStringHold = tempStrings.Split(new string[] { "-0000" }, StringSplitOptions.RemoveEmptyEntries);

                                for (int i = 0; i < tempStringHold.Length; i++)
                                {
                                    tempList.Add(tempStringHold[i]);
                                }
                            }
                            else if (tempStrings.Length == 29)
                            {
                                tempStrings = tempStrings.Insert(7, "-");
                                tempStrings = tempStrings.Insert(19, "-");
                                tempStringHold = tempStrings.Split(new string[] { "-0000" }, StringSplitOptions.RemoveEmptyEntries);

                                for (int i = 0; i < tempStringHold.Length; i++)
                                {
                                    tempList.Add(tempStringHold[i]);
                                }
                            }
                            else if (tempStrings.Length == 40)
                            {
                                tempStrings = tempStrings.Insert(7, "-");
                                tempStrings = tempStrings.Insert(19, "-");
                                tempStrings = tempStrings.Insert(31, "-");
                                tempStringHold = tempStrings.Split(new string[] { "-0000" }, StringSplitOptions.RemoveEmptyEntries);

                                for (int i = 0; i < tempStringHold.Length; i++)
                                {
                                    tempList.Add(tempStringHold[i]);
                                }

                            }
                        }
                        else if (line.Contains("CMOH"))
                        {
                            Regex states = new Regex(@"[CMOH]{4}");
                            insertDecimalInString = states.Split(line, 2);

                            string tempStrings = insertDecimalInString[0];
                            string[] tempStringHold = new string[parsingValues.Length];
                            if (tempStrings.Length == 7)
                            {
                                tempList.Add(tempStrings);
                            }
                            else if (tempStrings.Length == 18)
                            {
                                tempStrings = tempStrings.Insert(7, "-");
                                tempStringHold = tempStrings.Split(new string[] { "-0000" }, StringSplitOptions.RemoveEmptyEntries);

                                for (int i = 0; i < tempStringHold.Length; i++)
                                {
                                    tempList.Add(tempStringHold[i]);
                                }
                            }
                            else if (tempStrings.Length == 29)
                            {
                                tempStrings = tempStrings.Insert(7, "-");
                                tempStrings = tempStrings.Insert(19, "-");
                                tempStringHold = tempStrings.Split(new string[] { "-0000" }, StringSplitOptions.RemoveEmptyEntries);

                                for (int i = 0; i < tempStringHold.Length; i++)
                                {
                                    tempList.Add(tempStringHold[i]);
                                }
                            }
                            else if (tempStrings.Length == 40)
                            {
                                tempStrings = tempStrings.Insert(7, "-");
                                tempStrings = tempStrings.Insert(19, "-");
                                tempStrings = tempStrings.Insert(31, "-");
                                tempStringHold = tempStrings.Split(new string[] { "-0000" }, StringSplitOptions.RemoveEmptyEntries);

                                for (int i = 0; i < tempStringHold.Length; i++)
                                {
                                    tempList.Add(tempStringHold[i]);
                                }

                            }
                        }
                        //string tempString = insertDecimalInString[0];
                        //tempList.Add(tempString);
                    }
                }

                Array.Resize(ref insertDecimalInString, tempList.Count+1);

                insertDecimalInString = tempList.ToArray();
                tempList.Clear();

                //TBP (TOTAL BILLED PROCEDURES
                for (int i = 0; i < insertDecimalInString.Length; i++)
                {
                    double value = 0.0;
                    string temp= insertDecimalInString[i].Insert(3, ".");
                    string[] cleanUpNumbers = temp.Split(new string[] { "00" }, StringSplitOptions.RemoveEmptyEntries);
                    temp = cleanUpNumbers[0];
                    if (temp.StartsWith("0"))
                    {
                         cleanUpNumbers = temp.Split(new string[] { "0" }, StringSplitOptions.RemoveEmptyEntries);
                    }
                    temp = cleanUpNumbers[0];
                    if(double.TryParse(temp, out value))
                    {
                        requestedSum += value;
                    }

                   // requestedSum += value;
                    

                //allValues[i] = Convert.ToInt32(insertDecimalInString[i].Insert(3, "."));
                //requestedSum += allValues[i];
                }

                
                //RECEIVED TOTALS
                for (int i = 0; i < insertingDecimalInString.Length; i++)
                {
                    double value=0.0;
                    string temp = insertingDecimalInString[i].Insert(3, ".");
                    string[] cleanUpNumbers = temp.Split(new string[] { "00" }, StringSplitOptions.RemoveEmptyEntries);
                    temp = cleanUpNumbers[0];
                    if (temp.StartsWith("0"))
                    {
                        cleanUpNumbers = temp.Split(new string[] { "0" }, StringSplitOptions.RemoveEmptyEntries);
                    }
                    temp = cleanUpNumbers[0];

                    if (double.TryParse(temp, out value))
                    {
                        paidSum += value;
                    }
                     
                }

                //insert decimal 3 spaces after
                /*foreach (var line in parsingValuesOutOfStates)
                {

                    for (int i = 0; line != null; i++)
                    {
                        insertingDecimalInString[i] = line.Insert(3, ".");
                        values[i] = Convert.ToInt32(insertingDecimalInString[i]);
                        //gather recieved total
                        paidSum += values[i];
                    }
                }*/

                //total encounters
                //encounters

                //totalbilledprocedure
                totalBilledProcedure = paidSum + requestedSum;

                //received total
                //Paidsum

                //RECEIVED % 
                receivedPercent = (paidSum / (paidSum + requestedSum) * 100);

                //averagebilling rt/teb
                averageBilling = (paidSum / encounters);

                //# of encounters for states(cmoh, fhcv)
                //encounterFhcvCmoh

                //Monthly billing summary method below
                CreateMonthlyBillingSummary(path, dateofService, encounters, totalBilledProcedure, paidSum, receivedPercent, averageBilling, encounterFhcvCmoh);
            }
            catch(Exception e)
            {
                Console.WriteLine("{0}", e);
                //log error file does not exist
            }

            
        }

        public void CreateMonthlyBillingSummary(string path, string dateOfService, double encounters, double totalBilled, double receivedTotal, double receivedPercentage, double averageBilling, double numberOfStateEncounters)
        {

            dateOfService = dateOfService.Insert(4, "-");
            receivedPercentage = Math.Round(receivedPercentage, 2);
            averageBilling = Math.Round(averageBilling, 2);


            string newPath = "";
            int index = path.LastIndexOf('\\');
            string content = $"Total Encounters Billed: {encounters}{Environment.NewLine}Total Billed Procedures: {totalBilled}{Environment.NewLine}Received Total: {receivedTotal}{Environment.NewLine}Received Precentage: {receivedPercentage}{Environment.NewLine}Average Billing: {averageBilling}{Environment.NewLine}Encounters to Follow-up: {numberOfStateEncounters}";
            if (index!=-1)
            {
                newPath = path.Substring(0, index);
            }
            newPath = newPath + @"./BillingSummary-" + dateOfService + ".txt";

            if (!File.Exists(newPath))
            {
                FileStream billingSummaryFile =  File.Create(newPath);
                billingSummaryFile.Close();
            }

            File.WriteAllText(newPath, content);
        }
    }
    
}
