﻿using System.Collections.Generic;

namespace Common
{
    public delegate void OnClick();
    public delegate bool Validator(string text);

    /// <summary>
    /// This struct holds an (x, y) coordinate for the console.
    /// </summary>
    public struct Coordinates
    {
        public Coordinates(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public int x;
        public int y;
    }

    static class Common
    {
        public static UI.BaseMenu currentMenu { get; set; }
        public static Stack<UI.BaseMenu> lastMenu = new Stack<UI.BaseMenu>();
    }
}
