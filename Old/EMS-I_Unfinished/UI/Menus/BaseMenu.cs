﻿using System;

namespace UI
{
    /// <summary>
    /// This class is a template for all console menu pages.
    /// 
    /// Programmers:    William Bicknell, Jacob Funes, Firas Areibi, Moises Diaz<br/>
    /// Date:           2018-11-08
    /// </summary>
    public class BaseMenu
    {
        private readonly MenuItem[] items;
        private readonly Label[] labels;
        private readonly string title;
        private int selectedIndex = 0;
        private ConsoleKeyInfo lastKeyPressInfo = new ConsoleKeyInfo();
        private ConsoleColor bgColour;
        private ConsoleColor titleColour;
        public bool isSubmitExit = false;
        public bool isUpdateExit = false;

        /// <summary>
        /// The constructor for the BaseMenu class.
        /// </summary>
        /// <param name="menuItems">The menu items that will be drawn to the screen.</param>
        /// <param name="title">The title for the menu.</param>
        public BaseMenu(MenuItem[] menuItems, string title, Label[] labels = null, ConsoleColor titleColour = ConsoleColor.White, ConsoleColor backgroundColour = ConsoleColor.Black)
        {
            items = menuItems;
            this.labels = labels;
            this.title = title;
            this.titleColour = titleColour;
            bgColour = backgroundColour;
        }


        /// <summary>
        /// This function displays the title and all MenuItems in the menu.
        /// </summary>
        public void Display()
        {
            Console.BackgroundColor = bgColour;
            Console.ForegroundColor = titleColour;

            Console.Clear();
            
            Console.SetCursorPosition(0, 0);

            int leftBuffer = (Console.WindowWidth / 2) - (title.Length / 2);

            // Draw title
            Console.WriteLine("\n" + new string(' ', leftBuffer) + title + "\n");

            // Draw separator
            Console.WriteLine(new string('─', Console.WindowWidth));

            // Draw MenuItems
            foreach (MenuItem item in items)
            {
                item.Display();
            }

            // Draw Labels
            if (labels != null)
            {
                foreach (Label label in labels)
                {
                    label.Display();
                }
            }

            items[selectedIndex].Select();
        }


        /// <summary>
        /// This function handles input from the user for the menu.
        /// </summary>
        public void CheckInput()
        {
            bool running = true;
            isSubmitExit = false;
            isUpdateExit = false;

            while (running)
            {
                lastKeyPressInfo = Console.ReadKey(true);

                switch (lastKeyPressInfo.Key)
                {
                    case ConsoleKey.Enter:
                        items[selectedIndex].Click();

                        if (items[selectedIndex] is DropDown)
                        {
                            Display();
                        }
                        else if (items[selectedIndex] is SubmitButton)
                        {
                            if (Validate())
                            {
                                running = false;
                                selectedIndex = 0;
                                isSubmitExit = true;
                                continue;
                            }
                        }
                        else if (items[selectedIndex] is SwitchPageButton)
                        {
                            running = false;
                            selectedIndex = 0;
                            continue;
                        }
                        else if (items[selectedIndex] is Calendar)
                        {
                            running = false;
                            selectedIndex = 0;
                            isUpdateExit = true;
                            continue;
                        }

                        break;

                    case ConsoleKey.UpArrow:
                        items[selectedIndex].Unselect();
                        selectedIndex--;

                        if (selectedIndex < 0)
                        {
                            selectedIndex = items.Length - 1;
                        }

                        items[selectedIndex].Select();

                        break;

                    case ConsoleKey.DownArrow:
                        items[selectedIndex].Unselect();
                        selectedIndex++;

                        if (selectedIndex >= items.Length)
                        {
                            selectedIndex = 0;
                        }

                        items[selectedIndex].Select();

                        break;
                }

                if (running)
                {
                    int i = 0;
                    foreach (MenuItem item in items)
                    {
                        if (item.GetHotKey() == lastKeyPressInfo.KeyChar)
                        {
                            items[selectedIndex].Unselect();
                            item.Select();
                            selectedIndex = i;

                            item.Click();

                            if (items[selectedIndex] is DropDown)
                            {
                                Display();
                            }
                        }

                        i++;
                    }
                }
            }
        }


        /// <summary>
        /// This function gets the values from all MenuItems in the menu.
        /// </summary>
        /// <returns>string[] : The values of the MenuItems.</returns>
        public string[] GetUserInputs()
        {
            string[] userInputs = new string[items.Length];

            int i = 0;
            foreach (MenuItem item in items)
            {
                string input = item.GetValue();

                if (input != null)
                {
                    userInputs[i] = input;
                    i++;
                }
            }

            return userInputs;
        }


        /// <summary>
        /// This function runs validation on all menu items.
        /// </summary>
        /// <returns>bool : true if all are valid, false otherwise.</returns>
        public bool Validate()
        {
            bool isValid = true;

            foreach (MenuItem item in items)
            {
                if (!item.Validate())
                {
                    isValid = false;
                }
            }

            Display();

            return isValid;
        }
    }
}
