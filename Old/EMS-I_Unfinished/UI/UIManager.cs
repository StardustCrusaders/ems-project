﻿using System;
using System.Collections.Generic;
using Common;
using static Demographics.Demographics;
using static FileIO.FileIO;

//TODO: Scheduling function that gets all fully booked days for a month
//TODO: Logging function to generate folder

namespace UI
{
    /// <summary>
    /// This class is used to manage the UI of the program.
    /// 
    /// Programmers:    William Bicknell, Jacob Funes, Firas Areibi, Moises Diaz<br/>
    /// Date:           2018-11-28
    /// </summary>
    public class UIManager
    {
        private BaseMenu MainMenu;
        private BaseMenu PatientMenu;
        private BaseMenu AddPatientMenu;
        private BaseMenu SearchPatientMenu;
        private BaseMenu AppointmentMenu;
        private BaseMenu ScheduleMenu;
        private BaseMenu SearchAppointmentMenu;
        private BaseMenu BillingMenu;

        private enum PatientInfoOrder
        {
            HCN,
            FirstName,
            LastName,
            mInitial,
            DOB,
            sex,
            HeadOfHouse,
            Address1,
            Address2,
            City,
            Province,
            PhoneNumber
        }

        private const int WINDOW_WIDTH = 120;
        private const int WINDOW_HEIGHT = 54;

        public UIManager()
        {
            // Window size is number of coloumns and rows
            Console.SetWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
            // Set buffer size the same as window size to get rid of scroll bars
            Console.SetBufferSize(WINDOW_WIDTH, WINDOW_HEIGHT);

            Console.CursorVisible = false;

            // Set window title
            Console.Title = "EMS V1";

            BillingMenu = new BaseMenu
            (
                new MenuItem[]
                {
                    new SwitchPageButton(0, 20, "Generate Monthly Billing File", ConsoleColor.White, ConsoleColor.Yellow, ScheduleMenu, 'f', true),
                    new SwitchPageButton(0, 25, "Generate Monthly Billing Summary", ConsoleColor.White, ConsoleColor.Yellow, SearchAppointmentMenu, 's', true),
                    new SwitchPageButton(5, WINDOW_HEIGHT - 5, "Back", ConsoleColor.White, ConsoleColor.Yellow, null, 'b')
                },

                "Billing Management"
            );

            ScheduleMenu = new BaseMenu
            (
                new MenuItem[]
                {
                    new Calendar(0, 0, new int[] { 2, 5, 7 }, new int[] { 31, 30, 31 }, new string[] { "October", "November", "December" }, new int[][] { new int[] { 0 }, new int[] { 0 }, new int[] { 0 } }, ConsoleColor.White, ConsoleColor.Yellow, ConsoleColor.Gray),
                    new SwitchPageButton(5, WINDOW_HEIGHT - 5, "Back", ConsoleColor.White, ConsoleColor.Yellow, null, 'b')
                },

                "Schedule Appointment"
            );

            SearchAppointmentMenu = new BaseMenu
            (
                new MenuItem[]
                {
                    new TextBox(0, 10, 12, "HCN", HCNValidator, "", ConsoleColor.White, ConsoleColor.Yellow, center: true),
                    new TextBox(0, 16, 25, "First Name", NoNumbersValidator, "", ConsoleColor.White, ConsoleColor.Yellow, center: true),
                    new TextBox(0, 22, 25, "Last Name", NoNumbersValidator, "", ConsoleColor.White, ConsoleColor.Yellow, center: true),
                    new DataViewer(0, 28, new string[] { "HCN", "First Name", "Last Name", "Head of House HCN" }, null, ConsoleColor.White, ConsoleColor.Yellow, true),
                    new SubmitButton(0, WINDOW_HEIGHT - 5, "Search", ConsoleColor.White, ConsoleColor.Yellow, 's', true, false),
                    new SwitchPageButton(5, WINDOW_HEIGHT - 5, "Back", ConsoleColor.White, ConsoleColor.Yellow, null, 'b')
                },

                "Search for Appointments",

                new Label[]
                {
                    new Label(15, 10, "Search by:", ConsoleColor.White),
                    new Label(0, 14, "OR", ConsoleColor.White, true),
                    new Label(0, 20, "AND", ConsoleColor.White, true)
                }
            );

            AppointmentMenu = new BaseMenu
            (
                new MenuItem[]
                {
                    new SwitchPageButton(0, 20, "Schedule Appointment", ConsoleColor.White, ConsoleColor.Yellow, ScheduleMenu, 'a', true),
                    new SwitchPageButton(0, 25, "Search for Appointment", ConsoleColor.White, ConsoleColor.Yellow, SearchAppointmentMenu, 's', true),
                    new SwitchPageButton(5, WINDOW_HEIGHT - 5, "Back", ConsoleColor.White, ConsoleColor.Yellow, null, 'b')
                },

                "Appointment Management"
            );

            SearchPatientMenu = new BaseMenu
            (
                new MenuItem[]
                {
                    new TextBox(0, 10, 12, "HCN", HCNValidator, "", ConsoleColor.White, ConsoleColor.Yellow, center: true),
                    new TextBox(0, 16, 25, "First Name", NoNumbersValidator, "", ConsoleColor.White, ConsoleColor.Yellow, center: true),
                    new TextBox(0, 22, 25, "Last Name", NoNumbersValidator, "", ConsoleColor.White, ConsoleColor.Yellow, center: true),
                    new DataViewer(0, 28, new string[] { "HCN", "Name", "Head of House HCN", "Appointment Date", "Appointment Timeslot" }, null, ConsoleColor.White, ConsoleColor.Yellow, true),
                    new SubmitButton(0, WINDOW_HEIGHT - 5, "Search", ConsoleColor.White, ConsoleColor.Yellow, 's', true, false),
                    new SwitchPageButton(5, WINDOW_HEIGHT - 5, "Back", ConsoleColor.White, ConsoleColor.Yellow, null, 'b')
                },

                "Search For Patient",

                new Label[]
                {
                    new Label(15, 10, "Search by:", ConsoleColor.White),
                    new Label(0, 14, "OR", ConsoleColor.White, true),
                    new Label(0, 20, "AND", ConsoleColor.White, true)
                }
            );

            AddPatientMenu = new BaseMenu
            (
                new MenuItem[]
                {
                    new TextBox(1, 10, 12, "HCN* ", HCNValidator, "Invalid HCN!", ConsoleColor.White, ConsoleColor.Yellow, true),
                    new TextBox(1, 15, 25, "First Name* ", NoNumbersValidator, "Cannot contain numbers!", ConsoleColor.White, ConsoleColor.Yellow, true),
                    new TextBox(1, 20, 25, "Last Name* ", NoNumbersValidator, "Cannot contain numbers!", ConsoleColor.White, ConsoleColor.Yellow, true),
                    new TextBox(1, 25, 1, "M. Initial", NoNumbersValidator, "Cannot contain numbers!", ConsoleColor.White, ConsoleColor.Yellow),
                    new TextBox(1, 30, 12, "D.O.B* ", DoBValidator, "", ConsoleColor.White, ConsoleColor.Yellow, true),
                    new DropDown(1, 35, new string[] { "M", "F", "I", "H" }, "Sex* ", "- - -", ConsoleColor.White, ConsoleColor.Yellow, "M"),
                    new TextBox(60, 10, 12, "Head of House", HCNValidator, "Invalid HCN!", ConsoleColor.White, ConsoleColor.Yellow),
                    new TextBox(60, 15, 25, "Address 1** ", AddressValidator, "Invalid address!", ConsoleColor.White, ConsoleColor.Yellow),
                    new TextBox(60, 20, 25, "Address 2", AddressValidator, "Invalid address!", ConsoleColor.White, ConsoleColor.Yellow),
                    new TextBox(60, 25, 25, "City** ", NoNumbersValidator, "Cannot contain numbers!", ConsoleColor.White, ConsoleColor.Yellow),
                    new DropDown(60, 30, new string[] { "ON", "AB", "BC", "MB", "NB", "NL", "NT", "NS", "NU", "PE", "QC", "SK", "YT" }, "Province** ", "- - -", ConsoleColor.White, ConsoleColor.Yellow, "ON"),
                    new TextBox(60, 35, 12, "Phone Number** ", PhoneValidator, "Expected form: xxx-xxx-xxxx", ConsoleColor.White, ConsoleColor.Yellow),
                    new SubmitButton(WINDOW_WIDTH - 15, WINDOW_HEIGHT - 5, "Submit", ConsoleColor.White, ConsoleColor.Yellow, 's'),
                    new SwitchPageButton(5, WINDOW_HEIGHT - 5, "Back", ConsoleColor.White, ConsoleColor.Yellow, null, 'b')
                },

                "Add Patient",

                new Label[]
                {
                    new Label(0, WINDOW_HEIGHT - 4, "* - Required    ** - Required if Head of House is not filled", ConsoleColor.Red, true)
                }
            );

            PatientMenu = new BaseMenu
            (
                new MenuItem[]
                {
                    new SwitchPageButton(0, 20, "Add Patient", ConsoleColor.White, ConsoleColor.Yellow, AddPatientMenu, 'a', true),
                    new SwitchPageButton(0, 25, "Search for Patient", ConsoleColor.White, ConsoleColor.Yellow, SearchPatientMenu, 's', true),
                    new SwitchPageButton(5, WINDOW_HEIGHT - 5, "Back", ConsoleColor.White, ConsoleColor.Yellow, null, 'b')
                },

                "Patient Management"
            );

            MainMenu = new BaseMenu
            (
                new MenuItem[]
                {
                    new SwitchPageButton(0, 20, "Patient Management", ConsoleColor.White, ConsoleColor.Yellow, PatientMenu, 'p', true),
                    new SwitchPageButton(0, 25, "Appointment Management", ConsoleColor.White, ConsoleColor.Yellow, AppointmentMenu, 'a', true),
                    new SwitchPageButton(0, 30, "Billing Management", ConsoleColor.White, ConsoleColor.Yellow, BillingMenu, 'b', true),
                    new Button(5, WINDOW_HEIGHT - 5, "Exit", ConsoleColor.White, ConsoleColor.Yellow, Exit, 'x')
                },

                "EMS - Main Menu"
            );

            Common.Common.currentMenu = MainMenu;
        }

        public void RunUI()
        {
            while (true)
            {
                if (Common.Common.currentMenu == MainMenu)
                {
                    MainMenu.Display();
                    MainMenu.CheckInput();
                }
                else if (Common.Common.currentMenu == PatientMenu)
                {
                    PatientMenu.Display();
                    PatientMenu.CheckInput();
                }
                else if (Common.Common.currentMenu == AddPatientMenu)
                {
                    AddPatientMenu.Display();
                    AddPatientMenu.CheckInput();

                    if (AddPatientMenu.isSubmitExit)
                    {
                        string[] userData = AddPatientMenu.GetUserInputs();
                        DemographicInfo dInfo;

                        if (userData[(int)PatientInfoOrder.HeadOfHouse] == "")
                        {
                            dInfo = new DemographicInfo(
                                userData[(int)PatientInfoOrder.HCN],
                                userData[(int)PatientInfoOrder.FirstName],
                                userData[(int)PatientInfoOrder.LastName],
                                userData[(int)PatientInfoOrder.mInitial],
                                userData[(int)PatientInfoOrder.DOB],
                                userData[(int)PatientInfoOrder.sex],
                                userData[(int)PatientInfoOrder.Address1],
                                userData[(int)PatientInfoOrder.Address2],
                                userData[(int)PatientInfoOrder.City],
                                userData[(int)PatientInfoOrder.Province],
                                userData[(int)PatientInfoOrder.PhoneNumber]);
                        }
                        else
                        {
                            dInfo = new DemographicInfo(
                                userData[(int)PatientInfoOrder.HCN],
                                userData[(int)PatientInfoOrder.FirstName],
                                userData[(int)PatientInfoOrder.LastName],
                                userData[(int)PatientInfoOrder.mInitial],
                                userData[(int)PatientInfoOrder.DOB],
                                userData[(int)PatientInfoOrder.sex],
                                userData[(int)PatientInfoOrder.HeadOfHouse]);
                        }

                        WriteDemographic(dInfo);
                    }
                }
                else if (Common.Common.currentMenu == SearchPatientMenu)
                {
                    SearchPatientMenu.Display();
                    SearchPatientMenu.CheckInput();

                    if (SearchPatientMenu.isSubmitExit)
                    {
                        string[] userData = SearchPatientMenu.GetUserInputs();
                        
                        List<DemographicInfo> searchResults = new List<DemographicInfo>();
                        SearchDemographic(ref searchResults, userData[0], userData[1], userData[2]);
                        SearchDemographic(ref searchResults, userData[0], userData[1], userData[2], true);

                        string[][] infoLines = new string[searchResults.Count][];
                        int i = 0;
                        foreach (DemographicInfo dInfo in searchResults)
                        {
                            infoLines[i] = new string[] { dInfo.HCN, dInfo.firstName + " " + dInfo.mInitial.ToString() + " " + dInfo.lastName, dInfo.headOfHouse };
                            i++;
                        }

                        SearchPatientMenu = new BaseMenu
                        (
                            new MenuItem[]
                            {
                                new TextBox(0, 10, 12, "HCN", null, "", ConsoleColor.White, ConsoleColor.Yellow, center: true),
                                new TextBox(0, 16, 25, "First Name", null, "", ConsoleColor.White, ConsoleColor.Yellow, center: true),
                                new TextBox(0, 22, 25, "Last Name", null, "", ConsoleColor.White, ConsoleColor.Yellow, center: true),
                                new DataViewer(0, 28, new string[] { "HCN", "Name", "Head of House HCN" }, infoLines, ConsoleColor.White, ConsoleColor.Yellow, true),
                                new SubmitButton(0, WINDOW_HEIGHT - 5, "Search", ConsoleColor.White, ConsoleColor.Yellow, 's', true, false),
                                new SwitchPageButton(5, WINDOW_HEIGHT - 5, "Back", ConsoleColor.White, ConsoleColor.Yellow, null, 'b')
                            },

                            "Search For Patient",

                            new Label[]
                            {
                                new Label(15, 10, "Search by:", ConsoleColor.White),
                                new Label(0, 14, "OR", ConsoleColor.White, true),
                                new Label(0, 20, "AND", ConsoleColor.White, true)
                            }
                        );

                        Common.Common.currentMenu = SearchPatientMenu;
                    }
                }
                else if (Common.Common.currentMenu == AppointmentMenu)
                {
                    AppointmentMenu.Display();
                    AppointmentMenu.CheckInput();
                }
                else if (Common.Common.currentMenu == ScheduleMenu)
                {
                    ScheduleMenu.Display();
                    ScheduleMenu.CheckInput();

                    if (ScheduleMenu.isUpdateExit)
                    {
                        //TODO: Add buttons with information from the scheduling db
                    }
                }
                else if (Common.Common.currentMenu == SearchAppointmentMenu)
                {
                    SearchAppointmentMenu.Display();
                    SearchAppointmentMenu.CheckInput();

                    if (SearchAppointmentMenu.isSubmitExit)
                    {
                        string[] userData = SearchAppointmentMenu.GetUserInputs();

                        //TODO: use data to fill data viewer
                    }
                }
                else if (Common.Common.currentMenu == BillingMenu)
                {
                    BillingMenu.Display();
                    BillingMenu.CheckInput();
                }
            }
        }

        private void Exit()
        {
            Environment.Exit(0);
        }
    }
}
