﻿using System;
using Common;

namespace UI
{
    class Program
    {
        static void Main(string[] args)
        {
            UIManager ui = new UIManager();

            ui.RunUI();
        }

        //static void Main(string[] args)
        //{
        //    // Window size is number of coloumns and rows
        //    Console.SetWindowSize(100, 55);
        //    // Set buffer size the same as window size to get rid of scroll bars
        //    Console.SetBufferSize(100, 55);
        //    Console.CursorVisible = false;

        //    string[] title =
        //    {
        //        @"          _____                    _____                    _____          ",
        //        @"         /\    \                  /\    \                  /\    \         ",
        //        @"        /::\    \                /::\____\                /::\    \        ",
        //        @"       /::::\    \              /::::|   |               /::::\    \       ",
        //        @"      /::::::\    \            /:::::|   |              /::::::\    \      ",
        //        @"     /:::/\:::\    \          /::::::|   |             /:::/\:::\    \     ",
        //        @"    /:::/__\:::\    \        /:::/|::|   |            /:::/__\:::\    \    ",
        //        @"   /::::\   \:::\    \      /:::/ |::|   |            \:::\   \:::\    \   ",
        //        @"  /::::::\   \:::\    \    /:::/  |::|___|______    ___\:::\   \:::\    \  ",
        //        @" /:::/\:::\   \:::\    \  /:::/   |::::::::\    \  /\   \:::\   \:::\    \ ",
        //        @"/:::/__\:::\   \:::\____\/:::/    |:::::::::\____\/::\   \:::\   \:::\____\",
        //        @"\:::\   \:::\   \::/    /\::/    / ~~~~~/:::/    /\:::\   \:::\   \::/    /",
        //        @" \:::\   \:::\   \/____/  \/____/      /:::/    /  \:::\   \:::\   \/____/ ",
        //        @"  \:::\   \:::\    \                  /:::/    /    \:::\   \:::\    \     ",
        //        @"   \:::\   \:::\____\                /:::/    /      \:::\   \:::\____\    ",
        //        @"    \:::\   \::/    /               /:::/    /        \:::\  /:::/    /    ",
        //        @"     \:::\   \/____/               /:::/    /          \:::\/:::/    /     ",
        //        @"      \:::\    \                  /:::/    /            \::::::/    /      ",
        //        @"       \:::\____\                /:::/    /              \::::/    /       ",
        //        @"        \::/    /                \::/    /                \::/    /        ",
        //        @"         \/____/                  \/____/                  \/____/         ",
        //    };

        //    // items for the drop down menu
        //    string[] ddItems = { "One", "Two", "Three", "Four" };

        //    // menu items for the menu
        //    //MenuItem[] items =
        //    //{
        //    //    new Button(5, 25, "Hello", ConsoleColor.Black, ConsoleColor.White, ConsoleColor.Yellow, Test1, 'h'),
        //    //    new DropDown(5, 30, ddItems, "Select One", ConsoleColor.Black, ConsoleColor.White, ConsoleColor.Yellow),
        //    //    new TextBox(5, 35, 30, "Name", ConsoleColor.Black, ConsoleColor.White, ConsoleColor.Yellow)
        //    //};

        //    //TestMenu menu = new TestMenu(items, title);
        //    //menu.Display();
        //    //menu.CheckInput();

        //    string[] title2 =
        //    {
        //        "           ",
        //        "Add Patient",
        //        "           "
        //    };

        //    string[] sexes =
        //    {
        //        "Male",
        //        "Female",
        //        "Intersex",
        //        "Hermaphrodite"
        //    };

        //    MenuItem[] items =
        //    {
        //        //new TextBox(10, 8, 30, "HCN", ConsoleColor.White, ConsoleColor.Yellow),
        //        new TextBox(10, 12, 30, "First Name", AllLetters, "Cannot contain numbers!", ConsoleColor.White, ConsoleColor.Yellow),
        //        new TextBox(10, 16, 30, "Last Name", AllLetters, "Cannot contain numbers!",ConsoleColor.White, ConsoleColor.Yellow),
        //        new TextBox(10, 20, 1, "M. Initial", AllLetters, "Cannot contain numbers!",ConsoleColor.White, ConsoleColor.Yellow),
        //        //new TextBox(10, 24, 30, "D.O.B", ConsoleColor.White, ConsoleColor.Yellow),
        //        new DropDown(10, 28, sexes, "Sex", "---", ConsoleColor.White, ConsoleColor.Yellow)
        //        //new TextBox(10, 32, 30, "Head of House", ConsoleColor.White, ConsoleColor.Yellow),
        //        //new TextBox(10, 36, 30, "Address 1", ConsoleColor.White, ConsoleColor.Yellow),
        //        //new TextBox(10, 40, 30, "Address 2", ConsoleColor.White, ConsoleColor.Yellow),
        //        //new TextBox(10, 44, 30, "Province", ConsoleColor.White, ConsoleColor.Yellow),
        //        //new TextBox(10, 48, 30, "Phone #", ConsoleColor.White, ConsoleColor.Yellow)
        //    };

        //    TestMenu menu = new TestMenu(items, title);
        //    menu.Display();
        //    menu.CheckInput();

        //    Console.ReadLine();
        //}

        //// test function for click event of button
        //public static void Test1()
        //{
        //    Console.SetCursorPosition(0, 0);
        //    Console.Write("Button1");
        //}

        //// test validator for textbox
        //public static bool AllLetters(string text)
        //{
        //    foreach (char ch in text)
        //    {
        //        if (Char.IsDigit(ch))
        //        {
        //            return false;
        //        }
        //    }

        //    return true;
        //}
    }
}
