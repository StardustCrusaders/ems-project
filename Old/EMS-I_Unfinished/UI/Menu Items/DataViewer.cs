﻿using System;
using Common;
using System.Collections.Generic;

namespace UI
{
    /// <summary>
    /// This class is used to create a data viewer in the console.
    /// 
    /// Programmers:    William Bicknell, Jacob Funes, Firas Areibi, Moises Diaz<br/>
    /// Date:           2018-11-28
    /// </summary>
    public class DataViewer : MenuItem
    {
        private readonly string[] columnNames;
        private readonly string[][] rowData;
        private readonly List<int> columnWidths;
        private readonly int width;
        private int selectedIndex;

        public DataViewer(int x, int y, string[] columnNames, string[][] rowData, ConsoleColor textColour, ConsoleColor highlightColour, bool center=false) :
            base(x, y, textColour, highlightColour)
        {
            this.columnNames = columnNames;
            this.rowData = rowData;
            width = 0;

            columnWidths = new List<int>();
            for (int i = 0; i < columnNames.Length; i++)
            {
                columnWidths.Add(0);
            }

            // Calculate the width of the data viewer based on rows
            if (rowData != null)
            {
                for (int i = 0; i < rowData.Length; i++)
                {
                    List<int> widths = new List<int>();
                    foreach (string str in rowData[i])
                    {
                        widths.Add(str.Length);
                    }

                    for (int j = 0; j < columnWidths.Count; j++)
                    {
                        if (columnWidths[j] < widths[j])
                        {
                            columnWidths[j] = widths[j];
                        }
                    }
                }
            }

            // Calculate row width based on column names row
            List<int> nameWidths = new List<int>();
            foreach (string str in columnNames)
            {
                nameWidths.Add(str.Length);
            }

            for (int j = 0; j < columnWidths.Count; j++)
                {
                if (columnWidths[j] < nameWidths[j])
                {
                    columnWidths[j] = nameWidths[j];
                }
            }

            foreach (int colWidth in columnWidths)
            {
                width += colWidth;
            }

            width += columnNames.Length - 1;

            if (center)
            {
                pos.x = (Console.WindowWidth / 2) - width / 2;
            }
        }


        /// <summary>
        /// This functions displays the data viewer in the console.
        /// </summary>
        public override void Display()
        {
            Coordinates oldPos = new Coordinates(Console.CursorLeft, Console.CursorTop);
            ConsoleColor oldFg = Console.ForegroundColor;

            if (highlighted)
            {
                Console.ForegroundColor = hlColour;
            }
            else
            {
                Console.ForegroundColor = fgColour;
            }

            Console.SetCursorPosition(pos.x, pos.y);
            Console.Write("┌" + new string('─', width) + "┐");

            Console.SetCursorPosition(pos.x, Console.CursorTop + 1);
            Console.Write("│");
            for (int i = 0; i < columnNames.Length; i++)
            {
                Console.Write(FormatCellText(columnNames[i], i));
                Console.Write("│");
            }

            if (rowData != null)
            {
                for (int i = 0; i < rowData.Length; i++)
                {
                    Console.SetCursorPosition(pos.x - 1, Console.CursorTop + 1);

                    if (i == selectedIndex)
                    {
                        Console.Write(">");
                    }
                    else
                    {
                        Console.Write(" ");
                    }

                    Console.Write("│");
                    int j = 0;
                    foreach (string str in rowData[i])
                    {
                        Console.Write(FormatCellText(str, j));
                        Console.Write("│");
                        j++;
                    }
                }
            }

            Console.SetCursorPosition(pos.x, Console.CursorTop + 1);
            Console.Write("└" + new string('─', width) + "┘");

            Console.ForegroundColor = oldFg;
            Console.SetCursorPosition(oldPos.x, oldPos.y);
        }


        /// <summary>
        /// This function adds whitespace before & after text so that it fits into a cell.
        /// </summary>
        /// <param name="text">The text for the cell.</param>
        /// <param name="colIndex">The column that the text will appear in.</param>
        /// <returns>string : The formated text for the cell.</returns>
        private string FormatCellText(string text, int colIndex)
        {
            string cellText = new string(' ', columnWidths[colIndex] / 2 - text.Length / 2) + text;

            while (cellText.Length != columnWidths[colIndex])
            {
                cellText += " ";
            }

            return cellText;
        }


        /// <summary>
        /// This function lets the user select an item from the data viewer.
        /// </summary>
        public override void Click()
        {
            Unselect();
            ChangeSelection(0, selectedIndex);

            int prevSelectedIndex = 0;
            ConsoleKeyInfo info;
            while (true)
            {
                info = Console.ReadKey(true);
                prevSelectedIndex = selectedIndex;

                if (info.Key == ConsoleKey.Enter)
                {
                    break;
                }
                else if (info.Key == ConsoleKey.UpArrow)
                {
                    selectedIndex--;

                    if (selectedIndex < 0)
                    {
                        selectedIndex = rowData.Length - 1;
                    }
                }
                else if (info.Key == ConsoleKey.DownArrow)
                {
                    selectedIndex++;

                    if (selectedIndex >= rowData.Length)
                    {
                        selectedIndex = 0;
                    }
                }

                ChangeSelection(prevSelectedIndex, selectedIndex);
            }

            Select();
        }


        /// <summary>
        /// This function changes the visual selection in the data viewer.
        /// </summary>
        /// <param name="prevSelected">The last selected item to unselect.</param>
        /// <param name="newSelected">The new item to select.</param>
        private void ChangeSelection(int prevSelected, int newSelected)
        {
            Console.ForegroundColor = fgColour;
            Console.SetCursorPosition(pos.x - 1, pos.y + 2 + prevSelected);
            Console.Write(" │");
            for (int i = 0; i < rowData[prevSelected].Length; i++)
            {
                Console.Write(FormatCellText(rowData[prevSelected][i], i));
                Console.Write("│");
            }

            Console.ForegroundColor = hlColour;
            Console.SetCursorPosition(pos.x - 1, pos.y + 2 + newSelected);
            Console.Write(">│");
            for (int i = 0; i < rowData[newSelected].Length; i++)
            {
                Console.Write(FormatCellText(rowData[newSelected][i], i));
                Console.Write("│");
            }
        }


        /// <summary>
        /// This function returns the selected value of the data viewer.
        /// </summary>
        /// <returns>string : The selected value of the data viewer.</returns>
        public override string GetValue()
        {
            string value = "";

            if (rowData != null)
            {
                foreach (string str in rowData[selectedIndex])
                {
                    value += str + "│";
                }

                value = value.Substring(0, value.Length - 1);
            }

            return value;
        }
    }
}
