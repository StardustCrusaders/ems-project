﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace UI
{
    /// <summary>
    /// This class used to create a calendar in the console.
    /// 
    /// Programmers:    William Bicknell, Jacob Funes, Firas Areibi, Moises Diaz<br/>
    /// Date:           2018-11-16
    /// </summary>
    public class Calendar : MenuItem
    {
        private const int WEEK_LENGTH = 7;

        private readonly int[] startDay;
        private readonly int[] numDays;
        private readonly int[][] fullDays;
        private readonly string[] monthNames;
        private readonly ConsoleColor unavailableColour;
        private int currentMonth = 0;
        private int selectedDay = 0;
        private int width;


        public Calendar(int x, int y, int[] startDay, int[] numDays, string[] monthNames, int[][] fullDays, ConsoleColor textColour, ConsoleColor highlightColour, ConsoleColor unavailableColour, bool center=false) :
            base(x, y, textColour, highlightColour)
        {
            this.startDay = startDay;
            this.numDays = numDays;
            this.monthNames = monthNames;
            this.fullDays = fullDays;
            this.unavailableColour = unavailableColour;
            width = 1 + WEEK_LENGTH * 4;

            for (int i = 1; i < numDays[currentMonth]; i++)
            {
                if (!fullDays[currentMonth].Contains(i))
                {
                    selectedDay = i;
                    break;
                }
            }

            if (center)
            {
                pos.x = (Console.WindowWidth / 2) - (width / 2);
            }
        }


        /// <summary>
        /// This function displays the calendar in the console.
        /// </summary>
        public override void Display()
        {
            Coordinates oldPos = new Coordinates(Console.CursorLeft, Console.CursorTop);
            ConsoleColor oldFg = Console.ForegroundColor;

            if (highlighted)
            {
                Console.ForegroundColor = hlColour;
            }
            else
            {
                Console.ForegroundColor = fgColour;
            }

            Console.SetCursorPosition(pos.x, pos.y);
            Console.Write(new string(' ', width / 2 - monthNames[currentMonth].Length / 2) + monthNames[currentMonth]);
            Console.SetCursorPosition(pos.x, Console.CursorTop + 1);
            Console.Write("┌───┬───┬───┬───┬───┬───┬───┐");
            Console.SetCursorPosition(pos.x, Console.CursorTop + 1);
            Console.Write("│Sat│Mon│Tue│Wed│Thu│Fri│Sun│");
            Console.SetCursorPosition(pos.x, Console.CursorTop + 1);
            Console.Write("├───┼───┼───┼───┼───┼───┼───┤");
            Console.SetCursorPosition(pos.x, Console.CursorTop + 1);

            Console.Write("│");
            bool firstDayReached = false;
            int numDaysPlaced = 0;
            for (int i = 1; i < numDays[currentMonth] + 1; i++)
            {
                if (!firstDayReached)
                {
                    if (numDaysPlaced == startDay[currentMonth] - 1)
                    {
                        if (fullDays[currentMonth].Contains(1))
                        {
                            Console.ForegroundColor = unavailableColour;
                        }

                        Console.Write(" 1 ");

                        if (highlighted)
                        {
                            Console.ForegroundColor = hlColour;
                        }
                        else
                        {
                            Console.ForegroundColor = fgColour;
                        }

                        Console.Write("│");
                        firstDayReached = true;
                    }
                    else
                    {
                        Console.Write("   │");
                        i--;
                    }

                    numDaysPlaced++;
                }
                else
                {
                    if (numDaysPlaced == WEEK_LENGTH)
                    {
                        Console.SetCursorPosition(pos.x, Console.CursorTop + 1);
                        Console.Write("├───┼───┼───┼───┼───┼───┼───┤");
                        Console.SetCursorPosition(pos.x, Console.CursorTop + 1);
                        Console.Write("│");

                        numDaysPlaced = 0;
                    }

                    if (fullDays[currentMonth].Contains(i))
                    {
                        Console.ForegroundColor = unavailableColour;
                    }

                    if (i < 10)
                    {
                        Console.Write(" " + i + " ");
                    }
                    else
                    {
                        Console.Write(" " + i);
                    }

                    if (highlighted)
                    {
                        Console.ForegroundColor = hlColour;
                    }
                    else
                    {
                        Console.ForegroundColor = fgColour;
                    }

                    Console.Write("│");

                    numDaysPlaced++;
                }
            }

            if (numDaysPlaced != 0)
            {
                for (int i = numDaysPlaced; i < WEEK_LENGTH; i++)
                {
                    Console.Write("   │");
                }
            }

            Console.SetCursorPosition(pos.x, Console.CursorTop + 1);
            Console.Write("└───┴───┴───┴───┴───┴───┴───┘");

            Console.ForegroundColor = oldFg;
            Console.SetCursorPosition(oldPos.x, oldPos.y);
        }


        public override string GetValue()
        {
            //TODO: The selected day
            return null;
        }


        /// <summary>
        /// This function allows the user to select a day and then select a timeslot to book.
        /// </summary>
        public override void Click()
        {
            Unselect();

            SelectDay();

            Select();
        }


        private void SelectDay()
        {
            MoveCursorToSelectedDay();

            Console.ForegroundColor = hlColour;
            Console.Write(selectedDay);

            bool moveRight = true;
            ConsoleKeyInfo info;
            while (true)
            {
                info = Console.ReadKey(true);

                if (selectedDay < 10)
                {
                    Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
                }
                else
                {
                    Console.SetCursorPosition(Console.CursorLeft - 2, Console.CursorTop);
                }

                Console.ForegroundColor = fgColour;
                Console.Write(selectedDay);


                if (info.Key == ConsoleKey.Enter)
                {
                    break;
                }
                else if (info.Modifiers == ConsoleModifiers.Control && info.Key == ConsoleKey.RightArrow)
                {
                    currentMonth++;
                    selectedDay = 1;
                    moveRight = true;

                    if (currentMonth >= monthNames.Length)
                    {
                        currentMonth = 0;
                    }

                    Display();
                }
                else if (info.Modifiers == ConsoleModifiers.Control && info.Key == ConsoleKey.LeftArrow)
                {
                    currentMonth--;
                    selectedDay = 1;
                    moveRight = false;

                    if (currentMonth < 0)
                    {
                        currentMonth = monthNames.Length - 1;
                    }

                    Display();
                }
                else if (info.Key == ConsoleKey.UpArrow)
                {
                    selectedDay -= WEEK_LENGTH;
                    moveRight = false;

                    if (selectedDay <= 0)
                    {
                        selectedDay += WEEK_LENGTH;
                        selectedDay = selectedDay + WEEK_LENGTH * (numDays[currentMonth] / WEEK_LENGTH);

                        if (selectedDay > numDays[currentMonth])
                        {
                            selectedDay -= WEEK_LENGTH;
                        }
                    }
                }
                else if (info.Key == ConsoleKey.DownArrow)
                {
                    selectedDay += WEEK_LENGTH;
                    moveRight = true;

                    if (selectedDay > numDays[currentMonth])
                    {
                        selectedDay -= WEEK_LENGTH;
                        selectedDay = selectedDay - WEEK_LENGTH * (numDays[currentMonth] / WEEK_LENGTH);

                        if (selectedDay <= 0)
                        {
                            selectedDay += WEEK_LENGTH;
                        }
                    }
                }
                else if (info.Key == ConsoleKey.LeftArrow)
                {
                    selectedDay--;

                    if (selectedDay <= 0)
                    {
                        selectedDay = numDays[currentMonth];
                    }
                }
                else if (info.Key == ConsoleKey.RightArrow)
                {
                    selectedDay++;

                    if (selectedDay > numDays[currentMonth])
                    {
                        selectedDay = 1;
                    }
                }

                while (fullDays[currentMonth].Contains(selectedDay))
                {
                    if (moveRight)
                    {
                        selectedDay++;
                    }
                    else
                    {
                        selectedDay--;
                    }
                }

                MoveCursorToSelectedDay();

                Console.ForegroundColor = hlColour;
                Console.Write(selectedDay);
            }
        }


        private void MoveCursorToSelectedDay()
        {
            int week = 0;
            int dayOfWeek = (selectedDay + startDay[currentMonth] - 2);
            if (dayOfWeek >= WEEK_LENGTH)
            {
                week = (selectedDay + startDay[currentMonth] - 2) / WEEK_LENGTH;
                dayOfWeek = (selectedDay + startDay[currentMonth] - 2) % WEEK_LENGTH;

            }

            Console.SetCursorPosition(pos.x + (4 * dayOfWeek) + 2, pos.y + 4 + week * 2);
        }
    }
}
