﻿using System;
using Common;

namespace UI
{
    public class Label : MenuItem
    {
        private string text;

        public Label(int x, int y, string text, ConsoleColor textColour, bool center = false) :
            base(x, y, textColour, ConsoleColor.Yellow)
        {
            this.text = text;

            if (center)
            {
                pos.x = (Console.WindowWidth / 2) - (text.Length / 2) - 1;
            }
        }

        public override void Click()
        {
            return;
        }

        public override void Display()
        {
            Coordinates oldPos = new Coordinates(Console.CursorLeft, Console.CursorTop);
            ConsoleColor oldFg = Console.ForegroundColor;

            Console.ForegroundColor = fgColour;

            Console.SetCursorPosition(pos.x, pos.y);
            Console.Write(text);

            Console.ForegroundColor = oldFg;
            Console.SetCursorPosition(oldPos.x, oldPos.y);
        }

        public override string GetValue()
        {
            return null;
        }
    }
}
