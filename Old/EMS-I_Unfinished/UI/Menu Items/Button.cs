using System;
using Common;

namespace UI
{
    /// <summary>
    /// This class is used to create a button in the console.
    /// 
    /// Programmers:    William Bicknell, Jacob Funes, Firas Areibi, Moises Diaz<br/>
    /// Date:           2018-11-08
    /// </summary>
    public class Button : MenuItem
    {
        private readonly string text;
        private readonly int width;
        protected OnClick clickFunction;
        private ConsoleColor hotKeyColour = ConsoleColor.Cyan;

        public Button(int x, int y, string text, ConsoleColor textColour, ConsoleColor highlightColour, OnClick clickFunction, char hotKey='\0', bool center=false) : 
            base(x, y, textColour, highlightColour, hotKey)
        {
            this.text = text;

            width = text.Length + 2;
            this.clickFunction = clickFunction;

            if (center)
            {
                pos.x = (Console.WindowWidth / 2) - (width / 2) - 1;
            }
        }


        /// <summary>
        /// This function displays the button in the console.
        /// </summary>
        public override void Display()
        {
            Coordinates oldPos = new Coordinates(Console.CursorLeft, Console.CursorTop);
            ConsoleColor oldFg = Console.ForegroundColor;

            if (highlighted)
            {
                Console.ForegroundColor = hlColour;
            }
            else
            {
                Console.ForegroundColor = fgColour;
            }
            
            Console.SetCursorPosition(pos.x, pos.y);
            Console.Write("┌" + new string('─', width) + "┐");
            Console.SetCursorPosition(pos.x, pos.y + 1);
            Console.Write("│ " + text + " │");
            Console.SetCursorPosition(pos.x, pos.y + 2);
            Console.Write("└" + new string('─', width) + "┘");

            Console.ForegroundColor = oldFg;
            Console.SetCursorPosition(oldPos.x, oldPos.y);
        }


        /// <summary>
        /// This function returns null because the button does not have a value.
        /// </summary>
        /// <returns>null</returns>
        public override string GetValue()
        {
            return null;
        }


        public override void Click()
        {
            if (clickFunction != null)
            {
                clickFunction();
            }
        }
    }
}
