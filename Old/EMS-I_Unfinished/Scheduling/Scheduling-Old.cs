using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace Scheduling
{
    /// <summary>
    /// This class is gonna deal with every single part of the schedulling and checking for error codes.<br/>
    /// 
    /// Programmers: William Bicknell, Jacob Funes, Firas Areibi, Moises Diaz<br/>
    /// Date: 2018-11-16
    /// </summary>
   
    class Program
    {
        static void Main(string[] args)
        {

        }

        /// <summary>
        /// This function will check the availability of an appointment for a user.
        /// For example, if a user wants to book an appointment at 2PM on Thursday,
        /// but another user already booked the same time slot for that appointment.
        /// If the user booked the appointment already, this function will displays
        /// error messages to the user.
        /// </summary>
        /// <param name="schedulingInfo">This parameter is holding the whole sheduling info from the user </param>
        /// <returns>true: if it is succesful and everything is fine</returns>

        private static bool AvailabilityOfAppointment (SchedulingInfo schedulingInfo)
        {
            return true;
        }

        /// <summary>
        /// This function will deals with the sheduling and checking if the appointments per day were already taken.
        /// Also, this function will displays error messages to the user. Only if all the appointments (6) per day
        /// were already booked by other users.
        /// </summary>
        /// <param name="day"> This is the day (in general) of the appointments</param>
        /// <param name="month"> This is the month (in general) of the appointments</param>
        /// <param name="year"></param>
        /// <returns> 0: This is because the function returns an integer value</returns>

        public static int FullAppointmentShedulePerDay (int day, int month, int year)
        {

            return 0;
        }

        /// <summary>
        /// This function will displays error messages if the information fails to save inside the database.
        /// </summary>
        /// <param name="schedulingInfo">This parameter holds the whole information about appoinment and I need it to check if everything is ok</param>
        /// <returns>false: It means that it was a problem saving the info inside the database </returns>

        public static bool MajorProblemSavingInfoInDataBase (SchedulingInfo schedulingInfo)
        {
            return false;
        }

        /// <summary>
        /// This function will check if an appointment was booked a month before the actual time.
        /// It will displays error message to the user because it is impossible to have an appointment backwards.
        /// </summary>
        /// <param name="day"></param>
        /// <param name="month"></param>
        /// <param name="year"></param>
        /// <returns> true: It means it was an error and the message was displayed correctly. </returns>

        public static bool AppointmentBackawards (int day, int month, int year)
        {
            return true;
        }

        /// <summary>
        /// This function will check if the appointment was booked for a valid DATE and not for a invalid date.
        /// </summary>
        /// <param name="day"> I need the day of the appointment to check if it has a valid date</param>
        /// <param name="month">As I need the day, I need the month of the appointment to check if the month is valid or according of our requirements</param>
        /// <param name="year">I need the year too to check if the year is valid. Like the user should not book an appointment to 2019</param>
        /// <returns>0: It means an error happened while checking for the DATE of an appointment</returns>

        public static int AppointmentOnInvalidDate (int day, int month, int year)
        {
            return 0;
        }


        /// <summary>
        /// This function will deals with the feature of removing an appointment.
        /// It will check if an appointment exists and the user wants to remove it after was already taken.
        /// Then, thi function will remove the slot that was taken by the user.
        /// </summary>
        /// <param name="schedulingInfo"> I need the whole info in order to remove the appointment because it is necessary</param>
        /// <returns>true: </returns>

        public static bool RemoveAppointment (SchedulingInfo schedulingInfo)
        {
            return true;
        }
    }
}