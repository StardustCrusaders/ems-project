/*
*   Common items used between classes
*/

namespace uiTest
{
    public delegate void OnClick();

    public struct Coordinates
    {
        public Coordinates(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public int x;
        public int y;
    }
}
