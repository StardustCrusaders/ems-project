﻿using System;

namespace uiTest
{
    class Program
    {
        static void Main(string[] args)
        {
            // Window size is number of coloumns and rows
            Console.SetWindowSize(100, 50);
            // Set buffer size the same as window size to get rid of scroll bars
            Console.SetBufferSize(100, 50);
            Console.CursorVisible = false;

            string[] title =
            {
                @"          _____                    _____                    _____          ",
                @"         /\    \                  /\    \                  /\    \         ",
                @"        /::\    \                /::\____\                /::\    \        ",
                @"       /::::\    \              /::::|   |               /::::\    \       ",
                @"      /::::::\    \            /:::::|   |              /::::::\    \      ",
                @"     /:::/\:::\    \          /::::::|   |             /:::/\:::\    \     ",
                @"    /:::/__\:::\    \        /:::/|::|   |            /:::/__\:::\    \    ",
                @"   /::::\   \:::\    \      /:::/ |::|   |            \:::\   \:::\    \   ",
                @"  /::::::\   \:::\    \    /:::/  |::|___|______    ___\:::\   \:::\    \  ",
                @" /:::/\:::\   \:::\    \  /:::/   |::::::::\    \  /\   \:::\   \:::\    \ ",
                @"/:::/__\:::\   \:::\____\/:::/    |:::::::::\____\/::\   \:::\   \:::\____\",
                @"\:::\   \:::\   \::/    /\::/    / ~~~~~/:::/    /\:::\   \:::\   \::/    /",
                @" \:::\   \:::\   \/____/  \/____/      /:::/    /  \:::\   \:::\   \/____/ ",
                @"  \:::\   \:::\    \                  /:::/    /    \:::\   \:::\    \     ",
                @"   \:::\   \:::\____\                /:::/    /      \:::\   \:::\____\    ",
                @"    \:::\   \::/    /               /:::/    /        \:::\  /:::/    /    ",
                @"     \:::\   \/____/               /:::/    /          \:::\/:::/    /     ",
                @"      \:::\    \                  /:::/    /            \::::::/    /      ",
                @"       \:::\____\                /:::/    /              \::::/    /       ",
                @"        \::/    /                \::/    /                \::/    /        ",
                @"         \/____/                  \/____/                  \/____/         ",
            };

            // items for the drop down menu
            string[] ddItems = { "One", "Two", "Three", "Four" };

            // menu items for the menu
            MenuItem[] items =
            {
                new Button(5, 25, "Hello", ConsoleColor.Black, ConsoleColor.White, ConsoleColor.Yellow, Test1, 'h'),
                new DropDown(5, 30, ddItems, "Select One", ConsoleColor.Black, ConsoleColor.White, ConsoleColor.Yellow),
                new TextBox(5, 35, 30, "Name", ConsoleColor.Black, ConsoleColor.White, ConsoleColor.Yellow)
            };

            TestMenu menu = new TestMenu(items, title);
            menu.Display();
            menu.CheckInput();
            Console.ReadLine();
        }

        // test function for click event of button
        public static void Test1()
        {
            Console.SetCursorPosition(0, 0);
            Console.Write("Button1");
        }
    }
}
