﻿using System;

namespace uiTest
{
    public class TestMenu : BaseMenu
    {
        public TestMenu(MenuItem[] menuItems, string[] title) : base(menuItems, title)
        {
            
        }

        // overridden CheckInput should contain the input loop
        public override void CheckInput()
        {
            while (true)
            {
                lastKeyPressInfo = Console.ReadKey(true);

                // check base inputs in BaseMenu
                base.CheckInput();

                int i = 0;
                foreach (MenuItem item in items)
                {
                    if (item.GetHotKey() == lastKeyPressInfo.KeyChar)
                    {
                        items[selectedIndex].Unselect();
                        item.Select();
                        selectedIndex = i;

                        item.Click();

                        if (items[selectedIndex] is DropDown)
                        {
                            Display();
                        }
                    }

                    i++;
                }
            }
        }
    }
}
