﻿using System;

namespace uiTest
{
    public abstract class BaseMenu
    {
        protected readonly MenuItem[] items;
        protected readonly string[] title;
        protected int selectedIndex = 0;
        protected ConsoleKeyInfo lastKeyPressInfo = new ConsoleKeyInfo();

        public BaseMenu(MenuItem[] menuItems, string[] title)
        {
            items = menuItems;
            this.title = title;
        }

        public void Display()
        {
            Console.Clear();

            Console.SetCursorPosition(0, 0);

            int leftBuffer = (Console.WindowWidth / 2) - (title[0].Length / 2);

            // Draw title
            foreach (string line in title)
            {
                Console.WriteLine(new string(' ', leftBuffer) + line);
            }

            // Draw separator
            Console.WriteLine(new string('─', Console.WindowWidth));

            // Draw MenuItems
            foreach (MenuItem item in items)
            {
                item.Display();
            }

            items[selectedIndex].Select();
        }

        // This function will have basic input detection and should be called within the loop of the child function
        // See TestMenu for an example
        // This function handles selecting a MenuItem and moving between MenuItems
        public virtual void CheckInput()
        {
            switch (lastKeyPressInfo.Key)
            {
                case ConsoleKey.Enter:
                    items[selectedIndex].Click();

                    if (items[selectedIndex] is DropDown)
                    {
                        Display();
                    }

                    break;

                case ConsoleKey.UpArrow:
                    items[selectedIndex].Unselect();
                    selectedIndex--;

                    if (selectedIndex < 0)
                    {
                        selectedIndex = items.Length - 1;
                    }

                    items[selectedIndex].Select();

                    break;

                case ConsoleKey.DownArrow:
                    items[selectedIndex].Unselect();
                    selectedIndex++;

                    if (selectedIndex >= items.Length)
                    {
                        selectedIndex = 0;
                    }

                    items[selectedIndex].Select();

                    break;
            }
        }

        // Gets the user inputed values from all MenuItems
        // i.e. TextBox returns entered text, DropDown returns selected item, Button returns null
        public string[] GetUserInputs()
        {
            string[] userInputs = new string[items.Length];

            int i = 0;
            foreach (MenuItem item in items)
            {
                string input = item.GetValue();

                if (input != null)
                {
                    userInputs[i] = input;
                    i++;
                }
            }

            return userInputs;
        }
    }
}
