﻿using System;

namespace uiTest
{
    public class Button : MenuItem
    {
        private readonly string text;
        private readonly int width;

        public Button(int x, int y, string text, ConsoleColor backgroundColour, ConsoleColor textColour, ConsoleColor highlightColour, OnClick clickFunction, char hotKey = '\0') : 
            base(x, y, backgroundColour, textColour, highlightColour, hotKey, clickFunction)
        {
            this.text = text;

            width = text.Length + 2;
        }

        public override void Display()
        {
            Coordinates oldPos = new Coordinates(Console.CursorLeft, Console.CursorTop);
            ConsoleColor oldFg = Console.ForegroundColor;

            if (highlighted)
            {
                Console.ForegroundColor = hlColour;
            }
            else
            {
                Console.ForegroundColor = fgColour;
            }
            
            Console.SetCursorPosition(pos.x, pos.y);
            Console.Write("┌" + new string('─', width) + "┐");
            Console.SetCursorPosition(pos.x, pos.y + 1);
            Console.Write("│ " + text + " │");
            Console.SetCursorPosition(pos.x, pos.y + 2);
            Console.Write("└" + new string('─', width) + "┘");

            Console.ForegroundColor = oldFg;
            Console.SetCursorPosition(oldPos.x, oldPos.y);
        }

        public override string GetValue()
        {
            return null;
        }
    }
}
