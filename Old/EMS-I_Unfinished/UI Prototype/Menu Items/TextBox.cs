﻿using System;

namespace uiTest
{
    public class TextBox : MenuItem
    {
        private readonly string labelText;
        private readonly int maxInputChars;
        private string text = "";

        public TextBox(int x, int y, int maxInputChars, string labelText, ConsoleColor backgroundColour, ConsoleColor textColour, ConsoleColor highlightColour) :
            base(x, y, backgroundColour, textColour, highlightColour)
        {
            this.labelText = labelText;
            this.maxInputChars = maxInputChars;
        }

        public override void Display()
        {
            Coordinates oldPos = new Coordinates(Console.CursorLeft, Console.CursorTop);
            ConsoleColor oldFg = Console.ForegroundColor;

            if (highlighted)
            {
                Console.ForegroundColor = hlColour;
            }
            else
            {
                Console.ForegroundColor = fgColour;
            }

            Console.SetCursorPosition(pos.x, pos.y);
            Console.Write("┌─" + labelText + new string('─', maxInputChars - labelText.Length - 1) + "┐");
            Console.SetCursorPosition(pos.x, pos.y + 1);
            Console.Write("│" + text + new string(' ', maxInputChars - text.Length) + "│");
            Console.SetCursorPosition(pos.x, pos.y + 2);
            Console.Write("└" + new string('─', maxInputChars) + "┘");
        }

        public override string GetValue()
        {
            return text;
        }

        public override void Click()
        {
            Unselect();
            Console.CursorVisible = true;
            Console.SetCursorPosition(pos.x + 1 + text.Length, pos.y + 1);

            ConsoleKeyInfo info;
            while (true)
            {
                info = Console.ReadKey(true);

                if (info.Key == ConsoleKey.Enter)
                {
                    break;
                }
                else if (info.Key == ConsoleKey.Backspace)
                {
                    if (Console.CursorLeft > pos.x + 1)
                    {
                        text = text.Remove(text.Length - 1);
                        Console.CursorLeft--;
                        Console.Write(' ');
                        Console.CursorLeft--;
                    }
                }
                else if (info.KeyChar >= ' ' && info.KeyChar <= '~' && text.Length < maxInputChars)
                {
                    text += info.KeyChar;
                    Console.Write(info.KeyChar);
                }
            }

            Console.CursorVisible = false;
            Select();
        }
    }
}
