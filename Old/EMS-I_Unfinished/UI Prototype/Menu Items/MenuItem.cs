﻿using System;

namespace uiTest
{
    public abstract class MenuItem
    {
        protected Coordinates pos;
        protected readonly ConsoleColor bgColour;
        protected readonly ConsoleColor fgColour;
        protected readonly ConsoleColor hlColour;
        protected bool highlighted = false;
        protected OnClick clickFunction;
        protected char hotKey;
        

        public MenuItem(int x, int y, ConsoleColor backgroundColour, ConsoleColor textColour, ConsoleColor highlightColour, char hotKey = '\a', OnClick clickFunction = null)
        {
            pos = new Coordinates(x, y);
            bgColour = backgroundColour;
            fgColour = textColour;
            hlColour = highlightColour;
            this.clickFunction = clickFunction;
            this.hotKey = hotKey;
        }

        public abstract void Display();
        public abstract string GetValue();

        public char GetHotKey()
        {
            return hotKey;
        }

        public void Select()
        {
            highlighted = true;
            Display();
        }

        public void Unselect()
        {
            highlighted = false;
            Display();
        }

        public virtual void Click()
        {
            clickFunction();
        }
    }
}
