﻿using System;

namespace uiTest
{
    public class DropDown : MenuItem
    {
        private readonly string[] items;
        private readonly string defaultText;
        private readonly int width = 2;

        private string text;
        private int selectedIndex = 0;

        public DropDown(int x, int y, string[] items, string defaultText, ConsoleColor backgroundColour, ConsoleColor textColour, ConsoleColor highlightColour) : 
            base(x, y, backgroundColour, textColour, highlightColour)
        {
            this.items = items;
            this.defaultText = defaultText;
            text = defaultText;

            // find longest string in items
            foreach (string item in items)
            {
                if (item.Length + 2 > width)
                {
                    width = item.Length + 2;
                }
            }

            // if still shorter than default use default width
            if (width < defaultText.Length + 4)
            {
                width = defaultText.Length + 4;
            }
        }

        public override void Display()
        {
            // Save current cursor pos and colours (may not be needed but unsure if colours will affect Console.Clear())
            Coordinates oldPos = new Coordinates(Console.CursorLeft, Console.CursorTop);
            ConsoleColor oldBg = Console.BackgroundColor;
            ConsoleColor oldFg = Console.ForegroundColor;

            Console.SetCursorPosition(pos.x, pos.y);
            Console.BackgroundColor = bgColour;

            if (highlighted)
            {
                Console.ForegroundColor = hlColour;
            }
            else
            {
                Console.ForegroundColor = fgColour;
            }

            int leftPadding = (width - text.Length - 2) / 2;
            int rightPadding = width - text.Length - 2 - leftPadding;

            Console.Write("┌" + new string('─', width) + "┐");
            Console.SetCursorPosition(pos.x, pos.y + 1);
            Console.Write("│" + new string(' ', leftPadding) + text + " v" + new string(' ', rightPadding) + "│");
            Console.SetCursorPosition(pos.x, pos.y + 2);
            Console.Write("└" + new string('─', width) + "┘");

            Console.BackgroundColor = oldBg;
            Console.ForegroundColor = oldFg;
            Console.SetCursorPosition(oldPos.x, oldPos.y);
        }

        public void GetSelection()
        {
            int prevSelectedIndex = 0;
            ConsoleColor oldBg = Console.BackgroundColor;
            ConsoleColor oldFg = Console.ForegroundColor;

            Extend();

            ConsoleKeyInfo info;
            while (true)
            {
                info = Console.ReadKey(true);
                prevSelectedIndex = selectedIndex;

                if (info.Key == ConsoleKey.Enter) // finish selection
                {
                    break;
                }
                else if (info.Key == ConsoleKey.UpArrow) // move up in list
                {
                    selectedIndex--;

                    if (selectedIndex < 0)
                    {
                        selectedIndex = items.Length - 1;
                    }
                }
                else if (info.Key == ConsoleKey.DownArrow) // move down in list
                {
                    selectedIndex++;

                    if (selectedIndex >= items.Length)
                    {
                        selectedIndex = 0;
                    }
                }

                ChangeSelection(prevSelectedIndex, selectedIndex);
            }

            Console.BackgroundColor = oldBg;
            Console.ForegroundColor = oldFg;
            text = items[selectedIndex];
        }

        public override string GetValue()
        {
            return items[selectedIndex];
        }

        public override void Click()
        {
            GetSelection();
        }

        private void ChangeSelection(int originalIndex, int newIndex)
        {
            Coordinates oldPos = new Coordinates(Console.CursorLeft, Console.CursorTop);

            // unhighlight the last selection
            Console.SetCursorPosition(pos.x, pos.y + 2 + originalIndex);
            Console.ForegroundColor = fgColour;
            Console.BackgroundColor = bgColour;
            int leftPadding = (width - items[originalIndex].Length) / 2;
            int rightPadding = width - items[originalIndex].Length - leftPadding;

            Console.Write("│" + new string(' ', leftPadding) + items[originalIndex] + new string(' ', rightPadding) + "│");

            // highlight the new selection
            Console.SetCursorPosition(pos.x, pos.y + 2 + newIndex);
            Console.ForegroundColor = hlColour;
            leftPadding = (width - items[newIndex].Length) / 2;
            rightPadding = width - items[newIndex].Length - leftPadding;

            Console.Write("│" + new string(' ', leftPadding) + items[newIndex] + new string(' ', rightPadding) + "│");

            Console.SetCursorPosition(oldPos.x, oldPos.y);
        }

        private void Extend()
        {
            // changes top text to default text and unhighlights it
            text = defaultText;
            Unselect();

            Coordinates oldPos = new Coordinates(Console.CursorLeft, Console.CursorTop);

            Console.SetCursorPosition(pos.x, pos.y + 2);
            Console.ForegroundColor = fgColour;
            Console.BackgroundColor = bgColour;

            // draw dropdown items
            int i = 0;
            foreach (string item in items)
            {
                if (i == selectedIndex)
                {
                    Console.ForegroundColor = hlColour;
                }
                else
                {
                    Console.ForegroundColor = fgColour;
                }

                int leftPadding = (width - item.Length) / 2;
                int rightPadding = width - item.Length - leftPadding;

                Console.Write("│" + new string(' ', leftPadding) + item + new string(' ', rightPadding) + "│");

                i++;
                Console.SetCursorPosition(pos.x, pos.y + 2 + i);
            }

            Console.Write("└" + new string('─', width) + "┘");

            Console.SetCursorPosition(oldPos.x, oldPos.y);
        }
    }
}
