﻿using System;
using Common;

namespace UI
{
    /// <summary>
    /// This class is used to create a submit button in the console.
    /// 
    /// Programmers:    William Bicknell, Jacob Funes, Firas Areibi, Moises Diaz<br/>
    /// Date:           2018-11-20
    /// </summary>
    public class SubmitButton : Button
    {
        private readonly bool switchMenu;

        public SubmitButton(int x, int y, string text, ConsoleColor textColour, ConsoleColor highlightColour, char hotKey = '\0', bool center=false, bool switchMenu=true) :
            base(x, y, text, textColour, highlightColour, null, hotKey, center)
        {
            this.switchMenu = switchMenu;
        }

        public override void Click()
        {
            if (switchMenu)
            {
                Common.Common.currentMenu = Common.Common.lastMenu.Pop();

                Unselect();
            }
        }
    }
}
