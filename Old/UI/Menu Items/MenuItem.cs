﻿using System;
﻿using Common;

namespace UI
{
    /// <summary>
    /// This class is used as a template for all menu items used in the console.
    /// 
    /// Programmers:    William Bicknell, Jacob Funes, Firas Areibi, Moises Diaz<br/>
    /// Date:           2018-11-08
    /// </summary>
    public abstract class MenuItem
    {
        protected Coordinates pos;
        protected readonly ConsoleColor fgColour;
        protected readonly ConsoleColor hlColour;
        protected bool highlighted = false;
        protected char hotKey;
        

        public MenuItem(int x, int y, ConsoleColor textColour, ConsoleColor highlightColour, char hotKey = '\a')
        {
            pos = new Coordinates(x, y);
            fgColour = textColour;
            hlColour = highlightColour;
            this.hotKey = hotKey;
        }


        /// <summary>
        /// This function displays the menu item.
        /// </summary>
        public abstract void Display();


        /// <summary>
        /// This function returns the value of the menu item.
        /// </summary>
        /// <returns>string : The value of the menu item.</returns>
        public abstract string GetValue();


        /// <summary>
        /// This function returns the hotkey attached to the menu item.
        /// </summary>
        /// <returns>char : The hotkey attached to the menu item.</returns>
        public char GetHotKey()
        {
            return Char.ToLower(hotKey);
        }


        /// <summary>
        /// This function selects the menu item.
        /// </summary>
        public void Select()
        {
            highlighted = true;
            Display();
        }


        /// <summary>
        /// This function unselects the menu item.
        /// </summary>
        public void Unselect()
        {
            highlighted = false;
            Display();
        }


        /// <summary>
        /// This function calls the click function attached to the menu item.
        /// </summary>
        public abstract void Click();


        /// <summary>
        /// This function calls the validation function attached to the menu item.
        /// </summary>
        public virtual bool Validate()
        {
            return true;
        }
    }
}
