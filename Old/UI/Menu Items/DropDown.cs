using System;
using Common;

namespace UI
{
    /// <summary>
    /// This class is used to create a drop-down menu in the console.
    /// 
    /// Programmers:    William Bicknell, Jacob Funes, Firas Areibi, Moises Diaz<br/>
    /// Date:           2018-11-08
    /// </summary>
    public class DropDown : MenuItem
    {
        private readonly string[] items;
        private readonly string defaultText;
        private readonly string title;
        private readonly int width = 2;

        private string text;
        private int selectedIndex = 0;

        public DropDown(int x, int y, string[] items, string title, string defaultText, ConsoleColor textColour, ConsoleColor highlightColour, string defaultItem = "", bool center=false) : 
            base(x, y, textColour, highlightColour)
        {
            this.items = items;
            this.defaultText = defaultText;
            this.title = title;
            text = defaultText;

            if (defaultItem != "")
            {
                text = defaultItem;
            }

            // find longest string in items
            foreach (string item in items)
            {
                if (item.Length + 2 > width)
                {
                    width = item.Length + 2;
                }
            }

            // if shorter than default use default width
            if (width < defaultText.Length + 4)
            {
                width = defaultText.Length + 4;
            }

            // if shorter than title use title width
            if (width < title.Length + 4)
            {
                width = title.Length + 4;
            }

            if (center)
            {
                pos.x = (Console.WindowWidth / 2) - (width / 2) - 1;
            }
        }


        /// <summary>
        /// This function displays the drop-down menu in the console.
        /// </summary>
        public override void Display()
        {
            Coordinates oldPos = new Coordinates(Console.CursorLeft, Console.CursorTop);
            ConsoleColor oldFg = Console.ForegroundColor;

            if (highlighted)
            {
                Console.ForegroundColor = hlColour;
            }
            else
            {
                Console.ForegroundColor = fgColour;
            }

            int leftPadding = (width - text.Length - 2) / 2;
            int rightPadding = width - text.Length - 2 - leftPadding;

            Console.SetCursorPosition(pos.x, pos.y);
            Console.Write("┌─" + title + new string('─', width - title.Length - 1) + "┐");
            Console.SetCursorPosition(pos.x, pos.y + 1);
            Console.Write("│" + new string(' ', leftPadding) + text + " v" + new string(' ', rightPadding) + "│");
            Console.SetCursorPosition(pos.x, pos.y + 2);
            Console.Write("└" + new string('─', width) + "┘");

            Console.ForegroundColor = oldFg;
            Console.SetCursorPosition(oldPos.x, oldPos.y);
        }


        /// <summary>
        /// This function allows the user to select an item in the drop-down menu.
        /// </summary>
        public void GetSelection()
        {
            int prevSelectedIndex = 0;
            ConsoleColor oldBg = Console.BackgroundColor;
            ConsoleColor oldFg = Console.ForegroundColor;

            Extend();

            ConsoleKeyInfo info;
            while (true)
            {
                info = Console.ReadKey(true);
                prevSelectedIndex = selectedIndex;

                if (info.Key == ConsoleKey.Enter) // finish selection
                {
                    break;
                }
                else if (info.Key == ConsoleKey.UpArrow) // move up in list
                {
                    selectedIndex--;

                    if (selectedIndex < 0)
                    {
                        selectedIndex = items.Length - 1;
                    }
                }
                else if (info.Key == ConsoleKey.DownArrow) // move down in list
                {
                    selectedIndex++;

                    if (selectedIndex >= items.Length)
                    {
                        selectedIndex = 0;
                    }
                }

                ChangeSelection(prevSelectedIndex, selectedIndex);
            }

            Console.BackgroundColor = oldBg;
            Console.ForegroundColor = oldFg;
            text = items[selectedIndex];
        }


        /// <summary>
        /// This function gets the currently selected value in the menu.
        /// </summary>
        /// <returns>string : The selected value.</returns>
        public override string GetValue()
        {
            return items[selectedIndex];
        }


        /// <summary>
        /// This function is called when the menu is selected and opens the drop-down menu.
        /// </summary>
        public override void Click()
        {
            GetSelection();
        }


        /// <summary>
        /// This function switches the selection of the menu.
        /// </summary>
        /// <param name="originalIndex">The index of the item to deselect.</param>
        /// <param name="newIndex">The index of the item to select.</param>
        private void ChangeSelection(int originalIndex, int newIndex)
        {
            Coordinates oldPos = new Coordinates(Console.CursorLeft, Console.CursorTop);

            // unhighlight the last selection
            Console.SetCursorPosition(pos.x, pos.y + 2 + originalIndex);
            Console.ForegroundColor = fgColour;

            int leftPadding = (width - items[originalIndex].Length) / 2;
            int rightPadding = width - items[originalIndex].Length - leftPadding;

            Console.Write("│" + new string(' ', leftPadding) + items[originalIndex] + new string(' ', rightPadding) + "│");

            // highlight the new selection
            Console.SetCursorPosition(pos.x, pos.y + 2 + newIndex);
            Console.ForegroundColor = hlColour;
            leftPadding = (width - items[newIndex].Length) / 2;
            rightPadding = width - items[newIndex].Length - leftPadding;

            Console.Write("│" + new string(' ', leftPadding) + items[newIndex] + new string(' ', rightPadding) + "│");

            Console.SetCursorPosition(oldPos.x, oldPos.y);
        }


        /// <summary>
        /// This function extends the drop-down menu.
        /// </summary>
        private void Extend()
        {
            // changes top text to default text and unhighlights it
            text = defaultText;
            Unselect();

            Coordinates oldPos = new Coordinates(Console.CursorLeft, Console.CursorTop);

            Console.SetCursorPosition(pos.x, pos.y + 2);
            Console.ForegroundColor = fgColour;

            // draw dropdown items
            int i = 0;
            foreach (string item in items)
            {
                if (i == selectedIndex)
                {
                    Console.ForegroundColor = hlColour;
                }
                else
                {
                    Console.ForegroundColor = fgColour;
                }

                int leftPadding = (width - item.Length) / 2;
                int rightPadding = width - item.Length - leftPadding;

                Console.Write("│" + new string(' ', leftPadding) + item + new string(' ', rightPadding) + "│");

                i++;
                Console.SetCursorPosition(pos.x, pos.y + 2 + i);
            }

            Console.Write("└" + new string('─', width) + "┘");

            Console.SetCursorPosition(oldPos.x, oldPos.y);
        }
    }
}
