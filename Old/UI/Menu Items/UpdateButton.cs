﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI
{
    /// <summary>
    /// This class is used to create a submit button in the console.
    /// 
    /// Programmers:    William Bicknell, Jacob Funes, Firas Areibi, Moises Diaz<br/>
    /// Date:           2018-11-20
    /// </summary>
    class UpdateButton : Button
    {
        public UpdateButton(int x, int y, string text, ConsoleColor textColour, ConsoleColor highlightColour, char hotKey = '\0', bool center = false) :
            base(x, y, text, textColour, highlightColour, null, hotKey, center)
        {

        }

        public override void Click()
        {
            return;
        }
    }
}
