﻿using System;
using Common;

namespace UI
{
    /// <summary>
    /// This class is used to create a switch page button in the console.
    /// 
    /// Programmers:    William Bicknell, Jacob Funes, Firas Areibi, Moises Diaz<br/>
    /// Date:           2018-11-28
    /// </summary>
    public class SwitchPageButton : Button
    {
        private readonly BaseMenu nextMenu;

        public SwitchPageButton(int x, int y, string text, ConsoleColor textColour, ConsoleColor highlightColour, BaseMenu nextMenu, char hotKey = '\0', bool center = false) :
            base(x, y, text, textColour, highlightColour, null, hotKey, center)
        {
            this.nextMenu = nextMenu;
        }

        public override void Click()
        {
            if (nextMenu != null)
            {
                Common.Common.lastMenu.Push(Common.Common.currentMenu);
                Common.Common.currentMenu = nextMenu;
            }
            else
            {
                Common.Common.currentMenu = Common.Common.lastMenu.Pop();
            }

            Unselect();
        }
    }
}
