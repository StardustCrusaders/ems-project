using System;
using Common;

namespace UI
{
    /// <summary>
    /// This class is used to create a text box in the console.
    /// 
    /// Programmers:    William Bicknell, Jacob Funes, Firas Areibi, Moises Diaz<br/>
    /// Date:           2018-11-08
    /// </summary>
    public class TextBox : MenuItem
    {
        private readonly string title;
        private readonly int maxInputChars;
        private readonly string validatorErrorText;
        private readonly ConsoleColor errorColour = ConsoleColor.Red;
        private readonly bool required;
        private string text = "";
        private string errorText = "";
        private Validator validator;
        private bool isValid = true;
        private int width;

        public TextBox(int x, int y, int maxInputChars, string title, Validator validator, string errorText, ConsoleColor textColour, ConsoleColor highlightColour, bool required=false, bool center=false) :
            base(x, y, textColour, highlightColour)
        {
            this.title = title;
            validatorErrorText = errorText;
            this.maxInputChars = maxInputChars;
            this.validator = validator;
            this.required = required;

            width = maxInputChars + 2;
            if (maxInputChars < title.Length)
            {
                width = title.Length + 2;
            }

            if (center)
            {
                pos.x = (Console.WindowWidth / 2) - (width / 2) - 1;
            }
        }


        /// <summary>
        /// This function displays the text box in the console.
        /// </summary>
        public override void Display()
        {
            Coordinates oldPos = new Coordinates(Console.CursorLeft, Console.CursorTop);
            ConsoleColor oldFg = Console.ForegroundColor;

            if (highlighted)
            {
                Console.ForegroundColor = hlColour;
            }
            else
            {
                Console.ForegroundColor = fgColour;
            }

            Console.SetCursorPosition(pos.x, pos.y);
            Console.Write("┌─" + title + new string('─', width - title.Length - 1) + "┐");

            Console.SetCursorPosition(pos.x, pos.y + 1);
            Console.Write("│" + text + new string(' ', width - text.Length) + "│");

            Console.SetCursorPosition(pos.x, pos.y + 2);
            Console.Write("└" + new string('─', width) + "┘");

            Console.CursorLeft++;
            Console.CursorTop--;
            Console.Write(new string(' ', validatorErrorText.Length));
            Console.CursorLeft -= validatorErrorText.Length;

            if (!isValid)
            {
                Console.ForegroundColor = errorColour;

                Console.Write(errorText);
            }

            Console.ForegroundColor = oldFg;
            Console.SetCursorPosition(oldPos.x, oldPos.y);
        }


        /// <summary>
        /// This function returns the value in the text box.
        /// </summary>
        /// <returns>string : The text that is in the text box.</returns>
        public override string GetValue()
        {
            string value = text;
            text = "";

            return value;
        }


        /// <summary>
        /// This function allows the user to enter text into the text box when it is selected.
        /// </summary>
        public override void Click()
        {
            Unselect();
            Console.CursorVisible = true;
            Console.SetCursorPosition(pos.x + 1 + text.Length, pos.y + 1);

            int index = text.Length - 1;
            ConsoleKeyInfo info;
            while (true)
            {
                info = Console.ReadKey(true);

                if (info.Key == ConsoleKey.Enter)
                {
                    Validate();
                    break;
                }
                else if (info.Key == ConsoleKey.Backspace)
                {
                    if (Console.CursorLeft > pos.x + 1)
                    {
                        if (index == text.Length - 1)
                        {
                            text = text.Remove(text.Length - 1);
                        }
                        else
                        {
                            text = text.Remove(index, 1);
                        }

                        index--;
                        Console.CursorLeft--;
                    }
                }
                else if (info.Key == ConsoleKey.LeftArrow)
                {
                    if (Console.CursorLeft > pos.x + 1)
                    {
                        Console.CursorLeft--;
                        index--;
                    }
                }
                else if (info.Key == ConsoleKey.RightArrow)
                {
                    if (Console.CursorLeft < pos.x + maxInputChars + 1 && index < text.Length - 1)
                    {
                        Console.CursorLeft++;
                        index++;
                    }
                }
                else if (info.KeyChar >= ' ' && info.KeyChar <= '~' && text.Length < maxInputChars)
                {
                    if (index + 1 < text.Length)
                    {
                        string a = text.Substring(0, index + 1);
                        string b = text.Substring(index + 1);
                        text = a + info.KeyChar + b;
                        Console.Write(info.KeyChar + b);
                        Console.CursorLeft -= b.Length;
                    }
                    else
                    {
                        text += info.KeyChar;
                        Console.Write(info.KeyChar);
                    }

                    index++;
                }

                Display();
            }

            Console.CursorVisible = false;
            Select();
        }
        
        
        /// <summary>
        /// This function runs the validate function attached to the TextBox.
        /// </summary>
        /// <returns>bool : true if valid, false otherwise.</returns>
        public override bool Validate()
        {
            if (required && text == "")
            {
                isValid = false;
                errorText = "Entry is required!";
            }
            else
            {
                if (text != "")
                {
                    if (validator != null)
                    {
                        isValid = validator(text);
                        errorText = validatorErrorText;
                    }
                }
            }

            return isValid;
        }
    }
}
