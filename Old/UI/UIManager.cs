﻿using System;
using System.Linq;
using System.Collections.Generic;
using Common;
using static Demographics.Demographics;
using static FileIO.FileIO;
using static Scheduling.Scheduling;
using static Billing.Billing;

namespace UI
{
    /// <summary>
    /// This class is used to manage the UI of the program.
    /// 
    /// Programmers:    William Bicknell, Jacob Funes, Firas Areibi, Moises Diaz<br/>
    /// Date:           2018-11-28
    /// </summary>
    public class UIManager
    {
        private const int WINDOW_WIDTH = 120;
        private const int WINDOW_HEIGHT = 54;
        private const int YEAR = 2018;
        private readonly int[] monthLengths = { 31, 30, 31 };
        private readonly string[] monthNames = { "October", "November", "December" };
        private readonly int[] monthNums = { 10, 11, 12 };
        private readonly int[] monthStart = { 2, 5, 7 };

        private BaseMenu MainMenu;
        private BaseMenu PatientMenu;
        private BaseMenu AddPatientMenu;
        private BaseMenu SearchPatientMenu;
        private BaseMenu AppointmentMenu;
        private BaseMenu ScheduleMenu;
        private BaseMenu SearchAppointmentMenu;
        private BaseMenu BillingMenu;
        private BaseMenu BillingCodeMenu;
        private BaseMenu BillingSummaryMenu;
        private BaseMenu BillingFileMenu;
        private DemographicInfo currentClient;
        private SchedulingInfo currentAppointment;
        public static Label currentClientLabel = new Label(1, 5, "", ConsoleColor.White);
        private List<DemographicInfo> dInfoSearchResults = new List<DemographicInfo>();
        private List<SchedulingInfo> sInfoSearchResults = new List<SchedulingInfo>();
        private bool updateCalendar = true;
        private bool goBack = false;

        private enum PatientInfoOrder
        {
            HCN,
            FirstName,
            LastName,
            mInitial,
            DOB,
            sex,
            HeadOfHouse,
            Address1,
            Address2,
            City,
            Province,
            PhoneNumber
        }

        public UIManager()
        {
            // Window size is number of coloumns and rows
            Console.SetWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
            // Set buffer size the same as window size to get rid of scroll bars
            Console.SetBufferSize(WINDOW_WIDTH, WINDOW_HEIGHT);

            Console.CursorVisible = false;

            // Set window title
            Console.Title = "EMS V1";

            BillingFileMenu = new BaseMenu
            (
                new MenuItem[]
                {
                    new DropDown(0, 20, monthNames, "Month", "- - -", ConsoleColor.White, ConsoleColor.Yellow, monthNames[0], true),
                    new SubmitButton(0, WINDOW_HEIGHT - 5, "Generate", ConsoleColor.White, ConsoleColor.Yellow, 'G', true),
                    new SwitchPageButton(5, WINDOW_HEIGHT - 5, "Back", ConsoleColor.White, ConsoleColor.Yellow, null, 'B')
                },

                "Generate Monthly Billing File"
            );

            BillingSummaryMenu = new BaseMenu
            (
                new MenuItem[]
                {
                    new TextBox(0, 20, 30, "MoH Response File Name", null, "", ConsoleColor.White, ConsoleColor.Yellow, true, true),
                    new SubmitButton(0, WINDOW_HEIGHT - 5, "Generate", ConsoleColor.White, ConsoleColor.Yellow, 'G', true),
                    new SwitchPageButton(5, WINDOW_HEIGHT - 5, "Back", ConsoleColor.White, ConsoleColor.Yellow, null, 'B')
                },

                "Generate Monthly Billing Summary"
            );

            BillingCodeMenu = new BaseMenu
            (
                new MenuItem[]
                {
                    new TextBox(0, 20, 30, "Space Separated Billing Codes", null, "", ConsoleColor.White, ConsoleColor.Yellow, true, true),
                    new DropDown(0, 30, new string[] { "0", "1", "2", "3" }, "Recall", "- - -", ConsoleColor.White, ConsoleColor.Yellow, "0", true),
                    new SubmitButton(0, WINDOW_HEIGHT - 5, "Submit", ConsoleColor.White, ConsoleColor.Yellow, 'S', true),
                    new SwitchPageButton(5, WINDOW_HEIGHT - 5, "Back", ConsoleColor.White, ConsoleColor.Yellow, null, 'B')
                },

                "Billing Code Entry"
            );

            BillingMenu = new BaseMenu
            (
                new MenuItem[]
                {
                    new SwitchPageButton(0, 20, "Generate Monthly Billing File", ConsoleColor.White, ConsoleColor.Yellow, BillingFileMenu, 'F', true),
                    new SwitchPageButton(0, 25, "Generate Monthly Billing Summary", ConsoleColor.White, ConsoleColor.Yellow, BillingSummaryMenu, 'S', true),
                    new SwitchPageButton(5, WINDOW_HEIGHT - 5, "Back", ConsoleColor.White, ConsoleColor.Yellow, null, 'B')
                },

                "Billing Management"
            );

            CreateSchedulingMenu();

            SearchAppointmentMenu = new BaseMenu
            (
                new MenuItem[]
                {
                    new DataViewer(0, 15, new string[] { "Appointment Date", "Appointment Timeslot" }, null, ConsoleColor.White, ConsoleColor.Yellow, true),
                    new SwitchPageButton(WINDOW_WIDTH - 25, WINDOW_HEIGHT - 5, "Add Billing Codes", ConsoleColor.White, ConsoleColor.Yellow, BillingCodeMenu, 'D'),
                    new SubmitButton(0, WINDOW_HEIGHT - 5, "Delete", ConsoleColor.White, ConsoleColor.Yellow, 'D', true, false),
                    new SwitchPageButton(5, WINDOW_HEIGHT - 5, "Back", ConsoleColor.White, ConsoleColor.Yellow, null, 'B')
                },

                "Search for Appointments"
            );

            AppointmentMenu = new BaseMenu
            (
                new MenuItem[]
                {
                    new SwitchPageButton(0, 20, "Schedule Appointment", ConsoleColor.White, ConsoleColor.Yellow, ScheduleMenu, 'S', true),
                    new SwitchPageButton(0, 25, "Search for Appointment", ConsoleColor.White, ConsoleColor.Yellow, SearchAppointmentMenu, 'A', true),
                    new SwitchPageButton(5, WINDOW_HEIGHT - 5, "Back", ConsoleColor.White, ConsoleColor.Yellow, null, 'B')
                },

                "Appointment Management"
            );

            SearchPatientMenu = new BaseMenu
            (
                new MenuItem[]
                {
                    new TextBox(0, 10, 12, "HCN", HCNValidator, "Invalid HCN!", ConsoleColor.White, ConsoleColor.Yellow, center: true),
                    new TextBox(0, 16, 25, "First Name", NoNumbersValidator, "Cannot contain numbers!", ConsoleColor.White, ConsoleColor.Yellow, center: true),
                    new TextBox(0, 22, 25, "Last Name", NoNumbersValidator, "Cannot contain numbers!", ConsoleColor.White, ConsoleColor.Yellow, center: true),
                    new DataViewer(0, 28, new string[] { "HCN", "Name", "Head of House HCN" }, null, ConsoleColor.White, ConsoleColor.Yellow, true),
                    new UpdateButton(0, WINDOW_HEIGHT - 5, "Search", ConsoleColor.White, ConsoleColor.Yellow, 'S', true),
                    new SwitchPageButton(5, WINDOW_HEIGHT - 5, "Back", ConsoleColor.White, ConsoleColor.Yellow, null, 'B')
                },

                "Search For Patient",

                new Label[]
                {
                    new Label(20, 10, "Search by:", ConsoleColor.White),
                    new Label(0, 14, "OR", ConsoleColor.White, true),
                    new Label(0, 20, "AND", ConsoleColor.White, true)
                }
            );

            AddPatientMenu = new BaseMenu
            (
                new MenuItem[]
                {
                    new TextBox(1, 10, 12, "HCN* ", HCNValidator, "Invalid HCN!", ConsoleColor.White, ConsoleColor.Yellow, true),
                    new TextBox(1, 15, 25, "First Name* ", NoNumbersValidator, "Cannot contain numbers!", ConsoleColor.White, ConsoleColor.Yellow, true),
                    new TextBox(1, 20, 25, "Last Name* ", NoNumbersValidator, "Cannot contain numbers!", ConsoleColor.White, ConsoleColor.Yellow, true),
                    new TextBox(1, 25, 1, "M. Initial", NoNumbersValidator, "Cannot contain numbers!", ConsoleColor.White, ConsoleColor.Yellow),
                    new TextBox(1, 30, 12, "D.O.B* ", DoBValidator, "", ConsoleColor.White, ConsoleColor.Yellow, true),
                    new DropDown(1, 35, new string[] { "M", "F", "I", "H" }, "Sex* ", "- - -", ConsoleColor.White, ConsoleColor.Yellow, "M"),
                    new TextBox(60, 10, 12, "Head of House", HCNValidator, "Invalid HoH!", ConsoleColor.White, ConsoleColor.Yellow),
                    new TextBox(60, 15, 25, "Address 1** ", AddressFormat, "Invalid address!", ConsoleColor.White, ConsoleColor.Yellow),
                    new TextBox(60, 20, 25, "Address 2", AddressLine2Validator, "Invalid address!", ConsoleColor.White, ConsoleColor.Yellow),
                    new TextBox(60, 25, 25, "City** ", NoNumbersValidator, "Cannot contain numbers!", ConsoleColor.White, ConsoleColor.Yellow),
                    new DropDown(60, 30, new string[] { "ON", "AB", "BC", "MB", "NB", "NL", "NT", "NS", "NU", "PE", "QC", "SK", "YT" }, "Province** ", "- - -", ConsoleColor.White, ConsoleColor.Yellow, "ON"),
                    new TextBox(60, 35, 12, "Phone Number** ", PhoneValidator, "Expected form: xxx-xxx-xxxx", ConsoleColor.White, ConsoleColor.Yellow),
                    new SubmitButton(WINDOW_WIDTH - 15, WINDOW_HEIGHT - 5, "Submit", ConsoleColor.White, ConsoleColor.Yellow, 'S'),
                    new SwitchPageButton(5, WINDOW_HEIGHT - 5, "Back", ConsoleColor.White, ConsoleColor.Yellow, null, 'B')
                },

                "Add Patient",

                new Label[]
                {
                    new Label(0, WINDOW_HEIGHT - 4, "* - Required    ** - Required if Head of House is not filled", ConsoleColor.Red, true)
                }
            );

            PatientMenu = new BaseMenu
            (
                new MenuItem[]
                {
                    new SwitchPageButton(0, 20, "Add Patient", ConsoleColor.White, ConsoleColor.Yellow, AddPatientMenu, 'A', true),
                    new SwitchPageButton(0, 25, "Search for Patient", ConsoleColor.White, ConsoleColor.Yellow, SearchPatientMenu, 'S', true),
                    new SwitchPageButton(5, WINDOW_HEIGHT - 5, "Back", ConsoleColor.White, ConsoleColor.Yellow, null, 'B')
                },

                "Patient Management"
            );

            MainMenu = new BaseMenu
            (
                new MenuItem[]
                {
                    new SwitchPageButton(0, 20, "Patient Management", ConsoleColor.White, ConsoleColor.Yellow, PatientMenu, 'P', true),
                    new SwitchPageButton(0, 25, "Appointment Management", ConsoleColor.White, ConsoleColor.Yellow, AppointmentMenu, 'A', true),
                    new SwitchPageButton(0, 30, "Billing Management", ConsoleColor.White, ConsoleColor.Yellow, BillingMenu, 'B', true),
                    new Button(5, WINDOW_HEIGHT - 5, "Exit", ConsoleColor.White, ConsoleColor.Yellow, Exit, 'x')
                },

                "EMS - Main Menu"
            );

            Common.Common.currentMenu = MainMenu;
        }


        /// <summary>
        /// This function displays the menus and runs menu input loops.
        /// </summary>
        public void RunUI()
        {
            while (true)
            {
                if (Common.Common.currentMenu == MainMenu)
                {
                    ReloadMainMenu();

                    MainMenu.Display();
                    DisplayCurrentClient();
                    MainMenu.CheckInput();
                }
                else if (Common.Common.currentMenu == PatientMenu)
                {
                    PatientMenu.Display();
                    DisplayCurrentClient();
                    PatientMenu.CheckInput();
                }
                else if (Common.Common.currentMenu == AddPatientMenu)
                {
                    AddPatientMenu.Display();
                    DisplayCurrentClient();
                    AddPatientMenu.CheckInput();

                    if (AddPatientMenu.isSubmitExit)
                    {
                        string[] userData = AddPatientMenu.GetUserInputs();
                        DemographicInfo dInfo;

                        if (userData[(int)PatientInfoOrder.HeadOfHouse] == "")
                        {
                            dInfo = new DemographicInfo(
                                userData[(int)PatientInfoOrder.HCN],
                                userData[(int)PatientInfoOrder.FirstName],
                                userData[(int)PatientInfoOrder.LastName],
                                userData[(int)PatientInfoOrder.mInitial],
                                userData[(int)PatientInfoOrder.DOB],
                                userData[(int)PatientInfoOrder.sex],
                                userData[(int)PatientInfoOrder.Address1],
                                userData[(int)PatientInfoOrder.Address2],
                                userData[(int)PatientInfoOrder.City],
                                userData[(int)PatientInfoOrder.Province],
                                userData[(int)PatientInfoOrder.PhoneNumber]);
                        }
                        else
                        {
                            dInfo = new DemographicInfo(
                                userData[(int)PatientInfoOrder.HCN],
                                userData[(int)PatientInfoOrder.FirstName],
                                userData[(int)PatientInfoOrder.LastName],
                                userData[(int)PatientInfoOrder.mInitial],
                                userData[(int)PatientInfoOrder.DOB],
                                userData[(int)PatientInfoOrder.sex],
                                userData[(int)PatientInfoOrder.HeadOfHouse]);
                        }

                        WriteDemographic(dInfo);

                        // Keep track of current client
                        SetCurrentClient(dInfo);
                    }
                }
                else if (Common.Common.currentMenu == SearchPatientMenu)
                {
                    SearchPatientMenu.Display();
                    DisplayCurrentClient();
                    SearchPatientMenu.CheckInput();

                    string[] userData = SearchPatientMenu.GetUserInputs();

                    if (SearchPatientMenu.isUpdateExit)
                    {
                        UpdateClientSearch(userData);
                    }
                    else if (SearchPatientMenu.isSubmitExit)
                    {
                        SetCurrentClient(dInfoSearchResults[Int32.Parse(userData[3])]);
                        goBack = false;
                    }
                }
                else if (Common.Common.currentMenu == AppointmentMenu)
                {
                    if (currentClientLabel.text == "" && !goBack)
                    {
                        Common.Common.lastMenu.Push(AppointmentMenu);
                        Common.Common.currentMenu = SearchPatientMenu;
                        goBack = true;
                        continue;
                    }
                    else if (goBack)
                    {
                        Common.Common.currentMenu = MainMenu;
                        goBack = false;
                        continue;
                    }

                    AppointmentMenu.Display();
                    DisplayCurrentClient();
                    AppointmentMenu.CheckInput();
                }
                else if (Common.Common.currentMenu == ScheduleMenu)
                {
                    // Make sure fully booked days are always updated
                    if (updateCalendar)
                    {
                        CreateSchedulingMenu(true);
                        updateCalendar = false;
                    }

                    ScheduleMenu.Display();
                    DisplayCurrentClient();
                    ScheduleMenu.CheckInput();

                    if (ScheduleMenu.isUpdateExit)
                    {
                        CreateSchedulingMenu(true, true);
                    }
                    else if (ScheduleMenu.isSubmitExit)
                    {
                        string[] userData = ScheduleMenu.GetUserInputs();
                        string[] date = userData[0].Split('/');

                        SchedulingInfo sInfo = new SchedulingInfo(currentClient.HCN, Int32.Parse(userData[1]), Int32.Parse(userData[2]), Int32.Parse(date[1]), Int32.Parse(date[0]), Int32.Parse(date[2]));

                        WriteScheduling(sInfo);

                        updateCalendar = true;
                    }
                }
                else if (Common.Common.currentMenu == SearchAppointmentMenu)
                {
                    currentAppointment = new SchedulingInfo();

                    UpdateAppointmentSearch(currentClient.HCN);
                    SearchAppointmentMenu.Display();
                    DisplayCurrentClient();
                    SearchAppointmentMenu.CheckInput();

                    string[] userData = SearchAppointmentMenu.GetUserInputs();

                    if (sInfoSearchResults.Count > 0)
                    {
                        currentAppointment = sInfoSearchResults[Int32.Parse(userData[0])];
                    }

                    if (SearchAppointmentMenu.isSubmitExit)
                    {
                        if (currentAppointment.HCN != "")
                        {
                            RemoveSchedulingInfo(currentClient.HCN, currentAppointment.month, currentAppointment.day);
                        }
                    }
                }
                else if (Common.Common.currentMenu == BillingMenu)
                {
                    BillingMenu.Display();
                    DisplayCurrentClient();
                    BillingMenu.CheckInput();
                }
                else if (Common.Common.currentMenu == BillingCodeMenu)
                {
                    if (currentAppointment.HCN == "")
                    {
                        Common.Common.currentMenu = Common.Common.lastMenu.Pop();
                    }

                    BillingCodeMenu.Display();
                    BillingCodeMenu.CheckInput();

                    if (BillingCodeMenu.isSubmitExit)
                    {
                        string[] userData = BillingCodeMenu.GetUserInputs();

                        BillingInfo bInfo = new BillingInfo();
                        SearchBillingInfo(ref bInfo, currentAppointment.HCN, currentAppointment.day, currentAppointment.month, currentAppointment.year);

                        if (bInfo.billingCodes == null)
                        {
                            bInfo.billingCodes = new List<string>();
                        }

                        foreach (string code in userData[0].Split(' '))
                        {
                            bInfo.billingCodes.Add(code);
                        }

                        if (bInfo.HCN == null)
                        {
                            WriteBilling(new BillingInfo(currentClient.HCN, currentClient.sex[0], currentAppointment.day, currentAppointment.month, currentAppointment.year, bInfo.billingCodes));
                        }
                        else
                        {
                            UpdateBillingInfo(bInfo);
                        }

                        if (userData[1] != "0")
                        {
                            int numWeeks = Int32.Parse(userData[1]);
                            currentAppointment.day += numWeeks * 7;

                            for (int i = 0; i < monthNums.Length; i++)
                            {
                                if (monthNums[i] == currentAppointment.month)
                                {
                                    if (monthLengths[i] < currentAppointment.day)
                                    {
                                        currentAppointment.day -= monthLengths[i];
                                        currentAppointment.month += 1;

                                        if (currentAppointment.month > 12)
                                        {
                                            currentAppointment.month = 1;
                                            currentAppointment.year++;
                                        }
                                    }
                                }
                            }

                            WriteScheduling(currentAppointment);
                        }
                    }
                }
                else if (Common.Common.currentMenu == BillingSummaryMenu)
                {
                    BillingSummaryMenu.Display();
                    BillingSummaryMenu.CheckInput();

                    if (BillingSummaryMenu.isSubmitExit)
                    {
                        string[] userData = BillingSummaryMenu.GetUserInputs();

                        ReadResponseFile(userData[0]);
                    }
                }
                else if (Common.Common.currentMenu == BillingFileMenu)
                {
                    BillingFileMenu.Display();
                    BillingFileMenu.CheckInput();

                    if (BillingFileMenu.isSubmitExit)
                    {
                        string[] userData = BillingFileMenu.GetUserInputs();
                        List<BillingInfo> monthInfo = new List<BillingInfo>();

                        for (int i = 0; i < monthNames.Length; i++)
                        {
                            if (monthNames[i] == userData[0])
                            {
                                GetBillingInfo(ref monthInfo, monthNums[i], YEAR);
                            }
                        }

                        EnterInfo(monthInfo);
                    }
                }
            }
        }


        /// <summary>
        /// This function exits the program.
        /// </summary>
        private void Exit()
        {
            Environment.Exit(0);
        }


        /// <summary>
        /// This function creates the scheduling menu to update the calendar.
        /// </summary>
        /// <param name="update">Whether the new menu is from an update.</param>
        private void CreateSchedulingMenu(bool setLast=false, bool update=false)
        {
            int[][] fullDays = new int[monthNames.Length][];
            for (int i = 0; i < monthNames.Length; i++)
            {
                fullDays[i] = GetFullDaysInMonth(monthNums[i], monthLengths[i]);
            }

            if (update)
            {
                string[] userData = ScheduleMenu.GetUserInputs();
                string[] date = userData[0].Split('/');
                List<string> timeSlots = new List<string> { "1", "2", "3", "4", "5", "6" };

                List<SchedulingInfo> appointments = new List<SchedulingInfo>();
                int month = Int32.Parse(date[0]);
                int day = Int32.Parse(date[1]);

                GetScheduleInfo(ref appointments, month, day);

                foreach (SchedulingInfo sInfo in appointments)
                {
                    if (timeSlots.Contains(sInfo.timeSlot.ToString()))
                    {
                        timeSlots.Remove(sInfo.timeSlot.ToString());
                    }
                }

                int selectedMonth = 0;
                for (int i = 0; i < monthNums.Length; i++)
                {
                    if (monthNums[i] == month)
                    {
                        selectedMonth = i;
                        break;
                    }
                }

                ScheduleMenu = new BaseMenu
                (
                    new MenuItem[]
                    {
                        new Calendar(0, 10, monthStart, monthLengths, monthNames, monthNums, fullDays, YEAR, ConsoleColor.White, ConsoleColor.Yellow, ConsoleColor.Gray, true, selectedMonth, day),
                        new DropDown(0, 30, timeSlots.ToArray(), "Time Slot", "- - -", ConsoleColor.White, ConsoleColor.Yellow, timeSlots[0], true),
                        new DropDown(0, 35, new string[] { "1", "2" },  "# of Patients", "- - -", ConsoleColor.White, ConsoleColor.Yellow, "1", true),
                        new SubmitButton(0, WINDOW_HEIGHT - 5, "Book Appointment", ConsoleColor.White, ConsoleColor.Yellow, 'A', true),
                        new SwitchPageButton(5, WINDOW_HEIGHT - 5, "Back", ConsoleColor.White, ConsoleColor.Yellow, null, 'B')
                    },

                    "Schedule Appointment",

                    new Label[]
                    {
                        currentClientLabel
                    }
                );
            }
            else
            {
                ScheduleMenu = new BaseMenu
                (
                    new MenuItem[]
                    {
                        new Calendar(0, 10, monthStart, monthLengths, monthNames, monthNums, fullDays, YEAR, ConsoleColor.White, ConsoleColor.Yellow, ConsoleColor.Gray, true),
                        new SwitchPageButton(5, WINDOW_HEIGHT - 5, "Back", ConsoleColor.White, ConsoleColor.Yellow, null, 'B')
                    },

                    "Schedule Appointment"
                );
            }

            // Update to contain new schedule menu
            AppointmentMenu = new BaseMenu
            (
                new MenuItem[]
                {
                    new SwitchPageButton(0, 20, "Schedule Appointment", ConsoleColor.White, ConsoleColor.Yellow, ScheduleMenu, 'S', true),
                    new SwitchPageButton(0, 25, "Search for Appointment", ConsoleColor.White, ConsoleColor.Yellow, SearchAppointmentMenu, 'A', true),
                    new SwitchPageButton(5, WINDOW_HEIGHT - 5, "Back", ConsoleColor.White, ConsoleColor.Yellow, null, 'B')
                },

                "Appointment Management"
            );

            if (setLast)
            {
                Common.Common.lastMenu.Pop();
                Common.Common.lastMenu.Push(AppointmentMenu);

                Common.Common.currentMenu = ScheduleMenu;
            }
        }


        /// <summary>
        /// This function sets the currently selected client.
        /// </summary>
        /// <param name="dInfo">The new client.</param>
        private void SetCurrentClient(DemographicInfo dInfo)
        {
            currentClient = dInfo;
            currentClientLabel.text = "Current Client: " + currentClient.HCN + " - " + currentClient.firstName + " " + currentClient.lastName;
            currentClientLabel.Display();
        }


        /// <summary>
        /// This function updates the appointment search menu.
        /// </summary>
        /// <param name="HCN">The HCN of the currently selected user.</param>
        private void UpdateAppointmentSearch(string HCN)
        {
            sInfoSearchResults.Clear();
            SearchSchedulingInfoHCN(ref sInfoSearchResults, HCN, monthLengths, monthNums);

            string[][] infoLines = new string[sInfoSearchResults.Count][];
            int i = 0;
            foreach (SchedulingInfo sInfo in sInfoSearchResults)
            {
                infoLines[i] = new string[] { sInfo.month + "/" + sInfo.day + "/" + sInfo.year, sInfo.timeSlot.ToString() };
                i++;
            }

            SearchAppointmentMenu = new BaseMenu
            (
                new MenuItem[]
                {
                    new DataViewer(0, 15, new string[] { "Appointment Date", "Appointment Timeslot" }, infoLines, ConsoleColor.White, ConsoleColor.Yellow, true),
                    new SwitchPageButton(WINDOW_WIDTH - 25, WINDOW_HEIGHT - 5, "Add Billing Codes", ConsoleColor.White, ConsoleColor.Yellow, BillingCodeMenu, 'A'),
                    new SubmitButton(0, WINDOW_HEIGHT - 5, "Delete", ConsoleColor.White, ConsoleColor.Yellow, 'D', true, false),
                    new SwitchPageButton(5, WINDOW_HEIGHT - 5, "Back", ConsoleColor.White, ConsoleColor.Yellow, null, 'B')
                },

                "Search for Appointments"
            );

            // Update to contain new search menu
            AppointmentMenu = new BaseMenu
            (
                new MenuItem[]
                {
                    new SwitchPageButton(0, 20, "Schedule Appointment", ConsoleColor.White, ConsoleColor.Yellow, ScheduleMenu, 'S', true),
                    new SwitchPageButton(0, 25, "Search for Appointment", ConsoleColor.White, ConsoleColor.Yellow, SearchAppointmentMenu, 'A', true),
                    new SwitchPageButton(5, WINDOW_HEIGHT - 5, "Back", ConsoleColor.White, ConsoleColor.Yellow, null, 'B')
                },

                "Appointment Management"
            );

            Common.Common.lastMenu.Pop();
            Common.Common.lastMenu.Push(AppointmentMenu);

            Common.Common.currentMenu = SearchAppointmentMenu;
        }


        /// <summary>
        /// This function updates the data viewer in the client search menu.
        /// </summary>
        /// <param name="data">The data taken from the menu.</param>
        private void UpdateClientSearch(string[] data)
        {
            dInfoSearchResults.Clear();
            SearchDemographic(ref dInfoSearchResults, data[0], data[1], data[2]);
            SearchDemographic(ref dInfoSearchResults, data[0], data[1], data[2], true);

            string[][] infoLines = new string[dInfoSearchResults.Count][];
            int i = 0;
            foreach (DemographicInfo dInfo in dInfoSearchResults)
            {
                infoLines[i] = new string[] { dInfo.HCN, dInfo.firstName + " " + dInfo.mInitial.ToString() + " " + dInfo.lastName, dInfo.headOfHouse };
                i++;
            }

            SearchPatientMenu = new BaseMenu
            (
                new MenuItem[]
                {
                                new TextBox(0, 10, 12, "HCN", HCNValidator, "Invalid HCN!", ConsoleColor.White, ConsoleColor.Yellow, center: true),
                                new TextBox(0, 16, 25, "First Name", NoNumbersValidator, "Cannot contain numbers!", ConsoleColor.White, ConsoleColor.Yellow, center: true),
                                new TextBox(0, 22, 25, "Last Name", NoNumbersValidator, "Cannot contain numbers!", ConsoleColor.White, ConsoleColor.Yellow, center: true),
                                new DataViewer(0, 28, new string[] { "HCN", "Name", "Head of House HCN" }, infoLines, ConsoleColor.White, ConsoleColor.Yellow, true),
                                new SubmitButton(WINDOW_WIDTH - 15, WINDOW_HEIGHT - 5, "Select", ConsoleColor.White, ConsoleColor.Yellow, 'l'),
                                new UpdateButton(0, WINDOW_HEIGHT - 5, "Search", ConsoleColor.White, ConsoleColor.Yellow, 'S', true),
                                new SwitchPageButton(5, WINDOW_HEIGHT - 5, "Back", ConsoleColor.White, ConsoleColor.Yellow, null, 'B')
                },

                "Search For Patient",

                new Label[]
                {
                                new Label(20, 10, "Search by:", ConsoleColor.White),
                                new Label(0, 14, "OR", ConsoleColor.White, true),
                                new Label(0, 20, "AND", ConsoleColor.White, true)
                }
            );

            // Update to contain new search menu
            PatientMenu = new BaseMenu
            (
                new MenuItem[]
                {
                    new SwitchPageButton(0, 20, "Add Patient", ConsoleColor.White, ConsoleColor.Yellow, AddPatientMenu, 'A', true),
                    new SwitchPageButton(0, 25, "Search for Patient", ConsoleColor.White, ConsoleColor.Yellow, SearchPatientMenu, 'S', true),
                    new SwitchPageButton(5, WINDOW_HEIGHT - 5, "Back", ConsoleColor.White, ConsoleColor.Yellow, null, 'B')
                },

                "Patient Management"
            );

            if (Common.Common.lastMenu.Pop() == AppointmentMenu)
            {
                Common.Common.lastMenu.Push(AppointmentMenu);
            }
            else {
                Common.Common.lastMenu.Push(PatientMenu);
            }

            Common.Common.currentMenu = SearchPatientMenu;
        }

        
        /// <summary>
        /// This function displays the currently selected client.
        /// </summary>
        private void DisplayCurrentClient()
        {
            if (currentClientLabel.text != "")
            {
                currentClientLabel.Display();
            }
        }


        /// <summary>
        /// This function reloads the main menu.
        /// </summary>
        private void ReloadMainMenu()
        {
            MainMenu = new BaseMenu
            (
                new MenuItem[]
                {
                    new SwitchPageButton(0, 20, "Patient Management", ConsoleColor.White, ConsoleColor.Yellow, PatientMenu, 'P', true),
                    new SwitchPageButton(0, 25, "Appointment Management", ConsoleColor.White, ConsoleColor.Yellow, AppointmentMenu, 'A', true),
                    new SwitchPageButton(0, 30, "Billing Management", ConsoleColor.White, ConsoleColor.Yellow, BillingMenu, 'B', true),
                    new Button(5, WINDOW_HEIGHT - 5, "Exit", ConsoleColor.White, ConsoleColor.Yellow, Exit, 'x')
                },

                "EMS - Main Menu"
            );

            Common.Common.currentMenu = MainMenu;
        }
    }
}
