using System;
using System.Text.RegularExpressions;

namespace Demographics
{
    public static class Demographics
    {
        /// <summary>
        /// Validate the user input, checking if only numbers exist within the string.
        /// If there is a letter, then the input is invalid (ie, the user input a DOB such as "21NO199E")
        /// If there is no letter, the number is valid. 
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool OnlyNumbersValidator(string text)
        {
            int success = 0;
            bool check;
            check = Int32.TryParse(text, out success);

            if (check)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Validate the user input from the text boxes that do not need to contain a number. 
        /// If the input contains a number, return false.
        /// If the input contains no number, return true.
        /// </summary>
        /// <param name="text"> Text that the user input to the box. </param>
        /// <returns> Returns true or false depending on if the input is proper. </returns>
        public static bool NoNumbersValidator(string text)
        {
            string pattern = @"^[a-zA-Z]*$";
            Regex numberRegex = new Regex(pattern);

            if (numberRegex.IsMatch(text))
            { 
                return true;
            }
            return false;
        }

        /// <summary>
        /// Validate the user input from the HCN text box.
        /// If the HCN is not in the proper format (10 Numbers followed by 2 Letterss), return false.
        /// If the format is valid, return true.
        /// </summary>
        /// <param name="text"> Text that the user input to the box. </param>
        /// <returns> Returns true or false depending on if the input is proper. </returns>
        public static bool HCNValidator(string text)
        {
            string pattern = @"^[0-9]{10}[A-Za-z]{2}$";
            Regex HCNRegex = new Regex(pattern);

            if(HCNRegex.IsMatch(text))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Enforce a minimum of 1 number, followed by any numbers of letters without numbers.
        /// If the user inputs an address without a number, return false.
        /// If the user inputs an address with a number, return true.
        /// </summary>
        /// <param name="text"> Text that the user input to the box. </param>
        /// <returns> Returns true or false depending on if the input is proper. </returns>
        public static bool AddressFormat(string text)
        {
            string pattern = @"^\d+\s[\sa-zA-z]*$";
            Regex AddressRegex = new Regex(pattern);

            if (AddressRegex.IsMatch(text))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Enforce DoB to be in DD/MM/YYYY format, user cannot input days past 31, or months past 12.
        /// If the user inputs a day/month out of range (13/30/2001) or (12/32/2000), return true.
        /// If the DoB is a valid day/month, return true.
        /// </summary>
        /// <param name="text"> Text to be tested against regex </param>
        /// <returns> Returns true or false depending on if the input is proper. </returns>  
        public static bool DoBValidator(string text)
        {
            string pattern = @"^([0]{0,1}[1-9]{1}|[12][0-9]|[3][0-1])\/([0]{0,1}[1-9]{1}|1[012])\/(19|20)\d{2}$";
            Regex dateValidator = new Regex(pattern);

            if(dateValidator.IsMatch(text))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Phone Validator, enforcing format 000-000-0000.
        /// If the phone number isn't in the above format, return false.
        /// If the phone number is in proper format, return true.
        /// </summary>
        /// <param name="text"> Text to be tested against regex </param>
        /// <returns> Returns true or false depending on if the input is proper. </returns>
        public static bool PhoneValidator(string text)
        {
            string pattern = @"^[0-9]{3}-[0-9]{3}-[0-9]{4}$";
            Regex phoneRegex = new Regex(pattern);

            if (phoneRegex.IsMatch(text))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Address validator, allowing user to input Unit/Apt followed by 1 or 2 characters
        /// If the user inputs anything other than Unit/Apt followed by a number, return false.
        /// If the user inputs Unit followed by a number or Apt followed by a number, return true.
        /// </summary>
        /// <param name="text"> Text to be tested against regex </param>
        /// <returns> Returns true or false depending on if the input is proper. </returns>
        public static bool AddressLine2Validator(string text)
        {
            string pattern = @"^([u|U]nit|[a|A]pt|[b|B]ldg){1} [0-9]{1,2}$";
            Regex addressRegex = new Regex(pattern);

            if (addressRegex.IsMatch(text))
            {
                return true;
            }
            return false;
        }
    }
}