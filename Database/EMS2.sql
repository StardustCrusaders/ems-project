﻿USE [master]
GO

IF OBJECT_ID('ems2') IS NOT NULL
BEGIN
	DROP DATABASE [ems2]
END
GO


/****** Object:  Database [ems2]    Script Date: 2/20/2019 2:43:33 PM ******/
CREATE DATABASE [ems2]
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ems2] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ems2] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ems2] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ems2] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ems2] SET ARITHABORT OFF 
GO
ALTER DATABASE [ems2] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ems2] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ems2] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ems2] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ems2] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ems2] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ems2] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ems2] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ems2] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ems2] SET  ENABLE_BROKER 
GO
ALTER DATABASE [ems2] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ems2] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ems2] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ems2] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ems2] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ems2] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ems2] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ems2] SET RECOVERY FULL 
GO
ALTER DATABASE [ems2] SET  MULTI_USER 
GO
ALTER DATABASE [ems2] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ems2] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ems2] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ems2] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'ems2', N'ON'
GO
USE [ems2]
GO
/****** Object:  Table [dbo].[Appointment]    Script Date: 2/20/2019 2:43:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('Appointment') IS NOT NULL
BEGIN
	DROP TABLE [Appointment]
END
GO
CREATE TABLE [dbo].[Appointment](
	[PK_ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_HCN] [nchar](12) NOT NULL,
	[Date] [date] NOT NULL,
	[TimeSlot] [tinyint] NOT NULL,
 CONSTRAINT [PK_Appointment] PRIMARY KEY CLUSTERED 
(
	[PK_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AppointmentBill]    Script Date: 2/20/2019 2:43:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('AppointmentBill') IS NOT NULL
BEGIN
	DROP TABLE [AppointmentBill]
END
GO
CREATE TABLE [dbo].[AppointmentBill](
	[FK_AppointmentID] [int] NOT NULL,
	[BillingCode] [nchar](10) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AppointmentDetail]    Script Date: 2/20/2019 2:43:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('AppointmentDetail') IS NOT NULL
BEGIN
	DROP TABLE [AppointmentDetail]
END
GO
CREATE TABLE [dbo].[AppointmentDetail](
	[FK_AppointmentID] [int] NOT NULL,
	[Description] [varchar](250) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AppointmentRequest]    Script Date: 2/20/2019 2:43:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('AppointmentRequest') IS NOT NULL
BEGIN
	DROP TABLE [AppointmentRequest]
END
GO
CREATE TABLE [dbo].[AppointmentRequest](
	[PK_HCN] [nchar](12) NOT NULL,
	[RequestedDate] [date] NOT NULL,
	[RequestedTimeSlot] [tinyint] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Employee]    Script Date: 2/20/2019 2:43:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('Employee') IS NOT NULL
BEGIN
	DROP TABLE [Employee]
END
GO
CREATE TABLE [dbo].[Employee](
	[PK_EmployeeID] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeType] [nchar](1) NOT NULL,
 CONSTRAINT [PK_ems.Employee] PRIMARY KEY CLUSTERED 
(
	[PK_EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmployeeLogin]    Script Date: 2/20/2019 2:43:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('EmployeeLogin') IS NOT NULL
BEGIN
	DROP TABLE [EmployeeLogin]
END
GO
CREATE TABLE [dbo].[EmployeeLogin](
	[FK_EmployeeID] [int] NOT NULL,
	[UserName] [nvarchar](20) NOT NULL,
	[Password] [nvarchar](128) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MobileLogin]    Script Date: 2/20/2019 2:43:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('MobileLogin') IS NOT NULL
BEGIN
	DROP TABLE [MobileLogin]
END
GO
CREATE TABLE [dbo].[MobileLogin](
	[FK_HCN] [nchar](12) NOT NULL,
	[password] [nvarchar](128) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Patient]    Script Date: 2/20/2019 2:43:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('Patient') IS NOT NULL
BEGIN
	DROP TABLE [Patient]
END
GO
CREATE TABLE [dbo].[Patient](
	[PK_HCN] [nchar](12) NOT NULL,
	[FK_HOH] [nchar](12) NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[MiddleInitial] [nchar](1) NULL,
	[DOB] [date] NOT NULL,
	[Gender] [nchar](1) NOT NULL,
	[AddressLine1] [nvarchar](50) NOT NULL,
	[AddressLine2] [nvarchar](50) NULL,
	[City] [nvarchar](50) NOT NULL,
	[Province] [nchar](2) NOT NULL,
	[PostalCode] [nchar](7) NOT NULL,
	[PhoneNumber] [nchar](12) NOT NULL,
 CONSTRAINT [PK_Patient] PRIMARY KEY CLUSTERED 
(
	[PK_HCN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Appointment]  WITH CHECK ADD  CONSTRAINT [FK_Appointment_Patient] FOREIGN KEY([FK_HCN])
REFERENCES [dbo].[Patient] ([PK_HCN])
GO
ALTER TABLE [dbo].[Appointment] CHECK CONSTRAINT [FK_Appointment_Patient]
GO
ALTER TABLE [dbo].[AppointmentBill]  WITH CHECK ADD  CONSTRAINT [FK_AppointmentBill_AppointmentNumber] FOREIGN KEY([FK_AppointmentID])
REFERENCES [dbo].[Appointment] ([PK_ID])
GO
ALTER TABLE [dbo].[AppointmentBill] CHECK CONSTRAINT [FK_AppointmentBill_AppointmentNumber]
GO
ALTER TABLE [dbo].[AppointmentDetail]  WITH CHECK ADD  CONSTRAINT [FK_AppointmentDetail_AppointmentID] FOREIGN KEY([FK_AppointmentID])
REFERENCES [dbo].[Appointment] ([PK_ID])
GO
ALTER TABLE [dbo].[AppointmentDetail] CHECK CONSTRAINT [FK_AppointmentDetail_AppointmentID]
GO
ALTER TABLE [dbo].[AppointmentRequest]  WITH CHECK ADD  CONSTRAINT [FK_AppointmentRequest_HCN] FOREIGN KEY([PK_HCN])
REFERENCES [dbo].[Patient] ([PK_HCN])
GO
ALTER TABLE [dbo].[AppointmentRequest] CHECK CONSTRAINT [FK_AppointmentRequest_HCN]
GO
ALTER TABLE [dbo].[EmployeeLogin]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeLogin_Employee] FOREIGN KEY([FK_EmployeeID])
REFERENCES [dbo].[Employee] ([PK_EmployeeID])
GO
ALTER TABLE [dbo].[EmployeeLogin] CHECK CONSTRAINT [FK_EmployeeLogin_Employee]
GO
ALTER TABLE [dbo].[MobileLogin]  WITH CHECK ADD  CONSTRAINT [FK_MobileLogin_Patient] FOREIGN KEY([FK_HCN])
REFERENCES [dbo].[Patient] ([PK_HCN])
GO
ALTER TABLE [dbo].[MobileLogin] CHECK CONSTRAINT [FK_MobileLogin_Patient]
GO
USE [master]
GO
ALTER DATABASE [ems2] SET  READ_WRITE 
GO
