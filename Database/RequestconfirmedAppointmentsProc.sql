/****** Object:  StoredProcedure [dbo].[RequestConfirmedAppointments]    Script Date: 4/23/2019 1:25:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Conor Macpherson
-- Create Date: 2019-04-22
-- Description: Selects all appointmetn dates and time slots for a patient.
-- =============================================
ALTER PROCEDURE [dbo].[RequestConfirmedAppointments]
(
    @HealthCardNumber nchar(12)
)
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON

   SELECT [Date], TimeSlot FROM Appointment WHERE FK_HCN = @HealthCardNumber AND CheckedIn = 0
   ORDER BY TimeSlot
END


INSERT INTO Appointment VALUES
('1234567890AB', 2, '2019-04-23', 3, 0)


DELETE FROM AppointmentRequest
SELECT * FROM AppointmentRequest