/****** Object:  StoredProcedure [dbo].[SelectAllAppointmentsForMonthAndYear]    Script Date: 4/22/2019 12:44:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Conor Macpherson
-- Create Date: 2019-04-21
-- Description: Gets all the appointmetns for the provided month
-- =============================================
ALTER PROCEDURE [dbo].[SelectAllAppointmentsForMonthAndYear]
(
    @Month int,
	@Year int
)
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON

   SELECT [Date], TimeSlot From Appointment WHERE MONTH(Date) = @Month AND YEAR(Date) = @Year
END
