-- =======================================================
-- Create Stored Procedure Template for Azure SQL Database
-- =======================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Conor Macpherson
-- Create Date: 2019-04-22
-- Description: Sets an appointment to checked in.
-- =============================================
ALTER PROCEDURE CheckInForAppointment
(
	@HealthCardNumber nchar(12),
	@appointmentDate DATE,
	@appointmentSlot tinyint
)
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON


	IF EXISTS (SELECT FK_HCN FROM Appointment WHERE FK_HCN = @HealthCardNumber AND [Date] = @appointmentDate AND TimeSlot = @appointmentSlot)
	BEGIN
 		UPDATE Appointment SET CheckedIn = 1
		WHERE FK_HCN = @HealthCardNumber AND [Date] = @appointmentDate AND TimeSlot = @appointmentSlot

		SELECT 1 as 'Response'
	END
	ELSE
	BEGIN
		SELECT -1 as 'Response'
	END
END
GO
