USE [EMS]

-- STORE PROCEDURES FOR EMS 2 BY MOISES DIAZ, FIRAS AREIBI --

-- Inserting Patient --
IF OBJECT_ID ('InsertPatient') IS NOT NULL
	DROP PROCEDURE [InsertPatient]
GO
CREATE PROCEDURE InsertPatient
@HealthCardNumber nchar(12), @HeadOfHouse nchar(12),
@FirstName nvarchar(50), @LastName nvarchar(50), @MiddleInitial nchar(1), 
@DateOfBirth date, @Gender nchar(1), 
@AddressLine1 nvarchar(50), @AddressLine2 nvarchar(50), 
@City nvarchar(50), @Province nchar(2), 
@PostalCode nchar(7), @PhoneNumber nchar(12)
AS
BEGIN
INSERT INTO Patient 
	(PK_HCN, FK_HOH, FirstName, LastName, MiddleInitial, DOB, Gender, AddressLine1, AddressLine2, City, Province, PostalCode, PhoneNumber) VALUES 
	(@HealthCardNumber, @HeadOfHouse, @FirstName, @LastName, @MiddleInitial, @DateOfBirth, @Gender, @AddressLine1, @AddressLine2, @City, @Province, @PostalCode, @PhoneNumber);
END
GO

-- HCN Exists --
IF OBJECT_ID ('DoesHCNExist') IS NOT NULL
	DROP PROCEDURE DoesHCNExist
GO
CREATE PROCEDURE DoesHCNExist
@HealthCardNumber nchar(12),
@Found BIT OUTPUT
AS
BEGIN
	SET @Found = 'false'

	IF EXISTS (SELECT PK_HCN FROM Patient WHERE PK_HCN = @HealthCardNumber)
	BEGIN
		SET @Found = 'true'
	END
END
GO

-- Patient Lookup --
IF OBJECT_ID ('SearchPatient') IS NOT NULL
	DROP PROCEDURE [SearchPatient]
GO
CREATE PROCEDURE SearchPatient
@HCN nchar(12),
@LastName nvarchar(50)
AS
BEGIN
    IF (@HCN = '' AND @LastName = '')
    BEGIN
    	SELECT PK_HCN AS 'HCN', FirstName AS 'First Name', LastName AS 'Last Name', CONCAT(AddressLine1, ' ', AddressLine2) AS 'Address', PhoneNumber AS 'Phone Number', FK_HOH AS 'Head of House' FROM Patient
    END
    ELSE IF (@HCN = '')
    BEGIN
    	SELECT PK_HCN AS 'HCN', FirstName AS 'First Name', LastName AS 'Last Name', CONCAT(AddressLine1, ' ', AddressLine2) AS 'Address', PhoneNumber AS 'Phone Number', FK_HOH AS 'Head of House' FROM Patient
    		WHERE LastName = @LastName
    END
	ELSE
	BEGIN
		SELECT PK_HCN AS 'HCN', FirstName AS 'First Name', LastName AS 'Last Name', CONCAT(AddressLine1, ' ', AddressLine2) AS 'Address', PhoneNumber AS 'Phone Number', FK_HOH AS 'Head of House' FROM Patient
    		WHERE PK_HCN = @HCN OR FK_HOH = @HCN
	END
END
GO

-- New Appointments --
IF OBJECT_ID ('InsertAppointment') IS NOT NULL
	DROP PROCEDURE [InsertAppointment]
GO
CREATE PROCEDURE [InsertAppointment]
@HCN nchar (12),
@AppointmentDate date,
@TimeSlot TINYINT
AS
BEGIN
    INSERT INTO Appointment (FK_HCN, Appointment.Date, TimeSlot) 
	VALUES (@HCN, @AppointmentDate, @TimeSlot)
END
GO

-- Appointment Lookup --
IF OBJECT_ID ('PatientAppointments') IS NOT NULL
	DROP VIEW [PatientAppointments]
GO
CREATE VIEW [PatientAppointments]
AS
    SELECT PK_ID AS 'ID', FK_HCN AS 'HCN', CONVERT(varchar, Appointment.Date, 23) AS 'Date', TimeSlot, FirstName AS 'First Name', LastName AS 'Last Name', PhoneNumber AS 'Phone Number', FK_HOH AS 'HOH' FROM [Appointment]
    INNER JOIN [Patient] ON PK_HCN = FK_HCN
GO

IF OBJECT_ID ('SearchAppointment') IS NOT NULL
	DROP PROCEDURE [SearchAppointment]
GO
CREATE PROCEDURE [SearchAppointment]
@HCN nchar (12),
@LastName nvarchar(50)
AS
BEGIN
    IF (@HCN = '' AND @LastName = '')
    BEGIN
    	SELECT * FROM PatientAppointments
    END
    ELSE
    BEGIN
    	SELECT * FROM PatientAppointments
    		WHERE [HCN] = @HCN OR [Last Name] = @LastName
    END
END
GO

-- Get all dates and timeslots that are taken --
IF OBJECT_ID ('GetTakenDates') IS NOT NULL
	DROP PROCEDURE [GetTakenDates]
GO
CREATE PROCEDURE [GetTakenDates]
AS
BEGIN
	SELECT CONVERT(varchar, Appointment.Date, 23) AS 'Date', TimeSlot FROM Appointment
	ORDER BY Appointment.Date
END
GO


-- Get HOH Info --
IF OBJECT_ID ('GetHOHInfo') IS NOT NULL
	DROP PROCEDURE [GetHOHInfo]
GO
CREATE PROCEDURE [GetHOHInfo]
@HOH nchar (12)
AS
BEGIN
	SELECT AddressLine1, AddressLine2, City, PostalCode, Province, PhoneNumber FROM Patient WHERE PK_HCN = @HOH
END
GO

--add billing code
IF OBJECT_ID ('AddBillingCode') IS NOT NULL
	DROP PROCEDURE [AddBillingCode]
GO
CREATE PROCEDURE [AddBillingCode]
@AptID int,
@BillingCode nchar(10),
@HCN nchar(12),
@Month int,
@Year int
AS
BEGIN
	INSERT INTO AppointmentBill(FK_AppointmentID, BillingCode)
		VALUES (@AptID, @BillingCode)
END
GO


-- Get billing info
IF OBJECT_ID ('BillingInfo') IS NOT NULL
	DROP VIEW [BillingInfo]
GO
CREATE VIEW [BillingInfo]
AS
	SELECT [PK_ID] AS 'AppointmentID', [PK_HCN] AS 'HCN', [Gender], CONVERT(varchar, Appointment.Date, 23) AS 'Date' FROM [Appointment]
	INNER JOIN [Patient] ON [Appointment].[FK_HCN] = [Patient].[PK_HCN]
GO

IF OBJECT_ID ('GetBillingInfo') IS NOT NULL
	DROP PROCEDURE [GetBillingInfo]
GO
CREATE PROCEDURE [GetBillingInfo]
@Year [int],
@Month [int]
AS
BEGIN
	SELECT * FROM [BillingInfo]
	WHERE YEAR([Date]) = @Year AND MONTH([Date]) = @Month
END
GO


-- Get billing codes
IF OBJECT_ID ('GetBillingCodes') IS NOT NULL
	DROP PROCEDURE [GetBillingCodes]
GO
CREATE PROCEDURE [GetBillingCodes]
@AppointmentID [int]
AS
BEGIN
	SELECT [BillingCode] FROM [AppointmentBill]
	WHERE [FK_AppointmentID] = @AppointmentID
END
GO


--db back up
IF OBJECT_ID ('DataBaseBackUp') IS NOT NULL
	DROP PROCEDURE [DataBaseBackUp]
GO
CREATE PROCEDURE DataBaseBackUp 
AS
begin  
declare @path varchar(500);  
declare @date date
set @date=FORMAT(GETDATE(), 'D', 'en-US');
set @path=N'C:\Program Files\Microsoft SQL Server\MSSQL13.SCHOOL\MSSQL\Backup\'+CONVERT(CHAR(10),  GETDATE(), 121)+'-ems2.bak';  
BACKUP DATABASE SampleDBTest to DISK=@path;  
end  
GO  


-- data base restore
IF OBJECT_ID ('RestoreDateBase') IS NOT NULL
	DROP PROCEDURE [RestoreDateBase]
GO
CREATE PROCEDURE RestoreDateBase
@date varchar(15)
AS
begin
declare @path varchar(500)
set @path= N'C:\Program Files\Microsoft SQL Server\MSSQL13.SCHOOL\MSSQL\Backup\'+@date +'-ems.bak'
Restore Database [ems2]
from disk =@path
with replace 
end
go


IF OBJECT_ID ('LoginMobile') IS NOT NULL
	DROP PROCEDURE [LoginMobile]
GO
CREATE PROCEDURE [LoginMobile] @hcn nchar(12)
AS
begin
	DECLARE @result BIT
	SET @result = 0
    if exists (select PK_HCN from Patient where PK_HCN=@hcn)
	begin 
	insert into MobileLogin (Fk_HCN)values(@hcn)
    SET @result = 1
	end
	
    RETURN (@result)
end
GO


IF OBJECT_ID ('AES_Setup') IS NOT NULL
	DROP PROCEDURE AES_Setup
GO
create procedure AES_Setup
AS
BEGIN
	Create master key encryption by
	password = 'SchoolEms2';

Create Certificate AesCert
	with subject = 'mysecrets'

Create symmetric key SSN_KEY
	With algorithm = AES_256
	ENCRYPTION BY CERTIFICATE MEME;
END
GO


--IF OBJECT_ID ('aesEncrypDecryp') IS NOT NULL
--	DROP PROCEDURE aesEncrypDecryp
--GO
--create procedure aesEncrypDecryp
--AS
----encrypt
--OPEN symmetric key SSN_KEY
--	DECRYPTION BY CERTIFICATE MEME
--	INSERT INTO tableuwant values
--	(1,EncryptByKey(Key_GUID('SSN_KEY'),@value),EncryptByKey(Key_GUID('SSN_KEY'),@value)),
--	(2,EncryptByKey(Key_GUID('SSN_KEY'),'Conor2'),EncryptByKey(Key_GUID('SSN_KEY'),'conorfiras')),
--	(3,EncryptByKey(Key_GUID('SSN_KEY'),'jacob1'),EncryptByKey(Key_GUID('SSN_KEY'),'jacobfiras'))

--	--decrypt 
--open symmetric key SSN_KEY
--	DECRYPTION BY CERTIFICATE MEME;

--	SELECT names, convert (varchar, DecryptByKEy(@coloumnName)) as [@decryptedCol],  convert (varchar, DecryptByKEy(@coloumnName)) as [@decryptedCol]
--	from table1

--	close symmetric key SSN_KEY
--go


-- login validation
IF OBJECT_ID('CheckLogin') IS NOT NULL
	DROP FUNCTION [CheckLogin]
GO
CREATE FUNCTION [CheckLogin] (@Username nvarchar(20), @Password nvarchar(128))
RETURNS [bit]
AS
BEGIN
	DECLARE @Valid [bit]

	IF (SELECT [UserName] FROM [EmployeeLogin] WHERE @Username = [UserName] AND @Password = [Password]) IS NOT NULL
		SET @Valid = 1
	ELSE
		SET @Valid = 0
	
	RETURN(@Valid)
END
GO


--returns employee security level
IF OBJECT_ID ('GetEmployeeType') IS NOT NULL
	DROP FUNCTION [GetEmployeeType]
GO
CREATE FUNCTION [GetEmployeeType] (@Username [nvarchar](20), @Password [nvarchar](128))
RETURNS [nchar](1)
AS
BEGIN
DECLARE @EmployeeType [nchar](1)

SELECT @EmployeeType = Employee.EmployeeType FROM EmployeeLogin
	INNER JOIN Employee ON EmployeeLogin.FK_EmployeeID = Employee.PK_EmployeeID
	WHERE EmployeeLogin.Password = @Password AND EmployeeLogin.UserName = @Username

RETURN @EmployeeType
END
GO

-- delete an appointment request
IF OBJECT_ID ('DeleteAppointmentRequest') IS NOT NULL
	DROP PROCEDURE [DeleteAppointmentRequest]
GO
CREATE PROCEDURE [DeleteAppointmentRequest]
@RequestID [int]
AS
BEGIN
	DELETE FROM AppointmentRequest
	WHERE PK_ID = @RequestID
END
GO

IF OBJECT_ID ('AppointmentAccepted') IS NOT NULL
	DROP TRIGGER [AppointmentAccepted]
GO
create trigger [AppointmentAccepted]
ON Appointment 
AFTER INSERT, UPDATE 
as  
DECLARE @MyCursor CURSOR
DECLARE @AppAccHCN nchar(12)

SET @MyCursor = CURSOR FOR
    select FK_HCN from Appointment
	
--app tables
OPEN @MyCursor
    FETCH NEXT FROM @MyCursor 
    INTO @AppAccHCN

--app cursor	
WHILE @@FETCH_STATUS = 0
BEGIN
	

	FETCH NEXT FROM @MyCursor 
	INTO @AppAccHCN
END

    CLOSE @MyCursor 
    DEALLOCATE @MyCursor
GO


--grab req from appoint req table
IF OBJECT_ID ('GetAppReq') IS NOT NULL
	DROP procedure [GetAppReq]
GO
create procedure GetAppReq
as
	select PK_ID AS 'ID', FK_HCN AS 'HCN', CONVERT(varchar, RequestedDate, 23) AS 'Date', RequestedTimeSlot AS 'Time Slot' FROM AppointmentRequest
go

-- get appointments for the current day
IF OBJECT_ID ('GetCurrentCheckins') IS NOT NULL
	DROP procedure [GetCurrentCheckins]
GO
CREATE PROCEDURE [GetCurrentCheckins]
AS
BEGIN
	SELECT PK_ID AS 'ID', FK_HCN AS 'HCN', CONVERT(varchar, Appointment.Date, 23) AS 'Date', TimeSlot AS 'Time Slot', CheckedIn AS 'Checked In' FROM Appointment
	WHERE [Date] = CONVERT(DATE, GETDATE())
	ORDER BY [CheckedIn]
END
GO


IF OBJECT_ID ('CheckinApt') IS NOT NULL
	DROP procedure [CheckinApt]
GO
CREATE PROCEDURE [CheckinApt]
@AptID [int]
AS
BEGIN
	UPDATE [Appointment]
	SET CheckedIn = 1
	WHERE PK_ID = @AptID
END
GO