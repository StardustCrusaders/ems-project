-- =======================================================
-- Create Stored Procedure Template for Azure SQL Database
-- =======================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Conor Macpherson
-- Create Date: 2019-04-21
-- Description: Gets the appointments and their time slots for the provided month.
--				Note: Months range from 1 - 12 (January - December)
-- =============================================
ALTER PROCEDURE SelectTimeSlotsForMonth
(
   @Month int 
)
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON

    -- Insert statements for procedure here
    SELECT TimeSlot From Appointment WHERE MONTH([Date]) = @Month
END
GO
