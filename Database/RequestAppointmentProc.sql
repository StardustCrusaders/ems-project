/****** Object:  StoredProcedure [dbo].[RequestAppointment]    Script Date: 4/23/2019 1:50:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Author:      Conor Macpherson
-- Create Date: 2019-04-21
-- Description: Adds an appointment request to 
--				Column return with values of:
--					1  - Insert successful
--					-1 - The request already exists.
--					-2 - The patient does not exist.
-- =============================================
ALTER PROCEDURE [dbo].[RequestAppointment]
(
    @HealthcardNumber nchar(12),
	@Date Date,
	@TimeSlot tinyint
)
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON
	
	
	IF NOT EXISTS (SELECT FK_HCN, RequestedDate, RequestedTimeSlot FROM AppointmentRequest
					WHERE FK_HCN = @HealthcardNumber AND RequestedDate = @Date AND RequestedTimeSlot = @TimeSlot)
	BEGIN
		IF EXISTS (SELECT PK_HCN FROM Patient WHERE PK_HCN = @HealthcardNumber)
		BEGIN
			INSERT INTO AppointmentRequest VALUES
			(@HealthcardNumber, @Date, @TimeSlot)
			SELECT 1 as 'Response'
		END
		ELSE
		BEGIN
			SELECT -2 as 'Response'
		END
	END
	ELSE
	BEGIN
		SELECT -1 as 'Response'
	END
END