-- =======================================================
-- Create Stored Procedure Template for Azure SQL Database
-- =======================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Conor Macpherson
-- Create Date: 2019-04-22
-- Description: Validates that a mobile login exists for a healthcard number and hashed password
-- Returns:		1 If the user exists.
--				0 otherwise.
-- =============================================
Alter PROCEDURE VerifyMobileLogin
(
    @HealthCardNumber nchar(12),
	@Password nvarchar(128)
)
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON

	IF EXISTS (SELECT FK_HCN, password FROM MobileLogin WHERE FK_HCN = @HealthCardNumber AND [password] = @Password)
	BEGIN
		return 1
	END
	ELSE
	BEGIN
		return 0
	END
    
END
GO
