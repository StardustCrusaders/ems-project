-- =======================================================
-- Create Stored Procedure Template for Azure SQL Database
-- =======================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Conor Macpherson
-- Create Date: 2019-04-21
-- Description: Gets all the appointmetns for the provided month
-- =============================================
CREATE PROCEDURE SelectAllAppointmentsTimeSlot
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON

   SELECT TimeSlot From Appointment
END
GO