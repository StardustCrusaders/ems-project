-- =======================================================
-- Create Stored Procedure Template for Azure SQL Database
-- =======================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      <Author, , Name>
-- Create Date: <Create Date, , >
-- Description: <Description, , >
-- =============================================
ALTER PROCEDURE AddMobileLogin
(
    @healthCardNumber nchar(12),
	@passwordHash nvarchar(128)
)
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON

    IF EXISTS (SELECT PK_HCN FROM Patient WHERE PK_HCN = @healthCardNumber)
	BEGIN

		IF NOT EXISTS (SELECT FK_HCN FROM MobileLogin WHERE FK_HCN = @healthCardNumber)
		BEGIN
			INSERT INTO MobileLogin VALUES
			(@healthCardNumber, @passwordHash)

			SELECT 1 as 'returnValue'
		END
		ELSE
		BEGIN
			SELECT -1 as 'returnValue' 
		END
	END
	ELSE
	BEGIN
		SELECT -2 as 'returnValue'
	END
END
GO
