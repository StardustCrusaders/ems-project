using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.IO;

namespace Logging
{
    /// <summary>
    /// CLASS:          Logging
    /// Programmers:    Firas, Jacob, William, Moises
    /// Date:           2018-11-12
    /// </summary>
    public class Logging
    {
        //static void Main(string[] args)
        //{
        //    MethodCalled("Patient - Firas Areibi HCN (1111111111)", "Invalid");
        //}

        /// <summary>
        /// Creates log file for application if it does not already exist.
        /// </summary>
        /// <param name="contents">The information to be logged.</param>
        private static void CreateFile(string contents)
        {
            string newFile = @"./Logging/ems." + DateTime.Now.ToString("yyyy-MM-dd") + ".log";
            if (!File.Exists(newFile))
            {
                FileStream logFile = File.Create(newFile);
                logFile.Close();
                File.WriteAllText(newFile, "Logging File " + Environment.NewLine);
            }
                   
            OutputToFile(newFile, contents);
        }


        /// <summary>
        /// Outputs the logging message to the file.
        /// </summary>
        /// <param name="filePath">The path of the file being written to.</param>
        /// <param name="contents">The information that will be written to the file.</param>
        private static void OutputToFile(string filePath, string contents)
        {
            File.AppendAllText(filePath, contents + Environment.NewLine);
        }


        /// <summary>
        /// Grabs the class and method calling this function for logging purposes.
        /// </summary>
        /// <param name="description">The event description.</param>
        /// <param name="result">The result of the information.</param>
        public static void MethodCalled(string description, string result)
        {
            //if performance isnt a issue use this [MethodImpl(MethodImplOptions.NoInlining)] using System.Globalization using System.Runtime.CompilerServices
            //as well if class name and method name is wrong increase frame to 2.
            string methodName;
            string className;
            string wholeEvent = "";

            // Creates date year month day format with hour:min:sec
            string date = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"); 
            
            StackTrace stackTrace = new StackTrace();

            // Get calling method name
            methodName = stackTrace.GetFrame(1).GetMethod().Name;

            // Get class name 
            var methodInfo = stackTrace.GetFrame(1).GetMethod();
            className = methodInfo.ReflectedType.Name;

            // Log message
            wholeEvent = $"{date} [{className}.{methodName}] {description} - {result}";

            CreateFile(wholeEvent);
        }
    }

}
